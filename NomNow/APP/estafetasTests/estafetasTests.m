//
//  estafetasTests.m
//  estafetasTests
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Cart.h"
#import "Product.h"


@interface estafetasTests : XCTestCase

@end

@implementation estafetasTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - tests add products to cart

- (void)testAdd2TimeSameProduct {
    
    Product *p1 = [[Product alloc]init];
    
    p1.name = @"P1";
    
    Cart *cart = [[Cart alloc]init];
    
    [cart addProduct:p1];
    [cart addProduct:p1];
    
    OrderProduct *orderProdut = cart.products[0];
    
    XCTAssert(orderProdut.quantity.integerValue == 2, @"%@",orderProdut.quantity);
    
}

- (void)testAddProductWithQuantity {
    
    Product *p1 = [[Product alloc]init];
    
    p1.name = @"P1";
    
    Cart *cart = [[Cart alloc]init];
    
    [cart addProduct:p1];
    [cart addProduct:p1 withQuantity:[NSNumber numberWithInteger:2]];
    
    OrderProduct *orderProdut = cart.products[0];
    
    XCTAssert(orderProdut.quantity.integerValue == 3, @"%@",orderProdut.quantity);
    
}

- (void)testAddProductWithObservation {
    
    Product *p1 = [[Product alloc]init];
    
    p1.name = @"P1";
    
    Cart *cart = [[Cart alloc]init];
    
    [cart addProduct:p1];
    [cart addProduct:p1 withQuantity:[NSNumber numberWithInteger:2] andObservation:@"foo"];
    
    OrderProduct *orderProdut = cart.products[0];
    
    XCTAssert([orderProdut.observations isEqualToString:@"foo"]);
    
}

- (void)testAdd2DiferentsProducts {
    
    Product *p1 = [[Product alloc]init];
    Product *p2 = [[Product alloc]init];
    
    p1.name = @"P1";
    p2.name = @"P2";
    
    p1.objectId= @"a";
    p1.objectId= @"b";
    
    Cart *cart = [[Cart alloc]init];
    
    [cart addProduct:p1];
    [cart addProduct:p2];

    XCTAssert(cart.products.count == 2);
    
}

#pragma mark - tests remove product card

- (void)testRemoveProduct {
    
    Product *p1 = [[Product alloc]init];
    
    p1.name = @"P1";
    
    Cart *cart = [[Cart alloc]init];
    
    [cart addProduct:p1];
    [cart removeProducts:p1];
    
    XCTAssert(cart.products.count == 0);
    
}




@end
