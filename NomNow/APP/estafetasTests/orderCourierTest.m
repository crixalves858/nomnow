//
//  orderCourierTest.m
//  estafetas
//
//  Created by Cristiano Alves on 12/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ServiceManager.h"
#import "ClientServiceManager.h"
#import "CourierServiceManager.h"
#import "Session.h"
#import "ClientSession.h"
#import "CourierSession.h"

@interface orderCourierTest : XCTestCase

@end

@implementation orderCourierTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/*- (void)testSetStateCourier {

    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
       
        if(!error) {
            
            [[CourierServiceManager sharedInstance] setCourierState:CourierStateWaiting withBlock:^(BOOL succeeded, NSError *error) {
               
                if(!error) {
                    
                    [[CourierServiceManager sharedInstance] fetchCourierWithBlock:^(PFObject *user, NSError *error) {
                        
                        if(!error) {
                            
                            User *currentUser = (User*) user;
                            waitingForBlock = NO;
                            XCTAssert(currentUser.courier.privateData.state == CourierStateWaiting);
                            
                        } else {
                            waitingForBlock = NO;

                            XCTAssert(NO, @"Error in fetch");
                            
                        }
                    }];
                    
                } else {
                    waitingForBlock = NO;
                    XCTAssert(NO, @"Error in set");
                }
                
            }];
            
        } else {
            waitingForBlock = NO;
            XCTAssert(NO, @"Error in login");
        }
        
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
 

}

- (void)testSetStateCourier2 {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [[CourierServiceManager sharedInstance] setCourierState:CourierStateWaiting withBlock:^(BOOL succeeded, NSError *error) {
                
                if(!error) {
                    
                    [[CourierServiceManager sharedInstance] fetchCourierWithBlock:^(PFObject *user, NSError *error) {
                        
                        if(!error) {
                            
                            User *currentUser = (User*) user;
                            waitingForBlock = NO;
                            XCTAssert(currentUser.courier.privateData.state == CourierStateWaiting);
                            
                        } else {
                            waitingForBlock = NO;
                            
                            XCTAssert(NO, @"Error in fetch");
                            
                        }
                    }];
                    
                } else {
                    waitingForBlock = NO;
                    XCTAssert(NO, @"Error in set");
                }
                
            }];
            
        } else {
            waitingForBlock = NO;
            XCTAssert(NO, @"Error in login");
        }
        
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
 
    
}

- (void)testCreateOrder {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            double latitude = [[ClientSession sharedInstance] currentUser].client.privateData.location.latitude;
            double longitude = [[ClientSession sharedInstance] currentUser].client.privateData.location.longitude;

            [[ClientServiceManager sharedInstance]getNearRestaurantsWithLatitude:latitude andLongitude:longitude block:^(NSArray *restaurants, NSArray *states, NSError *error) {
                if(!error) {
                    
                    XCTAssert(restaurants.count == 1);
                    NSNumber *stateObj = states[0];
                    NearRestaurantState state = stateObj.integerValue;
                    XCTAssert(state == NearRestaurantStateAvaliable, @"state: %ld", state);
                    Restaurant *restaurant = restaurants[0];
                    
                    [[ClientServiceManager sharedInstance] getProductsByRestaurant:restaurant andCategory:restaurant.categories[0] block:^(NSArray *products, NSError *error) {
                    
                        if(!error) {
                            
                            XCTAssert(products.count == 1);
                            Product *product = products[0];
                            
                            Cart *cart = [[Cart alloc]init];
                            
                            [cart addProduct:product];
                            
                            [[ClientServiceManager sharedInstance]checkoutCart:cart withDeliveryAddress:@"glazed" location:nil doorNumber:nil andZipCode:nil  block:^(Order *order, NSError *error) {
                               
                                if(!error) {
                                    
                                    OrderProduct *orderProduct = order.products[0];
                                    XCTAssert(orderProduct.quantity.integerValue == 1);
                                    
                                   
                                    
                                    [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                                       
                                        XCTAssert(orders.count==1);
                                        waitingForBlock = NO;
                                        
                                    }];
                                    
                                    
                                    
                                } else {
                                    
                                    XCTAssert(NO, @"Error in create product");
                                    waitingForBlock = NO;
                                    
                                }
                                
                            }];

                        } else {
                            
                            XCTAssert(NO, @"Error in get products");
                            waitingForBlock = NO;
                            
                        }
                        
                    }];
                    
                    
                    
                } else {
                     XCTAssert(NO, @"Error in near restaurants");
                    waitingForBlock = NO;
                }
            }];
            
        } else {
            
             XCTAssert(NO, @"Error in login");
            waitingForBlock = NO;
            
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }

}

- (void) testCheckOrderState1 {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {

            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                
                XCTAssert(orders.count==1);
                [[ClientServiceManager sharedInstance]checkOrderState:orders[0] block:^(FindCourierState state) {
                    XCTAssert(state==FindCourierStateWaiting, @"state: %ld",state);
                    waitingForBlock = NO;
                }];

                
            }];
            
        } else {
            
            XCTAssert(NO, @"Error in login");
            waitingForBlock = NO;
            
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}


- (void)testCheckOrderResquestCourier1NotSelect {
    
    __block BOOL waitingForBlock = YES;

    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(!eligible);
                waitingForBlock = NO;
                
            }];
        
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
     
    
   

    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }

}


- (void)testCheckOrderResquestCourier2Select {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(eligible);
                waitingForBlock = NO;
               
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}

- (void) testCheckOrderState2 {
    
    __block BOOL waitingForBlock = YES;
     [NSThread sleepForTimeInterval:2];
    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                
                XCTAssert(orders.count==1);
                [[ClientServiceManager sharedInstance]checkOrderState:orders[0] block:^(FindCourierState state) {
                    XCTAssert(state==FindCourierStateWaiting, @"state: %ld",state);
                    waitingForBlock = NO;
                }];
                
                
            }];
            
        } else {
            
            XCTAssert(NO, @"Error in login");
            waitingForBlock = NO;
            
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}

- (void)testCheckOrderResquestCourier1Select {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(eligible);
                waitingForBlock = NO;
                
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    
    
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}


- (void)testCheckOrderResquestCourier2NotSelect {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(!eligible);
                waitingForBlock = NO;
                
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}


- (void) testCheckOrderState3 {
    
    __block BOOL waitingForBlock = YES;
    [NSThread sleepForTimeInterval:2];
    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                
                XCTAssert(orders.count==1);
                [[ClientServiceManager sharedInstance]checkOrderState:orders[0] block:^(FindCourierState state) {
                    XCTAssert(state==FindCourierStateWaiting, @"state: %ld",state);
                    waitingForBlock = NO;
                }];
                
                
            }];
            
        } else {
            
            XCTAssert(NO, @"Error in login");
            waitingForBlock = NO;
            
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

- (void)testCheckOrderResquestCourier1NotSelect2 {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(!eligible);
                waitingForBlock = NO;
                
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    
    
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}


- (void)testCheckOrderResquestCourier2Last {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(eligible);
                waitingForBlock = NO;
                
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}

- (void) testCourier2Refuse {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {

            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                [[CourierServiceManager sharedInstance] refuseOrderRequest:eligible withBlock:^(BOOL succeeded, NSError *error) {
                    
                    
                    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
                        
                        if(!error) {
                            
                            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                                
                                XCTAssert(orders.count==1);
                                [[ClientServiceManager sharedInstance]checkOrderState:orders[0] block:^(FindCourierState state) {
                                    
                                    
                                    [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                                        
                                        [[ServiceManager sharedInstance] loginWithEmail:@"estafeta2@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
                                        
                                            XCTAssert(!eligible);
                                            waitingForBlock = NO;
                                        
                                        }];
                                       
                                        
                                    }];
                                    
                                    
                                }];
                                
                                
                            }];
                            
                        } else {
                            
                            XCTAssert(NO, @"Error in login");
                            waitingForBlock = NO;
                            
                        }
                    }];
                    
                }];
            }];
            
        } else {
            
            XCTAssert(NO);
            waitingForBlock = NO;
            
        }
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
}


- (void) testCourier1Accept {

    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
                
                XCTAssert(eligible);
                
                [[CourierServiceManager sharedInstance] acceptOrderRequest:eligible withBlock:^(BOOL succeeded, NSError *error) {
                    
                    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
                        
                        if(!error) {
                            
                            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
                                
                                XCTAssert(orders.count==1);
                                [[ClientServiceManager sharedInstance]checkOrderState:orders[0] block:^(FindCourierState state) {
                                    
                                    XCTAssert(state == FindCourierStateHaveCourier);
                                    waitingForBlock = NO;
                                
                                }];
                            }];
                        }
                    }];
                    
                }];
                
            }];
            
        } else {
            XCTAssert(NO);
            waitingForBlock = NO;
        }
    }];
    
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }


}


- (void) testCourierInRestaurant {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            
            Order *order = objects[0];
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;

            [[CourierServiceManager sharedInstance] updateOrder:order state:OrderStateInRestaurant andLocation:point withBlock:^(BOOL succeeded, NSError *error) {
                
                [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
                    
                        Order *order = objects[0];
                        XCTAssert(order.state == OrderStateInRestaurant);
                        waitingForBlock = NO;
                    }];
            }];
        }];
    }];
   
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


- (void) testCourierUpdateProducts {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {

            Order *order = objects[0];
            OrderProduct *product = order.products[0];
            
            product.realPrice = @2;
            
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:@[product]];
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;
            
            [[CourierServiceManager sharedInstance] updateProducts:array location:point withOrder:order block:^(Order *order, NSError *error) {
                
                XCTAssert(order.state == OrderStateUpdateWaitingClientAccept);
                waitingForBlock = NO;
                
            }];
            
        }];
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


- (void) testClientAcceptUpdate {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
            
                Order *order = orders[0];
            
                [[ClientServiceManager sharedInstance] acceptedProductsWithOrder:order block:^(Order *order, NSError *error) {
                
                    XCTAssert(order.state == OrderStateClientAccept);
                    waitingForBlock = NO;
                    
                }];
                
            }];
        }
    }];


    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


- (void) testPayToArrive {
    
    __block BOOL waitingForBlock = YES;

    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            
            Order *order = objects[0];
            [[ServiceManager sharedInstance] updateOrder:order state:OrderStatePay withBlock:^(BOOL succeeded, NSError *error) {
                
                [[ServiceManager sharedInstance] updateOrder:order state:OrderStateGoToClient withBlock:^(BOOL succeeded, NSError *error) {
                    
                    PFGeoPoint *point = [[PFGeoPoint alloc] init];
                    point.latitude = 10;
                    point.longitude = 10;
                    [[CourierServiceManager sharedInstance] updateOrder:order state:OrderStateGoToClient andLocation:point withBlock:^(BOOL succeeded, NSError *error) {
     
                            [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
                                
                                Order *order = objects[0];
                                XCTAssert(order.state == OrderStateGoToClient);
                                waitingForBlock = NO;
                            }];
                        }];
                    
                }];

            
            }];
        }];
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
 
}


- (void) testArrive {
    __block BOOL waitingForBlock = YES;
    
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            
            Order *order = objects[0];
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;

            [[CourierServiceManager sharedInstance] arrivedWithOrder:order andLocation:point block:^(Order *order, NSError *error) {
                
                XCTAssert(order.state == OrderStateArrived);
                waitingForBlock = NO;
                
            }];
            
            
            
        }];
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    
}


- (void) testUploadPhoto {
    
    __block BOOL waitingForBlock = YES;
    
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImageTest"]];
            NSData* data = UIImageJPEGRepresentation(imageView.image, 1);
            
            PFFile *file = [PFFile fileWithData:data];

            
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;
            Order *order = objects[0];
            [[CourierServiceManager sharedInstance] uploadPhoto:file withOrder:order andLocation:point block:^(Order *order, NSError *error) {
               
                
                 XCTAssert(order.state == OrderStateArrivedWaitClient);
                waitingForBlock = NO;
            }];
            
            
        }];
    }];

    
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


- (void) testCanceleDelivery {
    
    __block BOOL waitingForBlock = YES;

    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            Order *order = objects[0];
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;
            [[CourierServiceManager sharedInstance] clientNotAppearWithOrder:order andLocation:point block:^(Order *order, NSError *error) {
               
                XCTAssert(order.state == OrderStateDeliveryFail);
                waitingForBlock = NO;
            }];
        }];
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}


- (void) testCanceleDeliveryClientResponse {
    
    __block BOOL waitingForBlock = YES;


    [[ServiceManager sharedInstance] loginWithEmail:@"client@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
            
            Order *order = orders[0];
            
            [[ClientServiceManager sharedInstance] clientNotAppearToRecebeOrder:order response:@"NAO O VI" block:^(Order *order, NSError *error) {
               
                XCTAssert(order.state == OrderStateDeliveryFailAnalize);
                
                waitingForBlock = NO;
                
            }];

        }];
    }];
    
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }

}

- (void) testConfirmDelivery {
    
    __block BOOL waitingForBlock = YES;
    
    [[ServiceManager sharedInstance] loginWithEmail:@"estafeta1@teste.com" password:@"123" block:^(BOOL succeeded, NSError *error) {
        
        [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
            Order *order = objects[0];
            PFGeoPoint *point = [[PFGeoPoint alloc] init];
            point.latitude = 10;
            point.longitude = 10;
            
            [[CourierServiceManager sharedInstance] confirmeOrder:order PIN:@7325 andLocation:point block:^(Order *order, NSError *error) {
               
                XCTAssert(order.state == OrderStateOrderDelivery);
                waitingForBlock = NO;
            }];

        }];
    }];
    
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}*/

@end