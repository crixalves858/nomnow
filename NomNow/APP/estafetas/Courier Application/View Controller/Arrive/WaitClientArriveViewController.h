//
//  WaitClientArriveViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 06/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "Order.h"
#import "RatingViewController.h"
#import "CourierServiceManager.h"
#import "THPinViewController.h"

@interface WaitClientArriveViewController : OrderStepViewController <THPinViewControllerDelegate>




@end
