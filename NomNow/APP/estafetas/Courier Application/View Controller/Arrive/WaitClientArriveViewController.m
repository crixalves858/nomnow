//
//  WaitClientArriveViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 06/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "WaitClientArriveViewController.h"

@interface WaitClientArriveViewController ()
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *zipCodeLabel;
@property (strong, nonatomic) IBOutlet UILabel *doorNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clientImageView;
@property (weak, nonatomic) IBOutlet UILabel *waitTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *deliveryCancelButton;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *observationsLabel;


@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) NSDate *hourFinishWaitTime;

@end

@implementation WaitClientArriveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initValues];
    [self updateUI];
    
    self.navigationItem.title = @"Aguarde";
    
    [[LocationService sharedInstance] startUpdatingLocation];
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"currentLocation" options:NSKeyValueObservingOptionNew context:nil];
}

- (void) initValues {
    
    [[CourierServiceManager sharedInstance] getUpdateOrder:self.order withState:OrderStateArrivedWaitClient block:^(OrderStateUpdate *orderStateUpdate, NSError *error) {
        
        if(!error) {
            
            if(!orderStateUpdate) {
                
                [self initValues];
            } else {
                
                NSDateComponents *dc = [[NSDateComponents alloc] init];
                [dc setSecond:TIME_WAIT_CLIENT];
                
                self.hourFinishWaitTime = [[NSCalendar currentCalendar] dateByAddingComponents:dc toDate:orderStateUpdate.date options:0];
                
                [self startTimer];
                
                [self updateWaitTime];
                
            }
            
            
        }
    }];
    
}

- (void) updateUI {
    
    self.deliveryAddressLabel.text = self.order.deliveryAddress;
    
    self.zipCodeLabel.text = self.order.deliveryZipCode;
    
    self.doorNumberLabel.text = [NSString stringWithFormat:@"N: %@", self.order.deliveryDoorNumber];
    
    self.clientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.order.client.client.firstName, self.order.client.client.lastName];
    
    [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if(!error) {
            
            self.clientImageView.image = [UIImage imageWithData:data];
        }
    }];
    
}

- (void) updateWaitTime {
    
    if(self.hourFinishWaitTime) {
        
        NSTimeInterval dateTime = [self.hourFinishWaitTime timeIntervalSinceNow];
        
        if(dateTime <= 0) {
            [self stopTimer];
            dateTime = 0;
            self.deliveryCancelButton.hidden = NO;
            
        }
        
        self.waitTimeLabel.text = [NSString stringWithFormat:@"%@", [self stringFromTimeInterval:dateTime]];
        
    }
    
}


#pragma mark - Timer

- (void) startTimer {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1  target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
    
}

- (void) timerTick {
    
    [self updateWaitTime];
}

- (void) stopTimer {
    
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - PIN

- (NSUInteger)pinLengthForPinViewController:(THPinViewController *)pinViewController {
    
    return 4;
}

- (void)pinViewController:(THPinViewController *)pinViewController isPinValid:(NSString *)pin withBlock:(void (^)(BOOL))block {
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *code = [f numberFromString:pin];
    
    [[CourierServiceManager sharedInstance] confirmeOrder:self.order PIN:code andLocation:[LocationService sharedInstance].currentLocation block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if( !error) {
            
            block(YES);
            
        } else {
            
            block(NO);
            
        }
    }];

}

- (BOOL)userCanRetryInPinViewController:(THPinViewController *)pinViewController {
    
    return 1;
}

- (void) pinViewControllerWillDismissAfterPinEntryWasSuccessful:(THPinViewController *)pinViewController {
    
    RatingViewController *ratingViewController = [[RatingViewController alloc] initWithNibName:@"RatingViewController" bundle:nil];
    
    ratingViewController.order = self.order;
    
    [self.navigationController setViewControllers:@[ratingViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];

}

#pragma mark - Action

- (IBAction)confirmButtonAction:(id)sender {
    
    THPinViewController *pinViewController = [[THPinViewController alloc] initWithDelegate:self];
    pinViewController.promptTitle = @"Introduza PIN";
    pinViewController.promptColor = [UIColor darkTextColor];
    pinViewController.view.tintColor = [UIColor darkTextColor];
    pinViewController.hideLetters = YES;
    
    self.view.tag = THPinViewControllerContentViewTag;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    //pinViewController.translucentBackground = YES;
    pinViewController.backgroundColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    
    pinViewController.delegate = self;
    
    [self presentViewController:pinViewController animated:YES completion:nil];
    
}


- (IBAction)deliveryCancelButtonAction:(id)sender {
    
    [self clientNotAppearDeliveryCancel];
    
}

- (void) clientNotAppearDeliveryCancel {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] clientNotAppearWithOrder:self.order andLocation:[[LocationService sharedInstance] currentLocation] block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Entrega cancelada"
                                                               message:@"A encomenda foi cancelada com sucesso" delegate:self
                                                     cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            
            [alertView show];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

#pragma mark - Location

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object  change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"currentLocation"]) {
        
        
        if([self.order.deliveryLocation distanceInKilometersTo:[PFGeoPoint geoPointWithLocation:[[LocationService sharedInstance] currentLocation]]]>0.1) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Volte ao local da entrega"
                                                               message:@"Espere pelo cliente" delegate:nil
                                                     cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            
            [alertView show];
            
            
        }
        
        
        [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateArrivedWaitClient andLocation:[[LocationService sharedInstance] currentLocation] withBlock:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
        
        [self goToHome];
    }
    
}

- (void) goToHome {
    
    WaitingRequestViewController *waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];
    
    [self.navigationController setViewControllers:@[waitingRequestViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (void) dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    
    [[LocationService sharedInstance] removeObserver:self forKeyPath:@"currentLocation"];
    [super dismissViewControllerAnimated:flag completion:completion];
}

@end
