//
//  ArriveViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 27/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ArriveViewController.h"

@interface ArriveViewController ()
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *zipCodeLabel;
@property (strong, nonatomic) IBOutlet UILabel *doorNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clientImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *observationsLabel;

@end

@implementation ArriveViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.navigationItem.title = @"Entrega";
    [self updateUI];
}

- (void) updateUI {
    
    self.deliveryAddressLabel.text = self.order.deliveryAddress;
    
    self.zipCodeLabel.text = self.order.deliveryZipCode;
    
    self.doorNumberLabel.text = [NSString stringWithFormat:@"%@", self.order.deliveryDoorNumber];
    
    self.clientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.order.client.client.firstName, self.order.client.client.lastName];
    
    [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        
        if(!error) {
            
            self.clientImageVIew.image = [UIImage imageWithData:data];
        }
    }];
    
    if([ self.order.deliveryObservations isEqualToString:@""]) {
        
        self.observationsLabel.text = @"Sem observações";
        
    } else {
        
        self.observationsLabel.text = self.order.deliveryObservations;
    }
    
    self.floorLabel.text =  [NSString stringWithFormat:@"%@", self.order.deliveryFloor];

}

#pragma mark - PIN

- (NSUInteger)pinLengthForPinViewController:(THPinViewController *)pinViewController {
    
    return 4;
}

- (void)pinViewController:(THPinViewController *)pinViewController isPinValid:(NSString *)pin withBlock:(void (^)(BOOL))block {
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *code = [f numberFromString:pin];
    
    [[CourierServiceManager sharedInstance] confirmeOrder:self.order PIN:code andLocation:[LocationService sharedInstance].currentLocation block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if( !error) {
            
            block(YES);
            
        } else {
            
            block(NO);
            
        }
    }];
    
}

- (BOOL)userCanRetryInPinViewController:(THPinViewController *)pinViewController {
    
    return 1;
}

- (void) pinViewControllerWillDismissAfterPinEntryWasSuccessful:(THPinViewController *)pinViewController {
    
    RatingViewController *ratingViewController = [[RatingViewController alloc] initWithNibName:@"RatingViewController" bundle:nil];
    
    ratingViewController.order = self.order;
    
    [self.navigationController setViewControllers:@[ratingViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


#pragma mark - Actions

- (IBAction)confirmButtonAction:(id)sender {
    
    THPinViewController *pinViewController = [[THPinViewController alloc] initWithDelegate:self];
    pinViewController.promptTitle = @"Introduza PIN";
    pinViewController.promptColor = [UIColor darkTextColor];
    pinViewController.view.tintColor = [UIColor darkTextColor];
    pinViewController.hideLetters = YES;
    
    self.view.tag = THPinViewControllerContentViewTag;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    //pinViewController.translucentBackground = YES;
    pinViewController.backgroundColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    
    pinViewController.delegate = self;
    
    [self presentViewController:pinViewController animated:YES completion:nil];
}

- (IBAction)sendPhotoButtonAction:(id)sender {
    
    ((UIButton *)sender).enabled = NO;
    
    SendPhotoViewController *sendPhotoViewController = [[SendPhotoViewController alloc] init];
    
    sendPhotoViewController.order = self.order;
    
    [self.navigationController pushViewController:sendPhotoViewController animated:YES];
    
    ((UIButton *)sender).enabled = YES;
}


@end
