//
//  SendPhotoViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 23/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "SendPhotoViewController.h"

@interface SendPhotoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation SendPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[LocationService sharedInstance] startUpdatingLocation];

    self.navigationItem.title = @"Local de entrega";

}

- (IBAction)takePhotoAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    
    [self presentViewController:picker animated:YES completion:^{
        [SVProgressHUD dismiss];
    }];
    
    

}
- (IBAction)findPhotoAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:picker animated:YES completion:^{
        [SVProgressHUD dismiss];
    }];
    
    
    
}


- (IBAction)sendAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] uploadPhoto:self.imageView.image withOrder:self.order andLocation:[LocationService sharedInstance].currentLocation block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        if(!error) {
            
            WaitClientArriveViewController *waitClientArriveViewController = [[WaitClientArriveViewController alloc] initWithNibName:@"WaitClientArriveViewController" bundle:nil];
            
            waitClientArriveViewController.order = self.order;
            
            [self.navigationController setViewControllers:@[waitClientArriveViewController] animated:YES];
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            
            ((UIButton*)sender).enabled = YES;
            [self alertWithError:error];
            
        }
    }];
    
}


#pragma mark - imagepicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    if(self.imageView.image != nil) {
        
        self.sendButton.enabled = YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
