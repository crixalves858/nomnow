//
//  SendPhotoViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 23/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "Order.h"
#import "CourierServiceManager.h"
#import "WaitClientArriveViewController.h"

@interface SendPhotoViewController : OrderStepViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) Order *order;

@end
