//
//  ArriveViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 27/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//


#import "Order.h"
#import "RatingViewController.h"
#import "CourierServiceManager.h"
#import "WaitClientArriveViewController.h"
#import "SendPhotoViewController.h"
#import "OrderStepViewController.h"
#import "THPinViewController.h"

@interface ArriveViewController : OrderStepViewController <THPinViewControllerDelegate>



@end
