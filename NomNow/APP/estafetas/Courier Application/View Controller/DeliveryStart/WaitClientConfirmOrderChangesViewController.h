//
//  WaitClientConfirmOrderChangesViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "Order.h"
#import "AppDelegate.h"
#import "CourierTabBarController.h"
#import "WaitingRequestViewController.h"
#import "ViewOrderDetailViewController.h"

@interface WaitClientConfirmOrderChangesViewController : OrderStepViewController

@property (nonatomic, strong) Order* order;


@end
