//
//  EditProductViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import "OrderProduct.h"

@interface EditProductViewController : CourierBaseViewController

@property (nonatomic, strong) OrderProduct *orderProduct;

@end
