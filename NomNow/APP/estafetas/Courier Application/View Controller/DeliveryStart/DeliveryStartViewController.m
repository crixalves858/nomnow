//
//  DeliveryStartViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 31/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "DeliveryStartViewController.h"

@interface DeliveryStartViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *waitTimeField;
@property (weak, nonatomic) IBOutlet UIButton *nextStepButton;
@property (strong, nonatomic) IBOutlet UIView *confirmButton;
@property (nonatomic) BOOL orderEdit;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewButtonConstraint;
@property (strong, nonatomic) NSMutableArray *quantity;
@end

@implementation DeliveryStartViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[LocationService sharedInstance] startUpdatingLocation];
    
    self.orderEdit = NO;
    
    [self addKeyBoardObserver];
    
    if(!self.quantity) {
        
        self.quantity = [[NSMutableArray alloc] init];
        
        for(int i =0; i < self.order.products.count ; i++) {
            
            OrderProduct *orderProduct = self.order.products[0];
            [self.quantity addObject:orderProduct.quantity];
        }
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self updateUI];
}



- (void)editState {
    
    self.order.timeWaitRestaurant =  [NSNumber numberWithInteger:[self.waitTimeField.text integerValue] * 60];
    self.orderEdit = NO;
    for(int i =0; i < self.order.products.count ; i++) {
        
        OrderProduct *orderProduct = self.order.products[0];
        if(self.quantity[i] != orderProduct.quantity) {
            self.orderEdit = YES;
            break;
        }
    }
    
    if(!self.orderEdit) {
        
        for(int i = 0; i< self.order.products.count; i++) {
            OrderProduct *orderProduct = self.order.products[0];
            
            if([orderProduct.realPrice doubleValue] != [orderProduct.product.price doubleValue]) {
                
                self.orderEdit = YES;
                
            }
            
        }
    }
    
    if(self.orderEdit) {
        
        self.nextStepButton.enabled = NO;
        
    } else {
        
        self.nextStepButton.enabled = YES;
    }
}

- (void) updateUI {
    
    
    [self editState];
    
    [self.tableView reloadData];
    
    self.navigationItem.title = @"Confirmar Encomenda";
    
    self.phoneNumberLabel.text = [NSString stringWithFormat:@"%@",self.order.restaurant.phoneNumber];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateUI];
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.order.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",orderProduct.realPrice ];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditProductViewController *editProductViewController = [[EditProductViewController alloc] initWithNibName:@"EditProductViewController" bundle:nil];
    
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    editProductViewController.orderProduct= orderProduct;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:editProductViewController];
    
    [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    nav.navigationBar.translucent = NO;
    
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (IBAction)nextStepButtonAction:(id)sender {
    
    UIButton *button = (UIButton*) sender;
    
    button.enabled = NO;
    
    GoToRestaurantViewController *goToRestaurantViewController = [[GoToRestaurantViewController alloc] initWithNibName:@"GoToRestaurantViewController" bundle:nil];
    
    goToRestaurantViewController.order = self.order;
    
    [self.navigationController setViewControllers:@[goToRestaurantViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)confirmButtonAction:(id)sender {

    OrderState state;
    
    UIButton *button = (UIButton*) sender;
    
    button.enabled = NO;
    
    if(!self.orderEdit) {
        
        state = OrderStateClientAccept;
        
    } else {
        
        state = OrderStateUpdateWaitingClientAccept;
    }
    
    self.order.timeWaitRestaurant = [NSNumber numberWithInteger:[self.waitTimeField.text integerValue]*60];
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:state andLocation:[LocationService sharedInstance].currentLocation  withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
            
            WaitClientConfirmOrderChangesViewController *waitClientConfirmOrderChangesViewController = [[WaitClientConfirmOrderChangesViewController alloc] initWithNibName:@"WaitClientConfirmOrderChangesViewController" bundle:nil];
            
            waitClientConfirmOrderChangesViewController.order = self.order;
            
            [self.navigationController setViewControllers:@[waitClientConfirmOrderChangesViewController] animated:YES];
        
            [self dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            
            [self alertWithError:error];
            
            button.enabled = YES;
        }
        
    }];
    
}

#pragma mark - Actions

- (IBAction)tapPhoneAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.order.restaurant.phoneNumber]]];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.waitTimeField) {
        [textField resignFirstResponder];
    }
    return NO;
}


#pragma mark - Keyboard

- (IBAction)tapView:(id)sender {
    
    [self.waitTimeField endEditing:YES];
}


- (void) addKeyBoardObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note {
    
    CGRect editViewFrame = self.waitTimeField.frame;
    CGRect viewLogin = self.waitTimeField.superview.frame;
    CGRect viewFrame = self.view.frame;
    
    NSDictionary* keyboardInfo = [note userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    
    CGFloat finishText = editViewFrame.size.height + editViewFrame.origin.y + /*viewFrame.origin.y +*/ viewLogin.origin.y;
    
    NSTimeInterval duration = [[note userInfo][UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    if((viewFrame.size.height - keyboardFrameBeginRect.size.height) < finishText) {
        
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            self.contentViewButtonConstraint.constant = finishText - (viewFrame.size.height - keyboardFrameBeginRect.size.height);
            self.contentViewTopConstraint.constant = - (finishText - (viewFrame.size.height - keyboardFrameBeginRect.size.height));
            
            
        } completion:nil];
        
        
    }
    [self.view layoutIfNeeded];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSTimeInterval duration = [[notification userInfo][UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.contentViewButtonConstraint.constant =0;
        self.contentViewTopConstraint.constant = 0;
        
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}


#pragma mark -  UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.tableView]) {
        
        return NO;
    }
    
    return YES;
}



@end
