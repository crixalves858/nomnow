//
//  WaitClientConfirmOrderChangesViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "WaitClientConfirmOrderChangesViewController.h"

@interface WaitClientConfirmOrderChangesViewController ()

@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (nonatomic, strong) NSTimer *timer;
@end

@implementation WaitClientConfirmOrderChangesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    [self startService];
}

- (NSArray *) stringLabelState {
    
    return @[@"À espera que o cliente aceite a alteração", @"A alteração foi confirmada", @"O cliente recusou a alteração"];
}

- (NSArray *) stringButtonTextState {
    
    return @[@"Ir para o restaurante", @"Ok", @"Realizar Pedido da encomenda"];
}

- (void) updateUI {
    
    if(self.order.state == OrderStateUpdateWaitingClientAccept || self.order.state == OrderStateUpdateWaitingClientAcceptInRestaurant) {
        
        self.navigationItem.title = @"Aguardar";
        
        self.stateLabel.text = [self stringLabelState][0];
        
        self.actionButton.hidden = YES;
        
        self.spinner.hidden = NO;
        
    } else if(self.order.state == OrderStateClientAccept){
        
        self.navigationItem.title = @"Confirmado";
        
        self.stateLabel.text = [self stringLabelState][1];
        
        [self.actionButton setTitle: [self stringButtonTextState][0] forState: UIControlStateNormal];
        
        self.actionButton.hidden = NO;
        
        self.spinner.hidden = YES;
        
    } else if(self.order.state == OrderStateFailClientNotAccept) {
        
        self.navigationItem.title = @"Rejeitado";
        
        self.stateLabel.text = [self stringLabelState][2];
        
        [self.actionButton setTitle: [self stringButtonTextState][1] forState: UIControlStateNormal];
        
        self.actionButton.hidden = NO;
        
        self.spinner.hidden = YES;
        
    } else if(self.order.state == OrderStateClientAcceptInRestaurant){
        
        self.navigationItem.title = @"Confirmado";
        
        self.stateLabel.text = [self stringLabelState][1];
        
        [self.actionButton setTitle: [self stringButtonTextState][2] forState: UIControlStateNormal];
        
        self.actionButton.hidden = NO;
        
        self.spinner.hidden = YES;
    }
    
    
}

- (void) startService {
    
    if(!(self.order.state == OrderStateClientAccept || self.order.state == OrderStateFailClientNotAccept || self.order.state == OrderStateClientAcceptInRestaurant) ) {
        
        if(!self.timer) {
            
            self.timer = [NSTimer scheduledTimerWithTimeInterval: TIMER_STEP target:self
                                                        selector:@selector(timerTick) userInfo:nil repeats: YES];
        }
    }
    
}

- (void) stopService {
    
    [self.timer invalidate];
    self.timer = nil;
}


- (void) timerTick {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self.order fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            
            if(!error) {
                
                self.order = (Order*)object;
                
                if(self.order.state == OrderStateClientAccept || self.order.state == OrderStateFailClientNotAccept || self.order.state == OrderStateClientAcceptInRestaurant ) {
                    
                    [self stopService];
                    
                }
               
                [self updateUI];
                
            }
            
        }];
        
    });
    
}

- (IBAction)actionButtonAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    if(self.order.state == OrderStateClientAccept){
        
        [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
        
        [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateGoToRestaurant andLocation:[LocationService sharedInstance].currentLocation  withBlock:^(BOOL succeeded, NSError *error) {
            
            [SVProgressHUD dismiss];
            
            if (!error) {
                
                GoToRestaurantViewController *goToRestaurantViewController = [[GoToRestaurantViewController alloc] initWithNibName:@"GoToRestaurantViewController" bundle:nil];
                
                goToRestaurantViewController.order = self.order;
                
                [self.navigationController setViewControllers:@[goToRestaurantViewController] animated:YES];
                
                [self dismissViewControllerAnimated:NO completion:nil];
                
            } else {
                
                ((UIButton*)sender).enabled = YES;
                
                [self alertWithError:error];
            }
            
        }];
        
    } else if(self.order.state == OrderStateFailClientNotAccept) {
        
        WaitingRequestViewController *waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];
        
        [self.navigationController setViewControllers:@[waitingRequestViewController] animated:YES];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
    } else if(self.order.state == OrderStateClientAcceptInRestaurant){
        
        ViewOrderDetailViewController *viewOrderDetailViewController = [[ViewOrderDetailViewController alloc] initWithNibName:@"ViewOrderDetailViewController" bundle:nil];
        
        viewOrderDetailViewController.order = self.order;
        
        [self.navigationController setViewControllers:@[viewOrderDetailViewController] animated:YES];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
    
}

@end
