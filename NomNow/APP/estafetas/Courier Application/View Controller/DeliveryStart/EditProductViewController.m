//
//  EditProductViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "EditProductViewController.h"

@interface EditProductViewController ()
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UITextField *quantityField;
@property (weak, nonatomic) IBOutlet UITextView *observationsField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation EditProductViewController


- (void)navigationBarSetup {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissAction)];
    UIBarButtonItem *addProductTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(saveProductAction)];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=addProductTabButton;
    self.navigationItem.title=@"Editar Producto";
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self updateUI];
    
    [self navigationBarSetup];

}

- (void) updateUI {
    
    [self.observationsField.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.observationsField.layer setBorderWidth:1.0];
    
    self.observationsField.layer.cornerRadius = 5;
    self.observationsField.clipsToBounds = YES;
    
    self.priceField.text = [NSString stringWithFormat:@"%@",self.orderProduct.realPrice];
    self.quantityField.text = [NSString stringWithFormat:@"%@", self.orderProduct.quantity];
    self.nameLabel.text = self.orderProduct.product.name;
    self.observationsField.text = self.orderProduct.observations;
}

- (void) saveProductAction {
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    NSNumber *quatity = [NSNumber numberWithInteger:[self.quantityField.text integerValue]];
    NSNumber *price = [NSNumber numberWithInteger:[self.priceField.text doubleValue]];
    self.orderProduct.realPrice = price;
    self.orderProduct.quantity = quatity;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) dismissAction {
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
