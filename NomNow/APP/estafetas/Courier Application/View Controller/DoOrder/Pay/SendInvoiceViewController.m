//
//  SendInvoiceViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 21/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "SendInvoiceViewController.h"

@interface SendInvoiceViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageVIew;
@property (weak, nonatomic) IBOutlet UIButton *takePhotButton;
@property (weak, nonatomic) IBOutlet UIButton *findPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation SendInvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[LocationService sharedInstance] startUpdatingLocation];
    
    self.navigationItem.title = @"Enviar recibo";
}

- (IBAction)takePhotoAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;

    [self presentViewController:picker animated:YES completion:^{
        
        [SVProgressHUD dismiss];
        
    }];
    
    
}

- (IBAction)findPhotoAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        
        [SVProgressHUD dismiss];
    
    }];
}

- (IBAction)sendAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] uploadInvoice:self.imageVIew.image withOrder:self.order block:^(BOOL succeeded, NSError *error) {
        if(!error) {
            
            [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStatePay andLocation:[[LocationService sharedInstance] currentLocation] withBlock:^(BOOL succeeded, NSError *error) {
                
                [SVProgressHUD dismiss];
                
                if (!error) {
                    
                    GoToClientViewController *goToClientViewController = [[GoToClientViewController alloc] init];
                    
                    goToClientViewController.order = self.order;
                    
                    [self.navigationController setViewControllers:@[goToClientViewController] animated:YES];
                    
                    [self dismissViewControllerAnimated:NO completion:nil];
                    
                } else {
                    
                    ((UIButton*)sender).enabled = YES;
                    [self alertWithError:error];
                }
                
            }];
        }
    }];
}

#pragma mark - imagepicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageVIew.image = chosenImage;
    
    if(self.imageVIew.image != nil) {
        
        self.sendButton.enabled = YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
