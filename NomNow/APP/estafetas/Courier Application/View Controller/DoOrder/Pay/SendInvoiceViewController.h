//
//  SendInvoiceViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 21/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "Order.h"
#import "CourierServiceManager.h"
#import "GoToClientViewController.h"

@interface SendInvoiceViewController : OrderStepViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) Order *order;

@end
