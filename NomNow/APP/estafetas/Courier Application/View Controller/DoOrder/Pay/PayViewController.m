//
//  PayViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 26/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "PayViewController.h"


@interface PayViewController ()

@end

@implementation PayViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.title = @"NIF";
    
}

#pragma mark - actions

- (IBAction)payButtonAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    SendInvoiceViewController *sendInvoiceViewController = [[SendInvoiceViewController alloc] init];
    
    sendInvoiceViewController.order = self.order;
    
    [self.navigationController pushViewController:sendInvoiceViewController animated:YES];
    
}
@end
