//
//  ViewOrderDetailViewController.m
//  estafetas
//
//  Created by Cristiano on 03/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ViewOrderDetailViewController.h"
#import "ProductInCartTableViewCell.h"

@interface ViewOrderDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewOrderDetailViewController


- (void)navigationBarSetup {
    
    UIBarButtonItem *payTabButton = [[UIBarButtonItem alloc] initWithTitle:@"Pagar" style:UIBarButtonItemStylePlain target:self action:@selector(payAction)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [array addObject:payTabButton];
    
    for(int i=0;i<self.navigationItem.rightBarButtonItems.count; i++) {
        
        [array addObject:self.navigationItem.rightBarButtonItems[i]];
    }
    
    
    
    self.navigationItem.rightBarButtonItems = [array copy];
    
    self.navigationItem.title = @"Encomenda";
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}


- (void) payAction {
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    PayViewController *payViewController = [[PayViewController alloc] initWithNibName:@"PayViewController" bundle:nil];
    
    payViewController.order = self.order;

    [self.navigationController pushViewController:payViewController animated:YES];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self navigationBarSetup];
}


#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.order.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",orderProduct.realPrice];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
    
    
    return cell;
    
}



@end
