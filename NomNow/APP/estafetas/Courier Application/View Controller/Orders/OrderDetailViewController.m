//
//  OrderDetailViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "ProductInCartTableViewCell.h"

@interface OrderDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL load;

@end

@implementation OrderDetailViewController

- (void)loadData {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    self.load = NO;
    for(int i=0;i<self.order.products.count; i++) {
        
        OrderProduct *product = self.order.products[i];
        
        [array addObject:product.product];
        
    }
    
    [PFObject fetchAllIfNeededInBackground:array block:^(NSArray *product, NSError *error) {
        
        self.load = YES;
        [self.tableView reloadData];
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.navigationItem.title = @"Detalhes";
    
    [self loadData];
    
}


#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.load) {
        
        return self.order.products.count;
    } else {
        
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",orderProduct.realPrice ];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
    
    return cell;
    
    
}





@end
