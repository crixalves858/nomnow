//
//  OrdersCourierNavigationController.m
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrdersCourierNavigationController.h"

@interface OrdersCourierNavigationController ()

@end

@implementation OrdersCourierNavigationController

- (id) init{
    
    OrdersCourierViewController *ordersCourierViewController = [[OrdersCourierViewController alloc] initWithNibName:@"OrdersCourierViewController" bundle:nil];
    
    
    self = [super initWithRootViewController:ordersCourierViewController];
    
    self.navigationBar.translucent = NO;
    
    return self;
}

@end
