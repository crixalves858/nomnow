//
//  OrdersCourierViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrdersCourierViewController.h"
#import "OrderTableViewCell.h"

@interface OrdersCourierViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *orders;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation OrdersCourierViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initValues];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.navigationItem.title = @"Entregas";

}


- (void) initValues {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualizar..."];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self loadData];
    
}


#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Order *order = self.orders[indexPath.row];
    
    cell.restaurantNameLabel.text = order.restaurant.name;
    cell.orderDateLabel.text = [self stringFromDate:order.orderDate];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OrderCourierViewController *orderCourierViewController = [[OrderCourierViewController alloc] initWithNibName:@"OrderCourierViewController" bundle:nil];
    
    
    orderCourierViewController.order = self.orders[indexPath.row];
    
    [self.navigationController pushViewController:orderCourierViewController animated:YES];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadData];
}

- (void)loadData {
    
    [self.refreshControl beginRefreshing];
    
    [[CourierServiceManager sharedInstance]myOrdersWithBlock:^(NSArray *objects, NSError *error) {
        [self.refreshControl endRefreshing];
        if(!error) {
            
            self.orders = [NSMutableArray arrayWithArray:objects];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderDate" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];

            self.orders = [[NSMutableArray alloc] initWithArray:[self.orders sortedArrayUsingDescriptors:sortDescriptors]];
            
            [self.tableView reloadData];
        } else {
            
            [self alertWithError:error];
        }
    }];
}

- (NSString *)stringFromDate:(NSDate*)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm - dd/MM/yyy"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
    
}

@end
