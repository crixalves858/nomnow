//
//  OrdersCourierViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import "CourierServiceManager.h"
#import "OrderCourierViewController.h"

@interface OrdersCourierViewController : CourierBaseViewController

@end
