//
//  OrdersCourierNavigationController.h
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdersCourierViewController.h"


@interface OrdersCourierNavigationController : UINavigationController

@end
