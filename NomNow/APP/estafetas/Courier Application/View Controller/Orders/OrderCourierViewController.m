//
//  OrderCourierViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 07/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderCourierViewController.h"

@interface OrderCourierViewController ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *comissionLabel;
@property (weak, nonatomic) IBOutlet UILabel *comissionStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *ratingRestaurant;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *ratingClient;
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLabel;
@property (weak, nonatomic) IBOutlet UIView *comissionView;

@end

@implementation OrderCourierViewController

- (NSArray*) orderStateText {
    
    return @[@"À espera de estafeta",
             @"Encontramos um estafeta para si",
             @"O estafeta actualizou a encomenda. Aceite ou rejeite a alteração",
             @"A encomenda está confirmada",
             @"O estafeta está a dirigir-se para o restaurante.",
             @"O estafeta chegou ao restaurante",
             @"O estafeta alterou a encomenda. Ele encontra-se no restaurante à espera que aceite",
             @"A encomenda está confirmada, e a ser finalizada no restaurante.",
             @"A encomenda está confirmada, e a ser finalizada no restaurante.",
             @"O estafeta está a dirigir-se para o local da entrega.",
             @"O cliente chegou ao local da entrega, dirija-se até ao local.",
             @"O estafeta não o esta a conseguir encontrar, verifique a fotografia que o estafeta enviou e dirija-se ao local.",
             @"A encomenda foi entregue",
             @"A entrega falhou",
             @"A falha na entrega encontra-se a ser analisada",
             @"A entrega falhou por falha do cliente. Receberá a comissão",
             @"A entrega falhou por sua falha, não receberá a comissão",
             @"Não encontramos estafetas para o seu pedido :-(",
             @"O cliente rejeitou a alteração, a encomenda foi cancelada",
             @"À espera de estafeta",
             @"Encomenda cancelada",
             @"O restaurante encontrava-se fechado. A encomenda foi cancelada.",
             @"A encomenda foi cancelada pelo cliente"
             ];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    
    self.navigationItem.title = self.order.restaurant.name;
}

- (void) updateUI {
    
    self.dateLabel.text = [self dateToString:self.order.deliveryDate];
    
    self.restaurantNameLabel.text = self.order.restaurant.name;
    
    self.clientName.text = self.order.client.client.firstName;
    
    self.comissionLabel.text = [NSString stringWithFormat:@"%.02f €",[self.order.commission.value doubleValue]];
    
    if([self.order.commission.state intValue] == 0) {
        
        self.comissionStateLabel.text = @"Não pago";
        
    } else {
        
        self.comissionStateLabel.text = @"Pago";
    }
    
    NSString *stateStr = self.orderStateText[self.order.state];
    
    self.orderStateLabel.text = stateStr;
    
    [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        
        if(!error) {
            
            self.clientImage.image = [UIImage imageWithData:data];
        }
    }];
    
    
    [self.order.restaurant.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        
        if(!error) {
            
            self.restaurantImage.image = [UIImage imageWithData:data];
        }
    }];
    
    self.ratingClient.enabled = NO;
    self.ratingRestaurant.enabled = NO;
    self.ratingRestaurant.rating = [self.order.restaurant.rating integerValue];
    self.ratingClient.rating = [self.order.client.client.classification integerValue];
    
    if(self.order.state == OrderStateDeliveryFailCourier || self.order.state == OrderStateFailClientNotAccept || self.order.state ==  OrderStateCanceleRestaurantClose) {
        
        self.comissionView.hidden = YES;
    } else {
        
        self.comissionView.hidden = NO;
    }
    
}

- (IBAction)ratingButtonAction:(id)sender {
    
    RatingViewController *ratingViewController = [[RatingViewController alloc] initWithNibName:@"RatingViewController" bundle:nil];
    
    ratingViewController.order = self.order;
    
    [self.navigationController pushViewController:ratingViewController animated:YES];
    
}
- (IBAction)orderDetail:(id)sender {
    
    OrderDetailViewController *orderDetailViewController = [[OrderDetailViewController alloc] initWithNibName:@"OrderDetailViewController" bundle:nil];
    
    orderDetailViewController.order = self.order;
    
    [self.navigationController pushViewController:orderDetailViewController animated:YES];
}

- (NSString *) dateToString:(NSDate*)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMM dd HH:mm:ss ZZZ yyyy"];
    
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"dd-MM-yyyy | HH:mm:ss"];
    NSString *newDate = [dateFormatter stringFromDate:date];
    
    return newDate;
}

@end
