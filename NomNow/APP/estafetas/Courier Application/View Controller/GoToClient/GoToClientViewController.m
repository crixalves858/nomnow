//
//  GoToClientViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 27/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "GoToClientViewController.h"

@interface GoToClientViewController ()

@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *zipcodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *doorNumberLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *observationsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;

@end

@implementation GoToClientViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self updateUI];
    
    [[LocationService sharedInstance] startUpdatingLocation];
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"currentLocation" options:NSKeyValueObservingOptionNew context:nil];
    
    [self annotationMark];
}


- (void) updateUI {

    self.navigationItem.title = @"Entregar";
    
    self.clientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.order.client.client.firstName, self.order.client.client.lastName];
    
    self.zipcodeLabel.text = self.order.deliveryZipCode;
    
    self.doorNumberLabel.text = [NSString stringWithFormat:@"%@", self.order.deliveryDoorNumber];
    
    self.deliveryAddressLabel.text = self.order.deliveryAddress;
    
    if(self.order.state == OrderStatePay) {
        
        [self.actionButton setTitle:@"Iniciar Viagem" forState:UIControlStateNormal];
    } else {
        
        [self.actionButton setTitle:@"Cheguei" forState:UIControlStateNormal];
    }
    
    self.floorLabel.text =  [NSString stringWithFormat:@"%@", self.order.deliveryFloor];
    
    if(self.order.client.client.photo != nil) {
        
        [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                self.clientImage.image = [UIImage imageWithData:data];
            }
        }];
    }
    
    if([ self.order.deliveryObservations isEqualToString:@""]) {
        
        self.observationsLabel.text = @"Sem observações";
        
    } else {
        
        self.observationsLabel.text = self.order.deliveryObservations;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object  change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"currentLocation"]) {
        
        [[CourierSession sharedInstance] currentUser].courier.clientData.location = [PFGeoPoint geoPointWithLocation:[[LocationService sharedInstance] currentLocation]];
        
        [self directionSetup];
    }
}

- (void) arraive {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateArrived andLocation:[LocationService sharedInstance].currentLocation  withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
            
            ArriveViewController *arriveViewController = [[ArriveViewController alloc] initWithNibName:@"ArriveViewController" bundle:nil];
            
            arriveViewController.order = self.order;
            
            [self.navigationController setViewControllers:@[arriveViewController] animated:YES];
                        
            [self dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

- (void) startTrip {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateGoToClient andLocation:[LocationService sharedInstance].currentLocation withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
           
            self.order.state = OrderStateGoToClient;
            
            [self updateUI];
        } else {
            
            [self alertWithError:error];
        }
        
    }];

}

- (IBAction)actionButtonAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    if(self.order.state == OrderStatePay) {
        
        [self startTrip];
    } else {
        
        [self arraive];
    }
    
    
    
    ((UIButton*)sender).enabled = YES;
}

- (IBAction)openMapAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    [self openMap];
    
    ((UIButton*)sender).enabled = YES;
    
}

#pragma mark - Map

- (void)annotationMark {
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = [[CLLocation alloc] initWithLatitude:[self.order.deliveryLocation latitude] longitude:[self.order.deliveryLocation longitude]].coordinate;
    point.title = self.order.client.client.firstName;
    point.subtitle = self.order.client.client.lastName;
    
    [self.mapView addAnnotation:point];
}


- (void)directionSetup {
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    CLLocationCoordinate2D deliveryLocation = CLLocationCoordinate2DMake(self.order.deliveryLocation.latitude, self.order.deliveryLocation.longitude);
    
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:deliveryLocation addressDictionary:nil];
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:[[LocationService sharedInstance] currentLocation].coordinate addressDictionary:nil];
    
    request.source =[[MKMapItem alloc]initWithPlacemark:sourcePlacemark];
    request.destination = [[MKMapItem alloc]initWithPlacemark:destinationPlacemark];
    
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error) {
        
        if (error) {
             
             [self alertWithError:error];
         
         } else {
         
             [self showRoute:response];
         }
     }];
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay {
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    
    renderer.strokeColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    renderer.lineWidth = 5.0;
    
    return renderer;
}

-(void)showRoute:(MKDirectionsResponse *)response {
    
    MKRoute *route = [response.routes firstObject];
    
    [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
    
    [self.mapView setVisibleMapRect:[[route polyline]boundingMapRect] animated:YES];
    
}


- (void) openMap {
    
    CLLocationCoordinate2D deliveryLocation = CLLocationCoordinate2DMake(self.order.deliveryLocation.latitude, self.order.deliveryLocation.longitude);
    
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:deliveryLocation addressDictionary:nil];
    
    MKMapItem *destinationItem = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    
    destinationItem.name = self.order.client.client.firstName;
    
    destinationItem.phoneNumber = [NSString stringWithFormat:@"%@",self.order.client.client.courierData.phoneNumber];
    
    NSDictionary *options = @{
                              MKLaunchOptionsDirectionsModeKey:
                                  MKLaunchOptionsDirectionsModeDriving,
                              MKLaunchOptionsMapTypeKey:
                                  [NSNumber numberWithInteger:MKMapTypeStandard],
                              MKLaunchOptionsShowsTrafficKey:@YES,
                              };
    
    [MKMapItem openMapsWithItems:@[destinationItem] launchOptions:options];
    
}

- (void) dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    
    [[LocationService sharedInstance] removeObserver:self forKeyPath:@"currentLocation"];
    [super dismissViewControllerAnimated:flag completion:completion];
}


@end
