//
//  ProfilleCourierViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 21/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ProfileCourierViewController.h"

@interface ProfileCourierViewController ()

@end

@implementation ProfileCourierViewController

- (void)viewDidLoad {
    [super viewDidLoad];


}

- (IBAction)logout:(id)sender {
    
    [[MessageCourierService sharedInstance] stopService];
    
    [[CourierServiceManager sharedInstance] logout];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] init];
    
    [appDelegate changeRootViewController:loginNavigationController animated:YES];
    
    [self.tabBarController dismissViewControllerAnimated:NO completion:nil];
    
}



@end
