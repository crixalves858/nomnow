//
//  ProfilleCourierViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 21/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import "CourierServiceManager.h"
#import "LoginNavigationController.h"
#import "MessageCourierService.h"

@interface ProfileCourierViewController : CourierBaseViewController

@end
