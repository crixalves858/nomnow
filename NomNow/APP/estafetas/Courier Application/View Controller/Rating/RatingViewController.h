//
//  RatingViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 06/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import <FinalStarRatingBar.h>	
#import "Order.h"
#import "CourierServiceManager.h"
#import "WaitingRequestViewController.h"

@interface RatingViewController : CourierBaseViewController

@property (nonatomic, strong) Order *order;

@end
