//
//  RatingViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 06/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RatingViewController.h"

@interface RatingViewController ()
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *ratingButtons;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clientImageView;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *oldRatingView;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;

@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initValues];
    [self updateUI];
    self.navigationItem.title = @"Avaliar";
}

- (void) updateUI {
    
    self.clientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.order.client.client.firstName, self.order.client.client.lastName];
    

    self.oldRatingView.enabled = NO;
    self.oldRatingView.rating = [self.order.client.client.classification integerValue];
    
    [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if(!error) {
            
            self.clientImageView.image = [UIImage imageWithData:data];
        }
    }];
    
    if(self.navigationController.viewControllers.count == 1) {
        
        self.homeButton.hidden = NO;
    } else {
        
        self.homeButton.hidden = YES;
    }
    
}

- (void) initValues {
 
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] getRatingWithReviewer:[CourierSession sharedInstance].currentUser rated:self.order.client andOrder:self.order block:^(Rating *rating, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            self.ratingButtons.rating = [rating.value integerValue];
        } else {
            
            [self alertWithError:error];
        }
    }];
    
    [self.ratingButtons setRatingChangedBlock:^(NSUInteger rating) {
        
        [[CourierServiceManager sharedInstance] setRatingWithReviewer:[CourierSession sharedInstance].currentUser rated:self.order.client  order:self.order andRating:rating block:^(Rating *rating, NSError *error) {
            if(error) {
                
                [self alertWithError:error];
            }
        }];
    }];
}

- (IBAction)homeButtonAction:(id)sender {
    
    WaitingRequestViewController *waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];
    
    [self.navigationController setViewControllers:@[waitingRequestViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}




@end
