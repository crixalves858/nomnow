//
//  OrderStepViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 18/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "WaitingRequestViewController.h"

@interface OrderStepViewController ()

@end

@implementation OrderStepViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCancelNotification) name:[NSString stringWithFormat:@"OrderCancel%@", self.order.objectId] object:nil];
    
}

- (void) viewDidLayoutSubviews {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect frame = self.view.frame;
    
    if(frame.size.height+self.navigationController.tabBarController.tabBar.frame.size.height+self.tabBarController.tabBar.frame.size.height <= screenHeight) {
        
        frame.size.height +=self.tabBarController.tabBar.frame.size.height;
        self.view.frame = frame;
        
    }
}

- (void) openCancelNotification {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Encomenda cancelada"
                                                       message:@"A encomenda foi cancelada pelo cliente" delegate:self
                                             cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
        
        [self goToHome];
    }
    
}

- (void) goToHome {
    
    WaitingRequestViewController *waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];
    
    [self.navigationController setViewControllers:@[waitingRequestViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

@end
