
//
//  ConfirmOrderViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 26/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ConfirmOrderViewController.h"
#import "ProductInCartTableViewCell.h"

@interface ConfirmOrderViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *quantity;
@property (strong, nonatomic) NSMutableArray *price;

@end

@implementation ConfirmOrderViewController


- (void) setupNavigationBar {
    
    UIBarButtonItem *cloreRestaurantTabButton =[[UIBarButtonItem alloc] initWithTitle:@"Fechado" style:UIBarButtonItemStylePlain target:self action:@selector(closeRestaurantAction)];
    
    self.navigationItem.title = @"Confirmar";
    self.navigationItem.leftBarButtonItem=cloreRestaurantTabButton;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupNavigationBar];
    
    [[LocationService sharedInstance] startUpdatingLocation];
    
    if(!self.quantity) {
        
        self.quantity = [[NSMutableArray alloc] init];
        self.price = [[NSMutableArray alloc] init];
        
        for(int i =0; i < self.order.products.count ; i++) {
            
            OrderProduct *orderProduct = self.order.products[0];
            [self.quantity addObject:orderProduct.quantity];
            [self.price addObject:orderProduct.realPrice];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (IBAction)confirmOrderAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    [self confirmRequest];
    
    ((UIButton*)sender).enabled = YES;
}

- (void)closeRestaurant {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] restaurantCloseWithOrder:self.order block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Encomenda terminada" message:@"A entrega terminou" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            
        } else {
            
            [self alertWithError:error];
        }
    }];
}

-(void) closeRestaurantAction  {
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restaurante fechado" message:@"Confirma que o restaurante esta fechado?" delegate:self cancelButtonTitle:@"Sim" otherButtonTitles:@"Cancelar", nil];
    [alert show];
      
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if([alertView.title isEqualToString:@"Restaurante fechado"]) {
        
        if (buttonIndex == 0){
            
            [self closeRestaurant];
        }
        self.navigationItem.leftBarButtonItem.enabled = YES;
        
    } else {
        
        if (buttonIndex == 0){
            
            [self goToHome];
        }
    }
    
}

- (void) goToHome {
    
    WaitingRequestViewController *waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];
    
    [self.navigationController setViewControllers:@[waitingRequestViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void) confirmRequest {
    
    BOOL orderEdit = NO;
    
    for(int i =0; i < self.order.products.count ; i++) {
        
        OrderProduct *orderProduct = self.order.products[0];
        if(self.quantity[i] != orderProduct.quantity) {
            orderEdit = YES;
            break;
            
        }
    }
    
    if(!orderEdit) {
        
        for(int i = 0; i< self.order.products.count; i++) {
            OrderProduct *orderProduct = self.order.products[0];
            NSNumber *price = self.price[i];
            if([price doubleValue] != [orderProduct.realPrice doubleValue]) {
                
                orderEdit = YES;
                
            }
            
        }
    }
    
    OrderState state;
    
    if(!orderEdit) {
        
        state = OrderStateClientAcceptInRestaurant;
        
    } else {
        
        state = OrderStateUpdateWaitingClientAcceptInRestaurant;
    }
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:state andLocation:[[LocationService sharedInstance] currentLocation]  withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
            
            if(state == OrderStateClientAcceptInRestaurant) {
                
                ViewOrderDetailViewController *viewOrderDetailViewController = [[ViewOrderDetailViewController alloc] initWithNibName:@"ViewOrderDetailViewController" bundle:nil];
                
                viewOrderDetailViewController.order = self.order;
                
                [self.navigationController setViewControllers:@[viewOrderDetailViewController] animated:YES];
                
                [self dismissViewControllerAnimated:NO completion:nil];
                
                
            } else {
                
                WaitClientConfirmOrderChangesViewController *waitClientConfirmOrderChangesViewController = [[WaitClientConfirmOrderChangesViewController alloc] initWithNibName:@"WaitClientConfirmOrderChangesViewController" bundle:nil];
                
                waitClientConfirmOrderChangesViewController.order = self.order;
                
                [self.navigationController setViewControllers:@[waitClientConfirmOrderChangesViewController] animated:YES];
                
                [self dismissViewControllerAnimated:NO completion:nil];
                
            }
            
            
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.order.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",orderProduct.realPrice];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];

    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditProductViewController *editProductViewController = [[EditProductViewController alloc] initWithNibName:@"EditProductViewController" bundle:nil];
    
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    editProductViewController.orderProduct= orderProduct;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:editProductViewController];
    
    [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    nav.navigationBar.translucent = NO;
    
    [self presentViewController:nav animated:YES completion:nil];
    
}



@end
