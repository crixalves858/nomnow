//
//  ConfirmOrderViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 26/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStepViewController.h"
#import "CourierSession.h"
#import "CourierServiceManager.h"
#import "Order.h"
#import "WaitClientConfirmOrderChangesViewController.h"
#import "AppDelegate.h"
#import "EditProductCartViewController.h"
#import "WaitingRequestViewController.h"

@interface ConfirmOrderViewController : OrderStepViewController


@end
