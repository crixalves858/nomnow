//
//  MessageBaseViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 16/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "MessageBaseViewController.h"

@interface MessageBaseViewController ()

@end

@implementation MessageBaseViewController

- (void)viewDidLoad {
    
    UIBarButtonItem *messageButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"messageIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(openMessage)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(int i=0;i<self.navigationItem.rightBarButtonItems.count; i++) {
        
        [array addObject:self.navigationItem.rightBarButtonItems[i]];
    }
    
    [array addObject:messageButton];
    
    self.navigationItem.rightBarButtonItems = [array copy];
    

    [super viewDidLoad];


}


- (void) openMessage {
    
    ChatViewController *messageViewController = [ChatViewController messagesViewController];
    
    messageViewController.order = self.order;
    
    [self.navigationController pushViewController:messageViewController animated:YES];
    
    
    
}




@end
