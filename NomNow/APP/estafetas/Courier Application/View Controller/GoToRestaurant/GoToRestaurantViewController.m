//
//  GoToRestaurantViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 26/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "GoToRestaurantViewController.h"

@interface GoToRestaurantViewController ()

@property (weak, nonatomic) IBOutlet UILabel *restaurantName;
@property (weak, nonatomic) IBOutlet UILabel *restaurantAddress;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *nextStateButton;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;

@end

@implementation GoToRestaurantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    [self annotationMark];
    [[LocationService sharedInstance] startUpdatingLocation];
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"currentLocation" options:NSKeyValueObservingOptionNew context:nil];
}

- (void) updateUI {
    
    if(self.order.state == OrderStateClientAccept || self.order.state == OrderStateHaveCourier || self.order.state == OrderStateWatingCourier) {
        
        [self.nextStateButton setTitle:@"Iniciar Viagem" forState:UIControlStateNormal];
        
    } else {
        
         [self.nextStateButton setTitle:@"Cheguei ao Restaurante" forState:UIControlStateNormal];
        
    }
    
    self.navigationItem.title = @"Ir para o Restaurante";
    
    if(self.order.restaurant.photo != nil) {
        
        [self.order.restaurant.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                self.restaurantImage.image = [UIImage imageWithData:data];
            }
        }];
    }
    
    self.restaurantName.text = self.order.restaurant.name;
    self.restaurantAddress.text = self.order.restaurant.address;
}

#pragma mark - Actions

- (IBAction)nextStateAction:(id)sender {

    ((UIButton*)sender).enabled = NO;
    
    if(self.order.state == OrderStateClientAccept || self.order.state == OrderStateHaveCourier || self.order.state == OrderStateWatingCourier) {
        
        [self goToRestaurant];
        
    } else {
        
        [self openInRestaurant];
        
    }
    
    ((UIButton*)sender).enabled = YES;
}

- (void) openInRestaurant {
    
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateInRestaurant andLocation:[LocationService sharedInstance].currentLocation withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
            
            ConfirmOrderViewController *confirmOrderViewController = [[ConfirmOrderViewController alloc] initWithNibName:@"ConfirmOrderViewController" bundle:nil];
            
            confirmOrderViewController.order = self.order;
            
            [self.navigationController setViewControllers:@[confirmOrderViewController] animated:YES];
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

- (void) goToRestaurant {
    
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] updateOrder:self.order state:OrderStateGoToRestaurant andLocation:[LocationService sharedInstance].currentLocation withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!error) {
            
            [self updateUI];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

- (IBAction)openMapAction:(id)sender {
    
    ((UIButton*)sender).enabled = NO;
    
    [self openMap];
    
    ((UIButton*)sender).enabled = YES;
}

- (void) openMap {
    
    CLLocationCoordinate2D restaurantLocation = CLLocationCoordinate2DMake(self.order.restaurant.location.latitude, self.order.restaurant.location.longitude);
   
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:restaurantLocation addressDictionary:nil];

    MKMapItem *destinationItem = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    
    destinationItem.name = self.order.restaurant.name;
    
    destinationItem.phoneNumber = [NSString stringWithFormat:@"%@",self.order.restaurant.phoneNumber];
        
    NSDictionary *options = @{
                              MKLaunchOptionsDirectionsModeKey:
                                  MKLaunchOptionsDirectionsModeDriving,
                              MKLaunchOptionsMapTypeKey:
                                  [NSNumber numberWithInteger:MKMapTypeStandard],
                              MKLaunchOptionsShowsTrafficKey:@YES,
                              };
    
    [MKMapItem openMapsWithItems:@[destinationItem] launchOptions:options];
    
}

#pragma mark - Map

- (void)annotationMark {
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = [[CLLocation alloc] initWithLatitude:[self.order.restaurant.location latitude] longitude:[self.order.restaurant.location longitude]].coordinate;
    point.title = self.order.restaurant.name;
    point.subtitle = @"Restaurante";
    
    [self.mapView addAnnotation:point];
}


- (void)directionSetup {
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    CLLocationCoordinate2D restaurantLocation = CLLocationCoordinate2DMake(self.order.restaurant.location.latitude, self.order.restaurant.location.longitude);
    
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:restaurantLocation addressDictionary:nil];
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:[[LocationService sharedInstance] currentLocation].coordinate addressDictionary:nil];
    
    request.source =[[MKMapItem alloc]initWithPlacemark:sourcePlacemark];
    request.destination = [[MKMapItem alloc]initWithPlacemark:destinationPlacemark];
    
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
       
         if (error) {
             
             [self alertWithError:error];
             
         } else {
             
             [self showRoute:response];
         }
     }];
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay {
    
    for (id<MKOverlay> overlayToRemove in mapView.overlays) {
        
        [self.mapView removeOverlay:overlayToRemove];
    }
    
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    
    renderer.strokeColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    renderer.lineWidth = 5.0;
    
    return renderer;
}

-(void)showRoute:(MKDirectionsResponse *)response {
   
    MKRoute *route = [response.routes firstObject];
    
    [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
    
    [self.mapView setVisibleMapRect:[[route polyline]boundingMapRect] animated:YES];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object  change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"currentLocation"]) {
        
        [self directionSetup];
    }
}

- (void) dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    
    [[LocationService sharedInstance] removeObserver:self forKeyPath:@"currentLocation"];
    [super dismissViewControllerAnimated:flag completion:completion];
}








@end
