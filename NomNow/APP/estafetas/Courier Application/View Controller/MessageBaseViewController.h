//
//  MessageBaseViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 16/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import "Order.h"
#import "ChatViewController.h"

@interface MessageBaseViewController : CourierBaseViewController

@property (nonatomic, strong) Order *order;

@end
