//
//  OrderRequestViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderRequestViewController.h"
#import "Constant.h"
#import <FinalStarRatingBar.h>
#import "OrderUpdateService.h"


@interface OrderRequestViewController ()

@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeToResponseLabel;
@property (weak, nonatomic) IBOutlet UILabel *comissionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *clientRating;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *restaurantRating;


@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) Client *client;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) NSDate *hourToResponse;

@end

@implementation OrderRequestViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self navigationBarSetupWithDoneAction:@selector(acceptOrder) cancelAction:@selector(refuseOrder) andTitle:@"PedidoRecebido"];
    [self initValues];
    [self updateUI];
    [self directionSetup];
    
}

- (void) updateUI {
    
    self.clientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.client.firstName, self.client.lastName];
    self.restaurantNameLabel.text = self.restaurant.name;
    double comission = ([self.eligibleCourier.order.distanceRestaurantClient doubleValue]+[self.eligibleCourier.distanceToRestaurant doubleValue])*PRICE_BY_METER;
    
    self.restaurantRating.enabled = NO;
    self.restaurantRating.rating = [self.eligibleCourier.order.restaurant.rating integerValue];
    
    self.clientRating.enabled = NO;
    self.clientRating.rating = [self.eligibleCourier.order.client.client.classification integerValue];
    
    if(self.eligibleCourier.order.restaurant.photo != nil) {
        
        [self.eligibleCourier.order.restaurant.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                self.restaurantImage.image = [UIImage imageWithData:data];
            }
        }];
    }

    if(self.eligibleCourier.order.client.client.photo != nil) {
        
        [self.eligibleCourier.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                self.clientImage.image = [UIImage imageWithData:data];
            }
        }];
    }

    
    self.comissionLabel.text = [NSString stringWithFormat:@"%.02f €", comission];
    [self annotationMark];
    [self updateTimeToResponse];
}

- (void) initValues {
    
    self.client = self.eligibleCourier.order.client.client;
    self.restaurant = self.eligibleCourier.order.restaurant;
    
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    [dc setSecond:TIME_TO_RESPONSE];
    
    self.hourToResponse = [[NSCalendar currentCalendar] dateByAddingComponents:dc toDate:self.eligibleCourier.lastContact options:0];
    
    NSLog(@"%@", self.hourToResponse);
    
    [self startTimer];
}


- (void) updateTimeToResponse {
    
    NSTimeInterval dateTime = [self.hourToResponse timeIntervalSinceNow];
    
    if(dateTime <= 0 ) {
        [self stopTimer];
        dateTime = 0;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            if ([self.delegate respondsToSelector:@selector(notResponse)]) {
                [self.delegate notResponse];
                
            }
        }];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
    
    int seconds = dateTime;
    self.timeToResponseLabel.text = [NSString stringWithFormat:@"%i s", seconds];
}


#pragma mark - Map

- (void)annotationMark {
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = [[CLLocation alloc] initWithLatitude:[self.restaurant.location latitude] longitude:[self.restaurant.location longitude]].coordinate;
    point.title = self.restaurant.name;
    point.subtitle = @"Restaurante";
    
    [self.mapView addAnnotation:point];
}


- (void)directionSetup
{
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    CLLocationCoordinate2D restaurantLocation = CLLocationCoordinate2DMake(self.restaurant.location.latitude, self.restaurant.location.longitude);
    
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:restaurantLocation addressDictionary:nil];
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:[[LocationService sharedInstance].currentLocation coordinate] addressDictionary:nil];
    
    request.source =[[MKMapItem alloc]initWithPlacemark:sourcePlacemark];
    request.destination = [[MKMapItem alloc]initWithPlacemark:destinationPlacemark];
    
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error) {
        
        if (error) {
            
            [self alertWithError:error];
            
        } else {
            
            [self showRoute:response];
            
        }
    }];
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    
    renderer.strokeColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    renderer.lineWidth = 5.0;
    
    return renderer;
}




-(void)showRoute:(MKDirectionsResponse *)response
{
    MKRoute *route = [response.routes firstObject];
    
    [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
    
    self.distanceLabel.text= [NSString stringWithFormat:@"%.02f km",[route distance]/1000];
    
    self.timeLabel.text = [NSString stringWithFormat:@"%@",[self stringFromTimeInterval:[route expectedTravelTime]]];
    
    [self.mapView setVisibleMapRect:[[route polyline]boundingMapRect] animated:YES];

}

#pragma mark - action

- (void) acceptOrder {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] acceptOrderRequest:self.eligibleCourier withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            [self stopTimer];
            [self dismissViewControllerAnimated:YES completion:^{
                
                if ([self.delegate respondsToSelector:@selector(accept:)]) {
                    [self.delegate accept:self.eligibleCourier];
                    
                }
                [[OrderUpdateService sharedInstance] startServiceWithOrder:self.eligibleCourier.order];
                
            }];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
    
}

- (void) refuseOrder {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[CourierServiceManager sharedInstance] refuseOrderRequest:self.eligibleCourier withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            if ([self.delegate respondsToSelector:@selector(refuse:)]) {
                [self.delegate refuse:self.eligibleCourier];
                
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            
            
            [self alertWithError:error];
        }
        
    }];
    
}


#pragma mark - Timer

- (void) startTimer {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1  target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
    
}

- (void) timerTick {
    
    [self updateUI];
}

- (void) stopTimer {
    
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - Util

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

@end
