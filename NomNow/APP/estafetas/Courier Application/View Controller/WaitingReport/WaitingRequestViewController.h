//
//  WaitingRequestViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierBaseViewController.h"
#import "CourierSession.h"
#import "CourierServiceManager.h"
#import "AppDelegate.h"
#import "CourierTabBarController.h"
#import "OrderRequestViewController.h"
#import "LocationService.h"
#import "GoToRestaurantViewController.h"
#import "UITabBarController+HideTabBar.h"

@interface WaitingRequestViewController : CourierBaseViewController <OrderRequestViewControllerDelegate>

@end
