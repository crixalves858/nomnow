//
//  OrderRequestViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "CourierBaseViewController.h"
#import "EligibleCourier.h"
#import "CourierServiceManager.h"
#import "CourierSession.h"
#import "LocationService.h"


@protocol OrderRequestViewControllerDelegate <NSObject>

@required
- (void) accept:(EligibleCourier *)eligible;

@required
- (void) refuse:(EligibleCourier *)eligible;

@required
- (void) notResponse;

@end

@interface OrderRequestViewController : CourierBaseViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) EligibleCourier *eligibleCourier;
@property (nonatomic, strong) id delegate;

@end
