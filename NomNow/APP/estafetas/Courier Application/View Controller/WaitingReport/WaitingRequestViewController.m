//
//  WaitingRequestViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "WaitingRequestViewController.h"

@interface WaitingRequestViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *stateButton;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) Courier *courier;
@property (weak, nonatomic) IBOutlet UILabel *stateStringLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation WaitingRequestViewController

- (void) viewDidLoad {
    
    self.tabBarController.tabBar.hidden = NO;
    [super viewDidLoad];
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"currentLocation" options:NSKeyValueObservingOptionNew context:nil];
    
    self.navigationItem.title = @"Esperar pedidos";
    
    [self updateUI];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    self.courier = [[CourierSession sharedInstance] currentUser].courier;
}

- (void) updateUI {
    
    if(self.stateButton.isOn) {
        
        
        [self.tabBarController setTabBarHidden:YES animated:YES];
        
       
        self.stateStringLabel.text = @"Modo de espera ativo";
        self.spinner.hidden = NO;
        
    } else {
        
        [self.tabBarController setTabBarHidden:NO animated:YES];
           self.stateStringLabel.text = @"Iniciar Modo de espera";
        self.spinner.hidden = YES;
    }
    
}



#pragma mark - action

- (IBAction)stateButtonAction:(id)sender {
    
    if(self.stateButton.isOn) {
        
        [[CourierServiceManager sharedInstance] setCourierState:CourierStateWaiting withBlock:^(BOOL succeeded, NSError *error) {
            
            if(!error) {
                
                [self startService];
                
                [self updateUI];
            } else {
                
                [self alertWithError:error];
                self.stateButton.on = NO;
                
                
            }
            
        }];
        
    } else {
        
        [[CourierServiceManager sharedInstance] setCourierState:CourierStateOff withBlock:^(BOOL succeeded, NSError *error) {
            
            if(!error) {
                
                [self stopService];
                [[LocationService sharedInstance] stopUpdatingLocation];
                
                 [self updateUI];
                
            } else {
                
                [self alertWithError:error];
                self.stateButton.on = YES;
            }
        }];
        
    }
}

#pragma mark - service

- (void) startService {
    
    if(!self.timer)
    {
        [[LocationService sharedInstance] startUpdatingLocation];
        self.timer = [NSTimer scheduledTimerWithTimeInterval: TIMER_STEP target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
    
}

- (void) stopService {
    
    [self.timer invalidate];
    self.timer = nil;
}


- (void) timerTick {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[CourierServiceManager sharedInstance] checkOrdersRequestsWithBlock:^(EligibleCourier *eligible, NSError *error) {
            
            if(!error)
            {
                if(eligible.state == EligibleCourierStateWaitResponse || eligible.state==EligibleCourierStateLastTry)
                {
                    
                    NSDateComponents *dc = [[NSDateComponents alloc] init];
                    [dc setSecond:TIME_TO_RESPONSE];
                    
                    NSDate *hourToResponde = [[NSCalendar currentCalendar] dateByAddingComponents:dc toDate:eligible.lastContact options:0];
                    NSTimeInterval dateTime = [hourToResponde timeIntervalSinceNow];
                    
                    if(dateTime > 0 ) {
                        
                        
                        OrderRequestViewController *orderRequestViewController= [[OrderRequestViewController alloc] initWithNibName:@"OrderRequestViewController" bundle:nil];
                        
                        orderRequestViewController.eligibleCourier = eligible;
                        
                        orderRequestViewController.delegate = self;
                        
                        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:orderRequestViewController];
                        
                        [navigation.navigationBar setTranslucent:NO];
                        
                        [orderRequestViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
                        [self presentViewController:navigation animated:YES completion:nil];
                        
                        [self stopService];
                    }
                    
                }
            } else {
                

            }
            
        }];
        
    });
    
}

#pragma mark - delegate actions

- (void) accept:(EligibleCourier *)eligible {
    
    DeliveryStartViewController *deliveryStartViewController = [[DeliveryStartViewController alloc] initWithNibName:@"DeliveryStartViewController" bundle:nil];
    
    deliveryStartViewController.order = eligible.order;
    
    [self.navigationController setViewControllers:@[deliveryStartViewController] animated:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

- (void) refuse:(EligibleCourier *)eligible {
    
    [self startService];
}

- (void) notResponse {
    
    [self startService];
}

#pragma mark - location


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object  change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"currentLocation"]) {
        
        self.courier.clientData.location = [PFGeoPoint geoPointWithLocation:[[LocationService sharedInstance] currentLocation]];
        [self.courier.clientData saveInBackground];
        
    }
}

- (void) dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    
    [[LocationService sharedInstance] removeObserver:self forKeyPath:@"currentLocation"];
    [super dismissViewControllerAnimated:flag completion:completion];
}



@end
