//
//  LocationService.m
//  estafetas
//
//  Created by Cristiano Alves on 23/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "LocationService.h"


@implementation LocationService 

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}


- (id)init {
    
    self = [super init];
    if(self) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 100; // meters
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }

        self.locationManager.delegate = self;
    }
    return self;
}


- (void)startUpdatingLocation {
    
    NSLog(@"Starting location updates");
    
   [self.locationManager stopUpdatingLocation];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    NSLog(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray*)locations {
    
    
        CLLocation *location = [locations lastObject];
        
        NSLog(@"Latitude %+.6f, Longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
        
        
        self.currentLocation = location;
    
    
}

- (void) stopUpdatingLocation {
    
    NSLog(@"Stop location updates");
    [self.locationManager stopUpdatingLocation];
}

@end
