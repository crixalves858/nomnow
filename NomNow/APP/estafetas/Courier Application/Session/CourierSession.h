//
//  CourierSession.h
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Session.h"

@interface CourierSession : Session

+ (instancetype)sharedInstance;

@end
