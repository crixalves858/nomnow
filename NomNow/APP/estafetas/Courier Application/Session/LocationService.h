//
//  LocationService.h
//  estafetas
//
//  Created by Cristiano Alves on 23/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>

@interface LocationService : NSObject <CLLocationManagerDelegate>

+ (instancetype) sharedInstance;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

- (void)startUpdatingLocation;

- (void)stopUpdatingLocation;

@end
