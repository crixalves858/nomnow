//
//  CourierSession.m
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierSession.h"

@implementation CourierSession

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (User*) currentUser {
    
    return [[Session sharedInstance]currentUser];
    
}

@end
