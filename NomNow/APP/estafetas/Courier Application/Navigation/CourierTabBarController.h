//
//  CourierTabBarController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WaitingRequestViewController.h"
#import "OrdersCourierNavigationController.h"
#import "Order.h"
#import "DeliveryStartViewController.h"
#import <SVProgressHUD.h>
#import "ArriveViewController.h"
#import "MessageCourierService.h"
#import "SendInvoiceViewController.h"
#import "UITabBarController+HideTabBar.h"
#import "ProfileCourierViewController.h"
#import "OrderUpdateService.h"


@interface CourierTabBarController : UITabBarController

- (id) initWithOrder:(Order *) order;

@end
