//
//  CourierTabBarController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierTabBarController.h"

@interface CourierTabBarController ()

@end

@implementation CourierTabBarController

- (id) init{
    
    self = [super init];
    
    WaitingRequestViewController* waitingRequestViewController = [[WaitingRequestViewController alloc] initWithNibName:@"WaitingRequestViewController" bundle:nil];

    UINavigationController *waitNav = [[UINavigationController alloc] initWithRootViewController:waitingRequestViewController];
    
    OrdersCourierNavigationController *ordersNav = [[OrdersCourierNavigationController alloc] init];
    
    ProfileCourierViewController *profileViewCourierViewController = [[ProfileCourierViewController alloc] init];
    
    UINavigationController *profileNav = [[UINavigationController alloc] initWithRootViewController:profileViewCourierViewController];

    
    waitNav.navigationBar.translucent = NO;
    
    self.viewControllers = @[waitNav, ordersNav, profileNav];
    
    UITabBar *tabBar = self.tabBar;
    
    tabBar.translucent = NO;
    
    tabBar.tintColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    
    UITabBarItem *waitItem = tabBar.items[0];
    UITabBarItem *ordersItem = tabBar.items[1];
    UITabBarItem *profileItem = tabBar.items[2];
    
    waitItem.image=[UIImage imageNamed:@"waitModeIcon"];
    ordersItem.image=[UIImage imageNamed:@"ordersIcon"];
    profileItem.image=[UIImage imageNamed:@"profileIcon"];
    
    waitItem.title = @"Modo espera";
    ordersItem.title = @"Entregas";
    profileItem.title = @"Perfil";
    [[MessageCourierService sharedInstance] startService];
    
    return self;
}

- (id) initWithOrder:(Order *) order{
    
    self = [super init];
    
    UIViewController *viewController = nil;
    
    self.tabBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    switch (order.state) {
            
        case OrderStateHaveCourier:
    
            viewController = [[DeliveryStartViewController alloc] init];
            
            ((DeliveryStartViewController*) viewController).order = order;
            
            break;
        case OrderStateGoToRestaurant:
            
            viewController = [[GoToRestaurantViewController alloc] init];
            
            ((GoToRestaurantViewController*) viewController).order = order;
            
            break;
        case OrderStateUpdateWaitingClientAccept:
            
        case OrderStateClientAccept:
            
            viewController = [[GoToRestaurantViewController alloc] init];
            
            ((GoToRestaurantViewController*) viewController).order = order;
            
            break;
            
        case OrderStateUpdateWaitingClientAcceptInRestaurant:
            
            
            viewController = [[WaitClientConfirmOrderChangesViewController alloc] init];
            
            ((WaitClientConfirmOrderChangesViewController*) viewController).order = order;
            break;
            
        case OrderStateClientAcceptInRestaurant:
            
            viewController = [[ViewOrderDetailViewController alloc] init];
            
            ((ViewOrderDetailViewController*) viewController).order = order;
            break;
            
        case OrderStateInRestaurant:
            viewController = [[ConfirmOrderViewController alloc] init];
            
            ((ConfirmOrderViewController*) viewController).order = order;
            break;
            
        
        case OrderStatePay:
            
        case OrderStateGoToClient:
            viewController = [[GoToClientViewController alloc] init];
            
            ((GoToClientViewController*) viewController).order = order;
            break;
        case OrderStateArrived:
            
            viewController = [[ArriveViewController alloc] init];
            ((ArriveViewController*) viewController).order = order;
            
            break;
            
        case OrderStateArrivedWaitClient:
            
            viewController = [[WaitClientArriveViewController alloc] init];
            
            ((WaitClientArriveViewController*) viewController).order = order;
            
            break;
            
            
        default:
            break;
    }

    
    UINavigationController *waitNav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    OrdersCourierNavigationController *ordersNav = [[OrdersCourierNavigationController alloc] init];
    
    ProfileCourierViewController *profileViewCourierViewController = [[ProfileCourierViewController alloc] init];
    
    UINavigationController *profileNav = [[UINavigationController alloc] initWithRootViewController:profileViewCourierViewController];
    
    
    waitNav.navigationBar.translucent = NO;
    
    self.viewControllers = @[waitNav, ordersNav, profileNav];
    
    UITabBar *tabBar = self.tabBar;
    
    tabBar.translucent = NO;
    
    tabBar.tintColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    
    UITabBarItem *waitItem = tabBar.items[0];
    UITabBarItem *ordersItem = tabBar.items[1];
    UITabBarItem *profileItem = tabBar.items[2];
    
    waitItem.image=[UIImage imageNamed:@"waitModeIcon"];
    ordersItem.image=[UIImage imageNamed:@"ordersIcon"];
    profileItem.image=[UIImage imageNamed:@"profileIcon"];
    
    waitItem.title = @"Modo espera";
    ordersItem.title = @"Entregas";
    profileItem.title = @"Perfil";
    [[MessageCourierService sharedInstance] startService];
    [[OrderUpdateService sharedInstance] startServiceWithOrder:order];
    
    return self;
}

- (void) viewDidLoad {
    
    [super viewDidLoad];
   //
    
}

@end
