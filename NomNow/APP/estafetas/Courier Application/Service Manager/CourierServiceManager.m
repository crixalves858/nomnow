//
//  CourierServiceManager.m
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierServiceManager.h"

@implementation CourierServiceManager

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (void) setCourierState:(CourierState)courierState withBlock:(void(^)(BOOL succeeded, NSError *error))block {
    
    User *user = [[CourierSession sharedInstance] currentUser];
    
    [user.courier.privateData setState:courierState];
    
    [user saveInBackgroundWithBlock:block];
    
}

- (void) checkOrdersRequestsWithBlock:(void (^)(EligibleCourier *, NSError *))block {
    
    PFQuery *query = [EligibleCourier query];
    PFQuery *queryStateWait = [EligibleCourier query];
    PFQuery *queryStateLastTry = [EligibleCourier query];
    
    [queryStateWait whereKey:@"courier" equalTo:[[CourierSession sharedInstance]currentUser]];
    [queryStateWait whereKey:@"history" equalTo:@NO];
    [queryStateWait whereKey:@"state" equalTo:[NSNumber numberWithInteger:EligibleCourierStateWaitResponse]];
    
    [queryStateLastTry whereKey:@"courier" equalTo:[[CourierSession sharedInstance]currentUser]];
    [queryStateLastTry whereKey:@"history" equalTo:@NO];
    [queryStateLastTry whereKey:@"state" equalTo:[NSNumber numberWithInteger:EligibleCourierStateLastTry]];
    
    query = [PFQuery orQueryWithSubqueries:@[queryStateLastTry,queryStateWait]];
    
    [query includeKey:@"order"];
    
    [query includeKey:@"order.products"];
    
    [query includeKey:@"order.restaurant"];
    
    [query includeKey:@"order.client"];
    
    [query includeKey:@"order.client.client"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error) {
            EligibleCourier *eligible = nil;
            if (objects.count > 0) {
                eligible = objects[0];
                
                NSMutableArray *array = [[NSMutableArray alloc] init];
                
                for(int i=0;i<eligible.order.products.count; i++) {
                    
                    OrderProduct *product = eligible.order.products[i];
                    
                    [array addObject:product.product];
                }
                
                [PFObject fetchAllInBackground:array block:^(NSArray *objects, NSError *error) {
                    
                    block(eligible, error);
                    
                }];
                
                
            }
            else {
                block(eligible,error);
                
            }
            
        }
    }];
    
}


- (void) whaitActiveOrderWithNumber:(int) number andBlock:(void (^)(NSArray *array, NSError *error))block {
    
    [self myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error) {
            
            if(objects.count>0) {
                
                block(objects, error);
                
            } else {
                
                if(number>0) {
                    [NSThread sleepForTimeInterval:1];
                    [self whaitActiveOrderWithNumber:number-1 andBlock:block];
                    
                }
            }
        } else {
            [NSThread sleepForTimeInterval:1];
            [self whaitActiveOrderWithNumber:number-1 andBlock:block];
            
        }
        
    }];
    
    
}


- (void)acceptOrderRequest:(EligibleCourier *)eligible withBlock:(void (^)(BOOL, NSError *))block {
    
    eligible.state = EligibleCourierStateAccept;
    
    [eligible saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
       
           /* [PFCloud callFunctionInBackground : @"checkOrderState" withParameters : @{@"order":eligible.order.objectId} block:^(id object, NSError *error) {
                
                [self myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
                    if(!error) {
                        
                        if(objects.count>0) {
                            
                            eligible.order = (Order*)objects[0];
                        }
                        
                        
                    }
                    block(succeeded,error);
                }];
                
                
                
            }];*/
            
           [self whaitActiveOrderWithNumber:60 andBlock:^(NSArray *array, NSError *error) {
              
               if(!error) {
                   
                   if(array.count>0) {
                       
                       eligible.order = (Order*)array[0];
                   }
                   
                   
               }
               block(succeeded,error);
               
           }];
            
            
        } else {
            
            block(succeeded, error);
        }
        
    }];
}

- (void) refuseOrderRequest:(EligibleCourier *)eligible withBlock:(void(^)(BOOL succeeded, NSError *error))block {
    
    eligible.state = EligibleCourierStateRefuse;
    
    [eligible saveInBackgroundWithBlock:block];
    
}


- (void) fetchCourierWithBlock:(void (^)(PFObject *, NSError *))block {
    
    User *user = [[CourierSession sharedInstance] currentUser];
    
    PFQuery *query = [User query];
    
    [query includeKey:@"courier"];
    [query includeKey:@"courier.privateData"];
    [query includeKey:@"courier.clientData"];
    
    [query getObjectInBackgroundWithId:user.objectId block:block];
    
}

- (void) updateProducts:(NSMutableArray *)products location:(PFGeoPoint*)location withOrder:(Order *)order block:(void(^)(Order *order, NSError *error))block {
    
    
    order.products = products;
    
    [PFObject saveAllInBackground:order.products block:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [PFCloud callFunctionInBackground : @"updateProducts" withParameters : @{@"order":order.objectId, @"position": location} block:^(id object, NSError *error) {
                
                block(object,error);
                
            }];
            
        } else {
            
            block(nil, error);
            
        }
        
    }];
    
}


- (void) uploadInvoice:(UIImage*)invoice withOrder:(Order*)order block:(void(^)(BOOL succeeded, NSError *error))block {
    
    NSData *imageData = UIImageJPEGRepresentation(invoice, 1.0);
    
    PFFile *imageFile = [PFFile fileWithData:imageData];
    
    order.invoice = imageFile;
    
    [order saveInBackgroundWithBlock:block];
    
}


- (void) arrivedWithOrder:(Order*)order andLocation:(PFGeoPoint *)location block:(void(^)(Order *order, NSError *error))block {
    
    [PFCloud callFunctionInBackground : @"courierArrived" withParameters : @{@"order":order.objectId, @"position":location} block:^(id object, NSError *error) {
        
        block(object, error);
    }];
    
}

- (void) confirmeOrder:(Order *)order PIN:(NSNumber*)pin andLocation:(CLLocation*)location block:(void (^)(Order *order, NSError *))block {
    
    [PFCloud callFunctionInBackground : @"courierArrivedConfirm" withParameters : @{@"order":order.objectId, @"pin":pin, @"position":[PFGeoPoint geoPointWithLocation:location]} block:^(id object, NSError *error) {
        
        block(object, error);
    }];
}

- (void) uploadPhoto:(UIImage*)file withOrder:(Order *)order andLocation:(CLLocation*)location block:(void (^)(Order *order, NSError *error))block {
    
    NSData *imageData = UIImageJPEGRepresentation(file, 1.0);
    
    PFFile *imageFile = [PFFile fileWithData:imageData];
    
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            [PFCloud callFunctionInBackground : @"courierUploadPhoto" withParameters : @{@"order":order.objectId, @"photoFile":imageFile, @"position":[PFGeoPoint geoPointWithLocation:location]} block:^(id object, NSError *error) {
                
                block(object,error);
            }];
        } else {
            block(nil, error);
        }
        
    }];
    
    
}

- (void) clientNotAppearWithOrder:(Order *)order andLocation:(CLLocation*)location block:(void (^)(Order* order, NSError *error))block {
    
    [PFCloud callFunctionInBackground : @"clientNotAppear" withParameters : @{@"order":order.objectId, @"position":[PFGeoPoint geoPointWithLocation:location]} block:^(id object, NSError *error) {
        
        block(object, error);
    }];
    
}


- (void) myActiveOrdersWithBlock:(void(^)(NSArray *objects, NSError *error))block {
    
    
    PFQuery *query = [Order query];
    
    [query includeKey:@"client.client"];
    [query includeKey:@"courier"];
    [query includeKey:@"courier.courier"];
    [query includeKey:@"client.client.courierData"];
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query whereKey:@"state" greaterThanOrEqualTo:[NSNumber numberWithInteger:OrderStateHaveCourier]];
    
    [query whereKey:@"state" lessThan:[NSNumber numberWithInteger:OrderStateOrderDelivery]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        
        if(!error) {
            
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            Order *order = [objects firstObject];
            
            for(int i=0;i<order.products.count; i++) {
                
                OrderProduct *product = order.products[i];
                
                [array addObject:product.product];
            }
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                block(objects, error);
                
            }];
            
            
        } else {
            block(objects, error);
        }
        
    }];
    
}

- (void) getUpdateOrder:(Order*) order withState:(OrderState) orderState block:(void (^)(OrderStateUpdate *orderStateUpdate, NSError *error))block {
    
    
    PFQuery *query = [OrderStateUpdate query];
    
    [query whereKey:@"order" equalTo:order];
    
    [query whereKey:@"state" equalTo:[NSNumber numberWithInteger:orderState]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        OrderStateUpdate *orderStateUpdate = nil;
        if(!error) {
            
            orderStateUpdate = [objects firstObject];
        }
        
        block(orderStateUpdate, error);
    }];
    
    
}


- (void) myOrdersWithBlock:(void(^)(NSArray *objects, NSError *error))block {
    
    
    PFQuery *query = [Order query];
    
    [query includeKey:@"client.client"];
    [query includeKey:@"client.client.courierData"];
    [query includeKey:@"courier"];
    [query includeKey:@"courier.courier"];
    [query includeKey:@"courier.courier.clientData"];
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query includeKey:@"commission"];
    
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error) {
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            Order *order = [objects firstObject];
            
            for(int i=0;i<order.products.count; i++) {
                
                OrderProduct *product = order.products[i];
                
                [array addObject:product.product];
                
            }
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                block(objects, error);
                
            }];
            
        } else {
            block(objects, error);
        }
        
    }];
    
}

- (void) restaurantCloseWithOrder:(Order*)order block:(void(^) (Order *order, NSError *error)) block {
    
    
    order.state = OrderStateCanceleRestaurantClose;
    
    [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        block(order, error);
    }];
}

- (void) fetchIfNeedCourierData:(Order*) order block:(void(^) (Order *order, NSError *error)) block {
    
    [order.client.client.courierData fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
       /* if(!error) {
            
            order.client.client.courierData = (ClientCourierData*)object;
            
            
        }*/
        
        block(order, error);
        
    }];
    
}


@end
