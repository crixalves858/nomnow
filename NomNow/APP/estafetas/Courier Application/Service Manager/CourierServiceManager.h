//
//  CourierServiceManager.h
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ServiceManager.h"
#import "Constant.h"
#import "Order.h"
#import "CourierSession.h"
#import "EligibleCourier.h"
#import "OrderStateUpdate.h"


@interface CourierServiceManager : ServiceManager

+ (instancetype) sharedInstance;

- (void) setCourierState:(CourierState)courierState withBlock:(void(^)(BOOL succeeded, NSError *error))block;

- (void) checkOrdersRequestsWithBlock:(void(^)(EligibleCourier *eligible, NSError *error))block;

- (void) acceptOrderRequest:(EligibleCourier *)eligible withBlock:(void(^)(BOOL succeeded, NSError *error))block;

- (void) refuseOrderRequest:(EligibleCourier *)eligible withBlock:(void(^)(BOOL succeeded, NSError *error))block;

- (void) fetchCourierWithBlock:(void(^)(PFObject *user, NSError *error)) block;

- (void) updateProducts:(NSMutableArray *)products location:(PFGeoPoint*)location withOrder:(Order *)order block:(void(^)(Order *order, NSError *error))block;

- (void) uploadInvoice:(UIImage*)invoice withOrder:(Order*)order block:(void(^)(BOOL succeeded, NSError *error))block;

- (void) arrivedWithOrder:(Order*)order andLocation:(PFGeoPoint *)location block:(void(^)(Order *order, NSError *error))block;

- (void) confirmeOrder:(Order *)order PIN:(NSNumber*)pin andLocation:(CLLocation*)location block:(void (^)(Order *order, NSError *))block;

- (void) uploadPhoto:(UIImage*)file withOrder:(Order *)order andLocation:(CLLocation*)location block:(void (^)(Order *order, NSError *error))block;

- (void) clientNotAppearWithOrder:(Order *)order andLocation:(CLLocation*)location block:(void (^)(Order* order, NSError *error))block;

- (void) myActiveOrdersWithBlock:(void(^)(NSArray *objects, NSError *error))block ;

- (void) getUpdateOrder:(Order*) order withState:(OrderState) orderState block:(void (^)(OrderStateUpdate *orderStateUpdate, NSError *error))block;

- (void) myOrdersWithBlock:(void(^)(NSArray *objects, NSError *error))block ;

- (void) restaurantCloseWithOrder:(Order*)order block:(void(^) (Order *order, NSError *error)) block;

- (void) fetchIfNeedCourierData:(Order*) order block:(void(^) (Order *order, NSError *error)) block;

@end
