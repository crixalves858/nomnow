//
//  MessageCourierService.h
//  estafetas
//
//  Created by Cristiano Alves on 17/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourierServiceManager.h"
#import "Order.h"

@interface MessageCourierService : NSObject

+ (instancetype) sharedInstance;

- (void) startService;

- (void) stopService;

@end
