//
//  MessageCourierService.m
//  estafetas
//
//  Created by Cristiano Alves on 17/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "MessageCourierService.h"

@interface MessageCourierService ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation MessageCourierService

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (void) startService {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: TIMER_STEP target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
}

- (void) stopService {
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void) timerTick {
    
    [self stopService];
    
    
    
    [[CourierServiceManager sharedInstance] checkNotReadMessageWithBlock:^(NSArray *messages, NSError *error) {
        
        if(!error) {
            
            for(int i = 0; i<messages.count; i++) {
                Message *message = messages[i];
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Message%@", message.order.objectId]
                                                                    object:self
                                                                  userInfo:@{@"order":message.order.objectId}];
            }
            [self startService];
        }
    }];
}


@end
