//
//  OrderUpdateService.h
//  Nom Now
//
//  Created by Cristiano Alves on 03/06/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "CourierServiceManager.h"

@interface OrderUpdateService : NSObject

+ (instancetype) sharedInstance;

@property (nonatomic, strong) Order* order;

- (void) startServiceWithOrder:(Order*) order;

- (void) startService;

- (void) stopService;

@end
