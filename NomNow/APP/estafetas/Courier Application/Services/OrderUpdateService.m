//
//  OrderUpdateService.m
//  Nom Now
//
//  Created by Cristiano Alves on 03/06/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderUpdateService.h"

@interface OrderUpdateService ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation OrderUpdateService

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (void) startServiceWithOrder:(Order *)order {
    
    self.order = order;
    [self startService];
}

- (void) startService {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: TIMER_STEP target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
}

- (void) stopService {
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void) timerTick {
    
    [self stopService];
    
    [self.order fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        if(!error) {
            if(self.order.state == OrderStateCanceleByClientWithCourier) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"OrderCancel%@", self.order.objectId]
                                                                    object:self
                                                                  userInfo:nil];
            } else {
                
                [self startService];
            }
        }
    }];
}


@end