//
//  RestaurantTableViewCell.h
//  estafetas
//
//  Created by Cristiano Alves on 13/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FinalStarRatingBar.h>

@interface RestaurantTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *restaurantRatingView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantStateLabel;

@end
