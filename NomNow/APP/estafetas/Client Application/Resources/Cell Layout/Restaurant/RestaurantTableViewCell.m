//
//  RestaurantTableViewCell.m
//  estafetas
//
//  Created by Cristiano Alves on 13/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RestaurantTableViewCell.h"

@interface RestaurantTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *alphaView;


@end

@implementation RestaurantTableViewCell

@synthesize restaurantNameLabel = _restaurantNameLabel;
@synthesize restaurantRatingView = _restaurantRatingView;
@synthesize restaurantImage = _restaurantImage;
@synthesize restaurantStateLabel = _restaurantStateLabel;

- (void)awakeFromNib {
   
    _restaurantRatingView.enabled = NO;
      
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) setUserInteractionEnabled:(BOOL)userInteractionEnabled {
    
    [super setUserInteractionEnabled:userInteractionEnabled];
    
    self.alphaView.hidden = userInteractionEnabled;
    
}
@end
