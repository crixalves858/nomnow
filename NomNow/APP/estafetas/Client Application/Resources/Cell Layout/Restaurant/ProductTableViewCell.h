//
//  ProductTableViewCell.h
//  estafetas
//
//  Created by Cristiano Alves on 13/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;

@end
