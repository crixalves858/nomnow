//
//  OrderTableViewCell.m
//  estafetas
//
//  Created by Cristiano Alves on 14/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderTableViewCell.h"

@implementation OrderTableViewCell

@synthesize restaurantNameLabel = _restaurantNameLabel;
@synthesize orderDateLabel = _orderDateLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
