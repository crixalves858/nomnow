//
//  OrderTableViewCell.h
//  estafetas
//
//  Created by Cristiano Alves on 14/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDateLabel;

@end
