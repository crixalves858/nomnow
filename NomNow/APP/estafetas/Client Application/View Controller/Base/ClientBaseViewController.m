//
//  ClientBaseViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//
#import "LoginNavigationController.h"
#import "Message.h"

#import "ClientBaseViewController.h"
#import "ServiceManager.h"
#import <MPGNotification.h>

@interface ClientBaseViewController ()

@end

@implementation ClientBaseViewController


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:[NSString stringWithFormat:@"Notification"] object:nil];
}
- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (void) notificationAction:(NSNotification *) postNotification {
    
    
    Order *order = (Order*) postNotification.userInfo[@"order"];
    
    Message *message = (Message *) postNotification.userInfo[@"message"];
    
    NSArray *buttonArray;
    UIImage *icon;
    NSString *subtitle;
    NSString *title;
    buttonArray = [NSArray arrayWithObjects:@"Ver",@"Agora não", nil];
    
    if(!message) {
        
        icon = [UIImage imageNamed:@"ordersIcon" ];
        
        subtitle = @"A encomenda foi actualizada";
        
        title = order.restaurant.name;
        
    } else {
        
        icon = [UIImage imageNamed:@"messageIcon" ];
        
        subtitle = @"Nova mensagem";
        
        title = message.order.restaurant.name;
        
        [[ServiceManager sharedInstance] seenMessage:message];
        
    }

    MPGNotification *notification = [[MPGNotification alloc] init];
    
    notification = [MPGNotification notificationWithTitle:title subtitle:subtitle backgroundColor:[UIColor whiteColor] iconImage:icon];
    [notification setButtonConfiguration:buttonArray.count withButtonTitles:buttonArray];
    notification.duration = 5.0;
    notification.swipeToDismissEnabled = NO;

    [notification setButtonHandler:^(MPGNotification *notification, NSInteger buttonIndex) {
        
        if(buttonIndex == notification.firstButton.tag) {
            
            Order *openOrder;
            
            if(!message) {
                
                openOrder = order;
                
            } else {
                
                openOrder = message.order;
            }
            
            UINavigationController *navigationController = self.tabBarController.viewControllers[2];
            
            OrderInProgressViewController *orderInProgressViewController = [[OrderInProgressViewController alloc] initWithNibName:@"OrderInProgressViewController" bundle:nil];
            
            orderInProgressViewController.order = openOrder;
            
            [navigationController popToRootViewControllerAnimated:NO];
            
            [navigationController pushViewController:orderInProgressViewController animated:NO];
            
            if(!order) {
                
                ChatViewController *chat = [ChatViewController messagesViewController];
                
                chat.order = openOrder;
                
                [navigationController pushViewController:chat animated:YES];
                
            }
            
            ClientTabBarController *tabController = (ClientTabBarController*) self.tabBarController;
            
            [tabController changeActiveTabWithIndex:2];

        }
    }];
    
    [notification setTitleColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    [notification setSubtitleColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    

    [notification setAnimationType:MPGNotificationAnimationTypeDrop];
    
    [notification show];
    
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
