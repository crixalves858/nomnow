//
//  ProfileViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 14/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "LoginViewController.h"

@interface ProfileViewController : ClientBaseViewController

@end
