//
//  ProfileViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 14/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loginCheck];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logoutButtonAction:(id)sender {

    [[UpdateService sharedInstance] stopService];
    
    [[ServiceManager sharedInstance] logout];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] init];
    
    [appDelegate changeRootViewController:loginNavigationController animated:YES];
    
    [self.tabBarController dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)loginCheck {
    if(![ClientSession sharedInstance].currentUser) {
        
        LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] initToCheckout];
        
        [loginNavigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        [self presentViewController:loginNavigationController animated:YES completion:nil];
    }
}


@end
