//
//  CheckoutAdressViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 31/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CheckoutAdressViewController.h"

@interface CheckoutAdressViewController ()
@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UITextField *doorNumberField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UITextField *floorField;
@property (weak, nonatomic) IBOutlet UITextView *observationField;


@end

@implementation CheckoutAdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self updateUI];
    [self navigationBarSetupWithoutCanceleAndWithDoneAction:@selector(confirmAddress) andTitle:@"Confirmar encomenda"];
}

- (void) updateUI {
    
    [self.observationField.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.observationField.layer setBorderWidth:1.0];
    
    self.observationField.layer.cornerRadius = 5;
    self.observationField.clipsToBounds = YES;

    
    self.addressField.text = self.orderAddress;
}

- (void) confirmAddress {
    
    CheckoutConfirmViewController *checkoutConfirmViewController = [[CheckoutConfirmViewController alloc] initWithNibName:@"CheckoutConfirmViewController" bundle:nil];
    
    if([self.addressField.text isEqualToString:@""] || [self.codeField.text isEqualToString:@""] || [self.doorNumberField.text isEqualToString:@""] || [self.floorField.text isEqualToString:@""]) {
        
        [self alertWithMessage:@"Preencha todos os campos"];
        return;
    }
    
    checkoutConfirmViewController.orderAddress = self.addressField.text;
    checkoutConfirmViewController.orderLocation = self.orderLocation;
    checkoutConfirmViewController.zipCode = self.codeField.text;
    checkoutConfirmViewController.doorNumber = self.doorNumberField.text;
    checkoutConfirmViewController.floor = self.floorField.text;
    checkoutConfirmViewController.observations = self.observationField.text;
    
    [self.navigationController pushViewController:checkoutConfirmViewController animated:YES];
}

#pragma mark - action

- (IBAction)tapAction:(id)sender {
    
    [self.addressField endEditing:YES];
    [self.doorNumberField endEditing:YES];
    [self.codeField endEditing:YES];
    [self.floorField endEditing:YES];
    [self.observationField endEditing:YES];
}



@end
