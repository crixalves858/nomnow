//
//  CheckoutAddressConfirmViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutAdressViewController.h"
#import "Order.h"
#import "ClientBaseViewController.h"
#import <MapKit/MapKit.h>

@interface CheckoutAddressConfirmMapViewController : ClientBaseViewController <CLLocationManagerDelegate>


@end
