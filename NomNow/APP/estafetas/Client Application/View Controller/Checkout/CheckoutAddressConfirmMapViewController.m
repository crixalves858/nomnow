//
//  CheckoutAddressConfirmViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CheckoutAddressConfirmMapViewController.h"


@interface CheckoutAddressConfirmMapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation CheckoutAddressConfirmMapViewController




- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self navigationBarSetupWithoutCanceleAndWithDoneAction:@selector(confirmCheckout) andTitle:@"Local de entrega"];
    [self locationSetup];
    
    [self mapSetup];
    
}

- (void) confirmCheckout {
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
    
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLocation:location];
    
    Restaurant *restaurant = [[ClientSession sharedInstance] cart].restaurant;
    
    if([geoPoint distanceInKilometersTo:restaurant.location]>(1.0*MAX_DISTANCE_RESTAURANT/1000)) {
        
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"A entrega só pode ser realizada a %d km do restaurante", MAX_DISTANCE_RESTAURANT/1000] maskType:SVProgressHUDMaskTypeBlack];
        
    } else {
        
        
        [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
        
        [ceo reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            
            CheckoutAdressViewController *checkoutAdressViewController = [[CheckoutAdressViewController alloc] initWithNibName:@"CheckoutAdressViewController" bundle:nil];
            PFGeoPoint *geoCode = [PFGeoPoint geoPointWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
            
            [SVProgressHUD dismiss];
            
            if (!error) {
                
                CLPlacemark *place = [placemarks firstObject];
                checkoutAdressViewController.orderAddress = place.name;
            }
            
            checkoutAdressViewController.orderLocation = geoCode;
            [self.navigationController pushViewController:checkoutAdressViewController animated:YES];
            
        }];
    }
}

#pragma mark - Location

- (void) locationSetup {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}



#pragma mark - map

- (void)mapSetup {
    
    Restaurant *restaurant = [[ClientSession sharedInstance] cart].restaurant;
    
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(restaurant.location.latitude, restaurant.location.longitude);
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:location radius:MAX_DISTANCE_RESTAURANT];
    [self.mapView addOverlay:circle];
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 4.1*MAX_DISTANCE_RESTAURANT, 4.1*MAX_DISTANCE_RESTAURANT);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location;
    point.title = restaurant.name;
    point.subtitle = @"Restaurante";
    
    [self.mapView addAnnotation:point];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKCircleRenderer *circleR = [[MKCircleRenderer alloc] initWithCircle:(MKCircle *)overlay];
    circleR.fillColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:0.2];
    circleR.strokeColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    circleR.lineWidth = 1;
    
    return circleR;
}



@end
