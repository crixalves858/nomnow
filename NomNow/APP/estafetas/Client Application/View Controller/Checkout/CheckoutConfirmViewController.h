//
//  CheckoutConfirmViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Product.h"
#import "ClientSession.h"
#import "ClientServiceManager.h"
#import "OrderInProgressViewController.h"
#import "ClientTabBarController.h"
#import "ClientBaseViewController.h"



@interface CheckoutConfirmViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *orderAddress;
@property (nonatomic, strong) PFGeoPoint *orderLocation;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *doorNumber;
@property (nonatomic, strong) NSString *floor;
@property (nonatomic, strong) NSString *observations;

@end
