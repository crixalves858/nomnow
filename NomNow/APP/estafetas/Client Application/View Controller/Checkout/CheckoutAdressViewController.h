//
//  CheckoutAdressViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 31/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "PFGeoPoint.h"
#include "CheckoutConfirmViewController.h"

@interface CheckoutAdressViewController : ClientBaseViewController

@property (nonatomic, strong) NSString *orderAddress;
@property (nonatomic, strong) PFGeoPoint *orderLocation;

@end
