//
//  CheckoutConfirmViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CheckoutConfirmViewController.h"
#import "ProductInCartTableViewCell.h"

@interface CheckoutConfirmViewController ()
@property (weak, nonatomic) IBOutlet UITableView *cartProductsTableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *doorNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *zipCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *waitTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;

@property (nonatomic, strong) NSNumber *priceMin;
@property (nonatomic, strong) NSNumber *priceMax;
@property (nonatomic, strong) NSNumber *timeMin;
@property (nonatomic, strong) NSNumber *timeMax;

@end

@implementation CheckoutConfirmViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadValues];
    [self navigationBarSetupWithoutCanceleAndWithDoneAction:@selector(confirmCheckout) andTitle:@"Confirmar encomenda"];
}


- (void) loadValues {
    
    OrderProduct *orderProduct = [[ClientSession sharedInstance] cart].products[0];
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] calcEstimatedPriceAndWaitTimeWithRestaurant:orderProduct.product.restaurant andClientLocation:self.orderLocation block:^(NSArray *objects, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            int sum = 0;
            
            for( int i = 0; i < [[ClientSession sharedInstance] cart].products.count; i++) {
                
                OrderProduct *orderProductT = [[ClientSession sharedInstance] cart].products[i];
                sum = sum + [orderProductT.quantity intValue];
            }
           
            Restaurant *restaurant = orderProduct.product.restaurant;
            
            double timeInRestaurant = sum * [restaurant.waitTime doubleValue];
            NSNumber *timeMi = objects[2];
            NSNumber *timeMa = objects[3];
            
            self.priceMin = objects[0];
            self.priceMax = objects[1];
            
            self.timeMin = [NSNumber numberWithDouble:[timeMi doubleValue]+timeInRestaurant];
            self.timeMax = [NSNumber numberWithDouble:[timeMa doubleValue]+timeInRestaurant];
            
            [self updateUI];
        } else {
            
            [self alertWithError:error];
        }
    }];
    
}

- (void)updateUI {
    
    [self.cartProductsTableView reloadData];
    
    self.cartProductsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSNumber *total = [[ClientSession sharedInstance]cart].total;
    
    if([self.priceMax doubleValue] != [self.priceMin doubleValue]) {
        
        self.totalLabel.text = [NSString stringWithFormat:@"%.02f - %.02f €", [total doubleValue]+[self.priceMin doubleValue],  [total doubleValue]+[self.priceMax doubleValue]];
        
        self.deliveryPriceLabel.text = [NSString stringWithFormat:@"%.02f - %.02f €", [self.priceMin doubleValue], [self.priceMax doubleValue]];
        
    } else {
        
        self.totalLabel.text = [NSString stringWithFormat:@"%.02f €", [total doubleValue]+[self.priceMin doubleValue]];
        
        self.deliveryPriceLabel.text = [NSString stringWithFormat:@"%.02f €", [self.priceMin doubleValue]];
        
    }
    
    if([self.timeMax doubleValue] != [self.timeMin doubleValue]) {
        
        self.waitTimeLabel.text = [NSString stringWithFormat:@"%.00f - %.00f m", [self.timeMin doubleValue], [self.timeMax doubleValue]];
    } else {
       
        self.waitTimeLabel.text = [NSString stringWithFormat:@"%.00f m", [self.timeMax doubleValue]];
        
    }
    
    self.addressLabel.text = self.orderAddress;
    
    self.zipCodeLabel.text = self.zipCode;
    
    self.doorNumberLabel.text = self.doorNumber;
    
    self.floorLabel.text = self.floor;
    
}

- (void) confirmCheckout {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] checkoutCart:[[ClientSession sharedInstance] cart] withDeliveryAddress:self.orderAddress location:self.orderLocation doorNumber:self.doorNumber zipCode:self.zipCode floor:self.floor andObservations:self.observations block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            UINavigationController *navigationController = self.tabBarController.viewControllers[2];
            
            OrderInProgressViewController *orderInProgressViewController = [[OrderInProgressViewController alloc] initWithNibName:@"OrderInProgressViewController" bundle:nil];
            
            orderInProgressViewController.order = order;
            
            [navigationController popToRootViewControllerAnimated:NO];
            
            [navigationController pushViewController:orderInProgressViewController animated:NO];
            
            ClientTabBarController *tabController = (ClientTabBarController*) self.tabBarController;

            [tabController changeActiveTabWithIndex:2];
            
            [ClientSession sharedInstance].cart = nil;
            
            [self.navigationController popToRootViewControllerAnimated:NO];
            
        } else {
            
            [self alertWithError:error];
        }
        
    } ];
    
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [ClientSession sharedInstance].cart.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    OrderProduct *orderProduct = [[ClientSession sharedInstance] cart].products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",product.price ];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
    
    return cell;

}

@end
