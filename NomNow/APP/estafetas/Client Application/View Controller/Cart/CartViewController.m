//
//  CartViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CartViewController.h"
#import "ProductInCartTableViewCell.h"

@interface CartViewController ()

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (nonatomic) RequestCheckoutState requestCheckoutState;
@end


@implementation CartViewController

- (void) viewDidLoad {

    [super viewDidLoad];
    
    [self initValues];
    
    [self navigationBarSetupWithDoneAction:@selector(checkOutAction) cancelAction:@selector(cancelCartAction) andTitle:@"Carrinho"];
}

- (void) initValues {
    
    self.productsTableView.allowsMultipleSelectionDuringEditing = NO;
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self updateUI];
}

- (void)updateUI {
    
    [self.productsTableView reloadData];
    
    NSNumber *total = [[ClientSession sharedInstance]cart].total;
    
    self.totalLabel.text = [NSString stringWithFormat:@"%.02f €", [total doubleValue]];

    self.productsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    [self checkout];
}

#pragma mark - actionss
- (void) cancelCartAction {
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [self cancelAlert];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
}

- (void) checkOutAction {
    
    if([[ClientSession sharedInstance]cart].products.count == 0) {
        
        [self alertWithMessage:@"O carrinho está vazio"];
        
        return;
    }
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    self.requestCheckoutState = RequestCheckoutStateRequest;
    
    [self checkout];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
    
}

#pragma mark - alert

- (void) cancelAlert {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção"
                                                    message:@"Deseja apagar todo o conteudo do carrinho?"
                                                   delegate:self
                                          cancelButtonTitle:@"Sim"
                                          otherButtonTitles:@"Não",nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Sim"]) {
        
        [ClientSession sharedInstance].cart = nil;
        [self updateUI];
    }
}

#pragma mark - checkout

- (void) checkout {
    
    switch (self.requestCheckoutState) {
        case RequestCheckoutStateNoResquest:
            return;
            break;
        case RequestCheckoutStateRequest:
            
            if ( ![ClientSession sharedInstance].currentUser) {
                
                LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] initToCheckout];
                
                [loginNavigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
                
                [self presentViewController:loginNavigationController animated:YES completion:nil];
                
                self.requestCheckoutState = RequestCheckoutStateNoLogin;
                break;
                
            }
            
            self.requestCheckoutState = RequestCheckoutStateNoLogin;
            
        case RequestCheckoutStateNoLogin:
            
            if ( ![ClientSession sharedInstance].currentUser) {
            
                self.requestCheckoutState = RequestCheckoutStateNoResquest;
            
            } else {
            
                if(![ClientSession sharedInstance].currentUser.client.privateData.card) {
                    
                    
                    CreateCreditCardViewController *createCreditCardViewController = [[CreateCreditCardViewController alloc] init];
                    
                    createCreditCardViewController.delegate = self;
                    
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:createCreditCardViewController];
                    
                    nav.navigationBar.translucent = NO;
                
                    [self presentViewController:nav animated:YES completion:nil];
                    
                    self.requestCheckoutState = RequestCheckoutStateNoCreditCard;

                    
                } else {
                    
                    CheckoutAddressConfirmMapViewController *checkoutAddressConfirmViewController = [[CheckoutAddressConfirmMapViewController alloc] init];
                    
                    [self.navigationController pushViewController:checkoutAddressConfirmViewController animated:YES];
                    
                    self.requestCheckoutState = RequestCheckoutStateNoResquest;
                    
                }
                
                
            }
            break;
            
        case RequestCheckoutStateNoCreditCard:
            
            if(![ClientSession sharedInstance].currentUser.client.privateData.card) {
                
                self.requestCheckoutState = RequestCheckoutStateNoResquest;
            
            } else {
                
                CheckoutAddressConfirmMapViewController *checkoutAddressConfirmViewController = [[CheckoutAddressConfirmMapViewController alloc] init];
                
                [self.navigationController pushViewController:checkoutAddressConfirmViewController animated:YES];
                self.requestCheckoutState = RequestCheckoutStateNoResquest;
            }
    
            break;
            
            
        case RequestCheckoutStateHaveCreditCard:
        {
            CheckoutAddressConfirmMapViewController *checkoutAddressConfirmViewController = [[CheckoutAddressConfirmMapViewController alloc] init];
            
            [self.navigationController pushViewController:checkoutAddressConfirmViewController animated:YES];
            self.requestCheckoutState = RequestCheckoutStateNoResquest;
            
            
            break;

        }
            
    
        default:
            break;
    }
    
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [ClientSession sharedInstance].cart.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    OrderProduct *orderProduct = [[ClientSession sharedInstance] cart].products[indexPath.row];
   
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",product.price ];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
   
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditProductCartViewController *editProductToCartViewController = [[EditProductCartViewController alloc] initWithNibName:@"EditProductCartViewController" bundle:nil];
    
    OrderProduct *orderProduct = [[ClientSession sharedInstance]cart].products[indexPath.row];
    
    editProductToCartViewController.orderProduct= orderProduct;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:editProductToCartViewController];
    
    [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    nav.navigationBar.translucent = NO;
    
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[[ClientSession sharedInstance]cart]removeProductByIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


#pragma mark - delegate

-(void) haveCreditCard:(BOOL)haveCreditCard {
    
    if(haveCreditCard) {
        self.requestCheckoutState = RequestCheckoutStateHaveCreditCard;
    } else {
        
        self.requestCheckoutState = RequestCheckoutStateNoCreditCard;
    }
}

@end
