//
//  CartNavigationController.m
//  estafetas
//
//  Created by Cristiano Alves on 08/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CartNavigationController.h"

@interface CartNavigationController ()

@end

@implementation CartNavigationController

- (id) init{
    
    CartViewController *cartViewController = [[CartViewController alloc] initWithNibName:@"CartViewController" bundle:nil];
    
    self = [super initWithRootViewController:cartViewController];
    
    self.navigationBar.translucent = NO;
    
    return self;
}

@end
