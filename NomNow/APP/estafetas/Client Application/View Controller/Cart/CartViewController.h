//
//  CartViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientSession.h"
#import "EditProductCartViewController.h"
#import "LoginNavigationController.h"
#import "CheckoutAddressConfirmMapViewController.h"
#import "ClientBaseViewController.h"
#import "CreateCreditCardViewController.h"

typedef NS_ENUM(NSInteger, RequestCheckoutState) {
    RequestCheckoutStateNoResquest = 0,
    RequestCheckoutStateRequest = 1,
    RequestCheckoutStateNoLogin = 2,
    RequestCheckoutStateNoCreditCard = 3,
    RequestCheckoutStateHaveCreditCard = 4
};

@interface CartViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource>

@end
