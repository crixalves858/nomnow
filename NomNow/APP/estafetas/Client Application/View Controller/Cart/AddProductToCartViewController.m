//
//  AddProductToCartViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "AddProductToCartViewController.h"

@interface AddProductToCartViewController ()

@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *productQuatityField;
@property (weak, nonatomic) IBOutlet UITextView *productObservationField;

@end

@implementation AddProductToCartViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self updateUI];
    
    [self navigationBarSetupWithAddAction:@selector(addProductAction) andTitle:@"Adicionar Producto"];
    
}

- (void)updateUI {
    
    [self.productObservationField.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.productObservationField.layer setBorderWidth:1.0];
    
    self.productObservationField.layer.cornerRadius = 5;
    self.productObservationField.clipsToBounds = YES;
    
    self.productNameLabel.text = self.product.name;
    self.productPriceLabel.text = [NSString stringWithFormat:@"%@ €", self.product.price];
}

#pragma mark - action

- (void) addProductAction {
    
    NSNumber *quatity = [NSNumber numberWithInteger:[self.productQuatityField.text integerValue]];
    
    NSString *observations = [self.productObservationField text];
    if ([quatity intValue] <= 0) {
        
        [self alertWithMessage:@"A quantidade tem de ser superiror a 0"];
        
    } else {
        
        if(![[ClientSession sharedInstance].cart addProduct:self.product withQuantity:quatity andObservation:observations]) {
            
            [self allertWrongRestaurant];
            
        } else {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (IBAction)tapAction:(id)sender {
    
    [self.productQuatityField endEditing:YES];
    [self.productObservationField endEditing:YES];
    
}

#pragma mark - alert

- (void)allertWrongRestaurant {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                    message:@"Numa encomenda só pode conter produtos de um restaurante. Deseja adicionar este produto e perder os restantes produtos da encomenda?"
                                                   delegate:self
                                          cancelButtonTitle:@"Sim"
                                          otherButtonTitles:@"Não", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Sim"]) {
        
        NSNumber *quatity = [NSNumber numberWithInteger:[self.productQuatityField.text integerValue]];
        
        NSString *observations = [self.productObservationField text];
        
        [ClientSession sharedInstance].cart = [[Cart alloc] init];
        [[ClientSession sharedInstance].cart addProduct:self.product withQuantity:quatity andObservation:observations];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
