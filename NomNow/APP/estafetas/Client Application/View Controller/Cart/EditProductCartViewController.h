//
//  EditProductCartViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderProduct.h"
#import "ClientBaseViewController.h"


@interface EditProductCartViewController : ClientBaseViewController

@property (nonatomic, strong) OrderProduct *orderProduct;

@end
