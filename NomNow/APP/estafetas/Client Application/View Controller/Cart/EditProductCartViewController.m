//
//  EditProductCartViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "EditProductCartViewController.h"

@interface EditProductCartViewController ()

@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *productQuantityField;
@property (weak, nonatomic) IBOutlet UITextView *productObservationField;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@end

@implementation EditProductCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateUI];
    
    [self navigationBarSetupWithEditAction:@selector(saveProductAction) andTitle:@"Editar Produtor"];
}

- (void)updateUI {
    
    [self.productObservationField.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.productObservationField.layer setBorderWidth:1.0];
    
    self.productObservationField.layer.cornerRadius = 5;
    self.productObservationField.clipsToBounds = YES;

    
    self.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",self.orderProduct.product.price];
    
    self.productQuantityField.text = [NSString stringWithFormat:@"%@", self.orderProduct.quantity];
    
    self.productObservationField.text = self.orderProduct.observations;
    
    self.productNameLabel.text = self.orderProduct.product.name;
}

- (void) saveProductAction
{
    
    NSNumber *quatity = [NSNumber numberWithInteger:[self.productQuantityField.text integerValue]];
    
    NSString *observations = [self.productObservationField text];
    
    if ([quatity intValue] <= 0) {
        
        [self alertWithMessage:@"A quantidade tem de ser superiror a 0"];
        
    } else {

        self.orderProduct.quantity = quatity;
        self.orderProduct.observations = observations;
        [self dismissViewControllerAnimated:YES completion:nil];
    
    }
    
}

@end
