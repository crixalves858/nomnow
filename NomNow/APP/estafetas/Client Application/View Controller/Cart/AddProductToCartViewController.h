//
//  AddProductToCartViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "ClientSession.h"
#import "ClientBaseViewController.h"

@interface AddProductToCartViewController : ClientBaseViewController <UIAlertViewDelegate>

@property (nonatomic, strong) Product *product;

@end
