//
//  CartNavigationController.h
//  estafetas
//
//  Created by Cristiano Alves on 08/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartViewController.h"

@interface CartNavigationController : UINavigationController

@end
