//
//  RestauranteViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 20/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RestaurantViewController.h"
#import "ALCustomColoredAccessory.h"
#import <FinalStarRatingBar.h>
#import "ProductTableViewCell.h"

@interface RestaurantViewController ()

@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLable;
@property (strong, nonatomic) IBOutlet UITableView *productsTableView;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UILabel *restaurantStateLabel;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *restaurantRatingView;

@property (nonatomic, strong) NSMutableArray *productByCategory;

@end

@implementation RestaurantViewController

NSMutableIndexSet *expandedSections;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!expandedSections){
        
        expandedSections = [[NSMutableIndexSet alloc] init];
    }

    self.navigationItem.title = self.restaurant.name;
    
    [self updateUI];
    [self loadData];
}

- (void)updateUI {
   
    if(self.restaurant.photo != nil) {
        
        [self.restaurant.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                self.restaurantImage.image = [UIImage imageWithData:data];
            }
        }];
    }
    
    
    self.restaurantRatingView.rating = [self.restaurant.rating integerValue];
    self.restaurantStateLabel.text = @"Aberto";
    self.restaurantNameLable.text = [self.restaurant name];
    self.productByCategory = [[NSMutableArray alloc]init];
    self.restaurantImage.layer.cornerRadius = self.restaurantImage.frame.size.width / 2;
    self.restaurantImage.clipsToBounds = YES;
    self.productsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Table

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.productByCategory count];
   
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    CategoryOfProduct *cat = self.restaurant.categories[section];
    return cat.name;
}*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    

    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return [self.productByCategory[section] count]+1; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
   
    
    if (!indexPath.row) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        }
        
        CategoryOfProduct *cat = self.restaurant.categories[indexPath.section];
        cell.textLabel.text = cat.name;
        
        if ([expandedSections containsIndex:indexPath.section]) {
            
            cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeUp];
        }
        else {
            
            cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeDown];
        }
        
         return cell;
    }
    else {
    
        ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            
            //para carregar a interface
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        
        NSMutableArray *array = self.productByCategory[indexPath.section];
        
        Product *product = array[indexPath.row-1];
        
        cell.productNameLabel.text = product.name;
        cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €", product.price];
        
        return cell;
    }
    
    
   
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if (!indexPath.row)
    {
        // only first row toggles exapand/collapse
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        NSInteger section = indexPath.section;
        BOOL currentlyExpanded = [expandedSections containsIndex:section];
        NSInteger rows;
        
        NSMutableArray *tmpArray = [NSMutableArray array];
        
        if (currentlyExpanded)
        {
            rows = [self tableView:tableView numberOfRowsInSection:section];
            [expandedSections removeIndex:section];
            
        }
        else
        {
            [expandedSections addIndex:section];
            rows = [self tableView:tableView numberOfRowsInSection:section];
        }
        
        for (int i=1; i<rows; i++)
        {
            NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                           inSection:section];
            [tmpArray addObject:tmpIndexPath];
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (currentlyExpanded)
        {
            [tableView deleteRowsAtIndexPaths:tmpArray
                             withRowAnimation:UITableViewRowAnimationTop];
            
            cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeDown];
            
        }
        else
        {
            [tableView insertRowsAtIndexPaths:tmpArray
                             withRowAnimation:UITableViewRowAnimationTop];
            cell.accessoryView =  [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeUp];
            
        }
    }
    else {
       
        NSMutableArray *array = self.productByCategory[indexPath.section];
        
        Product *product = array[indexPath.row-1];
        
        AddProductToCartViewController *addProductToCartViewController = [[AddProductToCartViewController alloc] initWithNibName:@"AddProductToCartViewController" bundle:nil];
        
        addProductToCartViewController.product= product;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:addProductToCartViewController];
        
        [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        nav.navigationBar.translucent = NO;
        
        [self presentViewController:nav animated:YES completion:nil];
        
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


#pragma mark - load data

- (void)loadData {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] getProductsByRestaurant:self.restaurant block:^(NSMutableArray *products, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            self.productByCategory = products;
            [self.productsTableView reloadData];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}



@end
