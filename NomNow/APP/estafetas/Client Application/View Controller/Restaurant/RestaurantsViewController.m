//
//  RestaurantViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 19/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RestaurantsViewController.h"
#import "RestaurantTableViewCell.h"


@interface RestaurantsViewController ()

@property (nonatomic, strong) NSMutableArray *restaurants;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLPlacemark *placeMark;
@property (weak, nonatomic) IBOutlet UITableView *restaurantTable;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarConstraintHeight;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchController;
@property (strong, nonatomic) NSMutableArray *filteredRestaurantsArray;
@end

@implementation RestaurantsViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self locationSetup];
    
    [self initValues];
    
}


- (void) initValues {
    
    UIBarButtonItem *searchButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchInit)];
    
    self.navigationItem.leftBarButtonItem=searchButton;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualizar..."];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.restaurantTable addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    self.navigationItem.title =@"Restaurantes";
    self.restaurantTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    self.searchBarConstraintHeight.constant = 0.0f;
    
    self.searchController.searchResultsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.searchController.searchResultsTableView.rowHeight = 80;
    
}

- (void) searchInit {
    
   [self.searchDisplayController setActive:YES animated:YES];
    self.searchBarConstraintHeight.constant = 44.0f;
    [self.searchController.searchBar becomeFirstResponder];
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    self.searchBarConstraintHeight.constant = 0.0f;
}


-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    
    [self.filteredRestaurantsArray removeAllObjects];
    
    for(int i=0;i <self.restaurants.count; i++) {
        
        Restaurant *restaurant = self.restaurants[i];
        
        if([restaurant.name.lowercaseString containsString:searchText.lowercaseString]) {
            
            [self.filteredRestaurantsArray addObject:restaurant];
        }
        
    }
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark - location

- (void) locationSetup {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
   
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [self.refreshControl beginRefreshing];
    
    [ClientSession sharedInstance].location = newLocation;
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    
    [ceo reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        self.placeMark = [placemarks firstObject];
        
        [self refresh:self.refreshControl];
        
        [self.locationManager stopUpdatingLocation];
        
    }];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status { 
   
    if (status == kCLAuthorizationStatusDenied) {
        NSLog(@"Location services denied");
        [self openChangeLocation];
    }

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [self alertLocationProblem];
    
}

#pragma mark - Alert

- (void)alertLocationProblem {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Estamos com problemas a receber a sua localização. Por favor indique a sua localização"
                                                   delegate:self
                                          cancelButtonTitle:@"Confirmar"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self openChangeLocation];
}

#pragma mark - changeLocation

- (void)openChangeLocation {
    ChangeLocationViewController *changeLocationViewController = [[ChangeLocationViewController alloc] initWithNibName:@"ChangeLocationViewController" bundle:nil];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:changeLocationViewController];
    
    [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    
    nav.navigationBar.translucent = NO;
    changeLocationViewController.delegate = self;
    
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - actions

- (IBAction)locationButtonAction:(id)sender {
    
    UIButton *button = sender;
    
    [button setEnabled:NO];
    
    [self openChangeLocation];
    
    [button setEnabled:YES];
    
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
     if (tableView == self.searchController.searchResultsTableView) {
         
         return [self.filteredRestaurantsArray count];
     }
    
    return [self.restaurants count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    RestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RestaurantTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Restaurant *restaurant;
    
    if (tableView == self.searchController.searchResultsTableView) {
    
        restaurant = self.filteredRestaurantsArray[indexPath.row];
    } else {

        restaurant = self.restaurants[indexPath.row];

    }
    
    cell.restaurantNameLabel.text = restaurant.name;
    cell.restaurantRatingView.rating = [restaurant.rating integerValue];
    
    NSString *stateStr = nil;
    
    switch (restaurant.state ) {
        case NearRestaurantStateAvaliable:
            
            stateStr = @"Aberto";
            cell.userInteractionEnabled = YES;
           
            break;
            
        case NearRestaurantStateClose:
            
            stateStr = @"Fechado";
            cell.userInteractionEnabled = NO;
           
            break;
            
        case NearRestaurantStateWithoutCouriers:
            
            stateStr = @"Sem estafetas";
            cell.userInteractionEnabled = NO;
            break;
            
        default:
            break;
    }
    
    if(restaurant.photo != nil) {
        
        [restaurant.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            
            if(!error) {
                
                cell.restaurantImage.image = [UIImage imageWithData:data];
            }
        }];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.restaurantStateLabel.text = stateStr;
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RestaurantViewController *restaurantViewController = [[RestaurantViewController alloc] initWithNibName:@"RestaurantViewController" bundle:nil];
    
    Restaurant *restaurant;
    
    if (tableView == self.searchController.searchResultsTableView) {
        
        restaurant = self.filteredRestaurantsArray[indexPath.row];
     
    } else {
        
        restaurant = self.restaurants[indexPath.row];
        
    }
    
    restaurantViewController.restaurant = restaurant;

    [self.navigationController pushViewController:restaurantViewController animated:YES];
    
 

}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadData];
}

#pragma mark - load data

- (void)loadDataWithPlaceMark:(CLPlacemark*)placeMark {
    
    self.restaurants = nil;
    [self.restaurantTable reloadData];
    
     [self.refreshControl beginRefreshing];
 
    self.placeMark = placeMark;
    
    [self loadData];

}

- (void)loadData{
    
    [self.locationButton setTitle:[NSString stringWithFormat:@"Perto de: %@",[self.placeMark locality]] forState:UIControlStateNormal];
    
    [[ClientServiceManager sharedInstance] getNearRestaurantsWithLatitude:[[ClientSession sharedInstance] location].coordinate.latitude andLongitude:[[ClientSession sharedInstance] location].coordinate.longitude  block:^(NSArray *restaurants, NSArray *states, NSError *error) {
        
        if(!error) {
            
            
            self.restaurants = [[NSMutableArray alloc] init];
            self.filteredRestaurantsArray = [[NSMutableArray alloc] init];

            for(int i=0;i<restaurants.count;i++) {
                Restaurant *restaurant = restaurants[i];
                NSNumber *state = states[i];
                restaurant.state = [state integerValue];
                [self.restaurants addObject:restaurant];
                [self.filteredRestaurantsArray addObject:restaurant];
            }
                     
            
            
            NSSortDescriptor *sortState = [[NSSortDescriptor alloc] initWithKey:@"state" ascending:YES];
            NSSortDescriptor *sortRating = [[NSSortDescriptor alloc] initWithKey:@"rating" ascending:NO];
            
            [self.restaurants sortUsingDescriptors:[NSArray arrayWithObjects:sortState, sortRating, nil]];
            
            [self.restaurantTable reloadData];
            
            [self.filteredRestaurantsArray sortUsingDescriptors:[NSArray arrayWithObjects:sortState, sortRating, nil]];
            
        } else {
            [self alertWithError:error];
            
        }
        
        [self.refreshControl endRefreshing];
       
    }];
}

@end
