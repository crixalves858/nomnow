//
//  RestauranteViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 20/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import "BaseViewController.h"
#import "ClientServiceManager.h"
#import "AddProductToCartViewController.h"

@interface RestaurantViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) Restaurant *restaurant;


@end
