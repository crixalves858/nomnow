//
//  RestaurantViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 19/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientServiceManager.h"
#import "ChangeLocationViewController.h"
#import "RestaurantViewController.h"
#import "Constant.h"
#import "BaseViewController.h"
#import  <SVProgressHUD.h>

@interface RestaurantsViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIAlertViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@end
