//
//  ChangeLocationViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 20/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ChangeLocationViewController.h"

@interface ChangeLocationViewController ()

@property (nonatomic, strong) NSMutableArray *results;

@property (weak, nonatomic) IBOutlet UITableView *restultsTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchController;



@end

@implementation ChangeLocationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    [self navigationBarSetupWithCanceleButtonAndTitle:@"Escolha de localização"];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.restultsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.searchController.searchResultsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

#pragma mark Content Filtering

-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.results removeAllObjects];
    
    [self loadDataWithInput:searchText];
   
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}






#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    
    CLPlacemark *place = self.results[indexPath.row];
    
    cell.textLabel.text = place.locality;
    cell.detailTextLabel.text = place.name;
 
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CLPlacemark *place = self.results[indexPath.row];

    [ClientSession sharedInstance].location = place.location;
    
    if ([self.delegate respondsToSelector:@selector(loadDataWithPlaceMark:)]) {
        [self.delegate loadDataWithPlaceMark:place];
        NSLog(@"latitude: %f, longitude: %f", place.location.coordinate.latitude, place.location.coordinate.longitude);
    }

    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - load data

- (void)loadDataWithInput:(NSString *)text {
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    
    [ceo geocodeAddressString:text completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if(!error) {
        
            self.results = [NSMutableArray arrayWithArray:placemarks];
            [self.searchController.searchResultsTableView reloadData];
            [self.restultsTableView reloadData];
    
        
        }
        
        
    }];
}




@end
