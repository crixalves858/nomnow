//
//  RestaurantsNavigationController.m
//  estafetas
//
//  Created by Cristiano Alves on 08/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RestaurantsNavigationController.h"

@interface RestaurantsNavigationController ()

@end

@implementation RestaurantsNavigationController

- (id) init{
    
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithNibName:@"RestaurantsViewController" bundle:nil];
    
    self = [super initWithRootViewController:restaurantsViewController];
    
    self.navigationBar.translucent = NO;
    
    return self;
}

@end
