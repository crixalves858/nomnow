//
//  ChangeLocationViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 20/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ClientSession.h"
#import "ClientBaseViewController.h"


@protocol ChangeLocationViewControllerDelegate <NSObject>

@required
- (void) loadDataWithPlaceMark:(CLPlacemark*)placeMark;

@end

@interface ChangeLocationViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (nonatomic,strong) id delegate;

@end
