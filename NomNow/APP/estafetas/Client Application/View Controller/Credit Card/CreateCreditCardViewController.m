//
//  CreateCreditCardViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 28/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CreateCreditCardViewController.h"

@interface CreateCreditCardViewController ()

@property (weak, nonatomic) IBOutlet UITextField *cardName;
@property (weak, nonatomic) IBOutlet UITextField *cardNumber;
@property (weak, nonatomic) IBOutlet UITextField *cardMonth;
@property (weak, nonatomic) IBOutlet UITextField *cardYear;
@property (weak, nonatomic) IBOutlet UITextField *cardCVV;
@property (weak, nonatomic) IBOutlet UITextField *cardCountry;

@end

@implementation CreateCreditCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self navigationBarSetupWithDoneAction:@selector(addCardAction) andTitle:@"Adicionar cartão"];
   
}



- (void) addCardAction {
    
    NSString *name = self.cardName.text;
    NSNumber *number = [NSNumber numberWithInteger:[self.cardNumber.text integerValue]];
    NSNumber *month = [NSNumber numberWithInteger:[self.cardMonth.text integerValue]];
    NSNumber *year = [NSNumber numberWithInteger:[self.cardYear.text integerValue]];
    NSString *cvv = self.cardCVV.text;
    NSString *country = self.cardCountry.text;
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] createCardWithName:name number:number month:month year:year cvv:cvv country:country withBlock:^(Card *card, NSError *error) {
       
        [SVProgressHUD dismiss];
        
        if(!error) {
           
            if ([self.delegate respondsToSelector:@selector(haveCreditCard:)]) {
                [self.delegate haveCreditCard:YES];
               
            }

            [self dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            
            if ([self.delegate respondsToSelector:@selector(haveCreditCard:)]) {
                [self.delegate haveCreditCard:NO];
                
            }
            [self alertWithError:error];
            
        }
    }];
    
    
    
}

- (IBAction)addCardButtonAction:(id)sender {
    
    [self addCardAction];
    
}


@end
