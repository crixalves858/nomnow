//
//  CreateCreditCardViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 28/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "ClientServiceManager.h"
#import "CheckoutAddressConfirmMapViewController.h"


@protocol CreateCreditCardViewControllerDelegate <NSObject>

@required
- (void) haveCreditCard:(BOOL)haveCreditCard;

@end

@interface CreateCreditCardViewController : ClientBaseViewController

@property (nonatomic,strong) id delegate;


@end
