//
//  OrdersViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrderTableViewCell.h"
#import <MPGNotification.h>

@interface OrdersViewController ()

@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;
@property (nonatomic, strong) NSMutableArray *activeOrders;
@property (nonatomic, strong) NSMutableArray *incativeOrders;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation OrdersViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self loginCheck];
    [self initValues];
    
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self loadData];
    

}


- (void) initValues {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Actualizar..."];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.ordersTableView addSubview:self.refreshControl];
    self.ordersTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.navigationItem.title = @"Encomendas";
    [self loadData];
   
}

- (void)loginCheck {
    if(![ClientSession sharedInstance].currentUser) {
        
        LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] initToCheckout];
        
        [loginNavigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        [self presentViewController:loginNavigationController animated:YES completion:nil];
    }
}

- (void)loadData {
    
    [self.refreshControl beginRefreshing];

    [[ClientServiceManager sharedInstance]getMyOrdersWithBlock:^(NSArray *orders, NSError *error) {
        
        [self.refreshControl endRefreshing];
        if (!error) {
            
            self.activeOrders = [[NSMutableArray alloc] init];
            self.incativeOrders = [[NSMutableArray alloc] init];
            
            for(int i = 0; i < orders.count; i++ ) {
                
                Order *order = orders[i];
                
                if(order.state >= OrderStateOrderDelivery) {
                    
                    [self.incativeOrders addObject:order];
                    
                } else {
                    
                    [self.activeOrders addObject:order];
                }
            }
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderDate" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    

            self.activeOrders = [[NSMutableArray alloc] initWithArray:[self.activeOrders sortedArrayUsingDescriptors:sortDescriptors]];
            self.incativeOrders = [[NSMutableArray alloc] initWithArray:[self.incativeOrders sortedArrayUsingDescriptors:sortDescriptors]];
            
            [self.ordersTableView reloadData];
            
            
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}



#pragma mark - Table

- (NSArray*) sectionsName {
    if(self.activeOrders.count>0) {
        
        return @[@"Encomendas activas", @"Encomendas finalizadas"];
    } else {
        
        return @[@"Encomendas finalizadas", @"Encomendas activas"];
    }
}

- (NSArray*) sections {
    
    if(!self.activeOrders || !self.self.incativeOrders) {
        
        return @[];
    }
    
    if(self.incativeOrders.count == 0 && self.activeOrders.count == 0) {
        return @[];
    }
    
    if(self.activeOrders.count>0 && self.incativeOrders.count>0) {
        
        return @[self.activeOrders, self.incativeOrders];
    }
    
    if(self.activeOrders.count>0) {
        
        return @[self.activeOrders];
    }
    
    return @[self.incativeOrders];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return self.sectionsName[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSMutableArray *array = self.sections[section];
    
    return array.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSMutableArray *array = self.sections[indexPath.section];
    
    Order *order = array[indexPath.row];
    
    cell.restaurantNameLabel.text = order.restaurant.name;
    cell.orderDateLabel.text = [self stringFromDate:order.orderDate];
    
    return cell;
    
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadData];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderInProgressViewController *orderInProgressViewController = [[OrderInProgressViewController alloc] initWithNibName:@"OrderInProgressViewController" bundle:nil];
    
    NSMutableArray *array = self.sections[indexPath.section];
    
    Order *order = array[indexPath.row];
    
    orderInProgressViewController.order = order;
    
    [self.navigationController pushViewController:orderInProgressViewController animated:YES];
    
}

- (NSString *)stringFromDate:(NSDate*)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm - dd/MM/yyy"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
    
}



@end
