//
//  RatingInClientViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 09/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import <FinalStarRatingBar.h>
#import "Order.h"
#import "ClientServiceManager.h"

@interface RatingInClientViewController : ClientBaseViewController

@property (nonatomic, strong) Order *order;

@end
