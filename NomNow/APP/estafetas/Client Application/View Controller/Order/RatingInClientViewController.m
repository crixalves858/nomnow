//
//  RatingInClientViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 09/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RatingInClientViewController.h"

@interface RatingInClientViewController ()
@property (weak, nonatomic) IBOutlet UILabel *courierNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courierRatingLabel;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *ratingView;
@property (weak, nonatomic) IBOutlet UIImageView *courierImageView;

@end

@implementation RatingInClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initValues];
    [self updateUI];
    
}

- (void) updateUI {
    
    self.courierNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.order.courier.courier.firstName, self.order.courier.courier.lastName];
    
    [self.order.courier.courier.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if(!error) {
            
            self.courierImageView.image = [UIImage imageWithData:data];
        }
    }];
    
}

- (void) initValues {
    
    [[ClientServiceManager sharedInstance] getRatingWithReviewer:self.order.client rated:self.order.courier andOrder:(Order*)self.order block:^(Rating *rating, NSError *error) {
        
        if(!error) {
            
            self.ratingView.rating = [rating.value integerValue];
        }
    }];
    
    [self.ratingView setRatingChangedBlock:^(NSUInteger rating) {
   
        [[ClientServiceManager sharedInstance] setRatingWithReviewer:self.order.client rated:self.order.courier order:self.order andRating:rating block:^(Rating *rating, NSError *error) {
            
            if(error) {
                
                NSLog(@"erro: %@", error.description);
            }
        }];
    }];
}


@end
