//
//  OrderProductDetailViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 12/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "Order.h"

@interface OrderProductDetailViewController : ClientBaseViewController

@property (nonatomic, strong) Order *order;

@end
