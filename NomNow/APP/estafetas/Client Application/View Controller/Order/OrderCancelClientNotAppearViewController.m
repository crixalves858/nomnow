//
//  OrderCancelClientNotAppearViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 09/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderCancelClientNotAppearViewController.h"

@interface OrderCancelClientNotAppearViewController ()
@property (weak, nonatomic) IBOutlet UITextView *justificationField;

@end

@implementation OrderCancelClientNotAppearViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self navigationBarSetupWithDoneAction:@selector(sendJustification) cancelAction:@selector(cancelJustification) andTitle:@"Falha na entrega"];

    [self.justificationField.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.justificationField.layer setBorderWidth:1.0];
    
    self.justificationField.layer.cornerRadius = 5;
    self.justificationField.clipsToBounds = YES;
    
}


- (void) sendJustification {
    
    [[ClientServiceManager sharedInstance] clientNotAppearToRecebeOrder:self.order response:self.justificationField.text block:^(Order *order, NSError *error) {
       
        if(!error) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }];
    
}

- (void) cancelJustification {
    
    self.order.state = OrderStateDeliveryFailClient;
    
    [self.order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
