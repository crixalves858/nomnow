//
//  OrdersClientNavigationController.m
//  estafetas
//
//  Created by Cristiano Alves on 08/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrdersClientNavigationController.h"

@interface OrdersClientNavigationController ()

@end

@implementation OrdersClientNavigationController

- (id) init{
    
    OrdersViewController *ordersViewController = [[OrdersViewController alloc] initWithNibName:@"OrdersViewController" bundle:nil];
    
    self = [super initWithRootViewController:ordersViewController];
    
    self.navigationBar.translucent = NO;
    
    [self.navigationBar setBarTintColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    
    [self.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationBar.tintColor = [UIColor whiteColor];
    return self;
}

@end
