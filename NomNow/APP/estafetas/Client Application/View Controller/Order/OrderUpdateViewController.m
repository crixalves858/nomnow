//
//  OrderUpdateViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderUpdateViewController.h"
#import "ProductInCartTableViewCell.h"

@interface OrderUpdateViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation OrderUpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self navigationBarSetup];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)navigationBarSetup {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(refuseAction)];
    UIBarButtonItem *addProductTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(acceptAction)];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=addProductTabButton;
    self.navigationItem.title=@"Encomenda alterada";
    
}

- (void) acceptAction {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] updateOrder:self.order state:self.acceptState andLocation:nil withBlock:^(BOOL sucess, NSError * error) {
       
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

- (void) refuseAction {

    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] updateOrder:self.order state:self.acceptState andLocation:nil withBlock:^(BOOL sucess, NSError * error) {
       
        [SVProgressHUD dismiss];
        if(!error) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            [self alertWithError:error];
        }
    }];
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.order.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"RestaurantTableViewCell";
    
    ProductInCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        //para carregar a interface
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductInCartTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.productNameLabel.text = product.name;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@ €",orderProduct.realPrice ];
    cell.productQuantityLabel.text = [NSString stringWithFormat: @"%@",orderProduct.quantity];
    
    
    return cell;
    
}
@end
