//
//  OrdersViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "ClientSession.h"
#import "ClientServiceManager.h"
#import "BaseViewController.h"
#import "OrderInProgressViewController.h"
#import "LoginNavigationController.h"

@interface OrdersViewController : ClientBaseViewController <UITableViewDelegate, UITableViewDataSource>

@end
