//
//  OrderUpdateViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 01/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "Order.h"
#import "OrderProduct.h"
#import "ClientServiceManager.h"

@interface OrderUpdateViewController : ClientBaseViewController

@property (nonatomic, strong) Order *order;
@property (nonatomic) OrderState acceptState;

@end
