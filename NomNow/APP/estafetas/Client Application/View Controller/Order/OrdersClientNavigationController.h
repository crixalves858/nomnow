//
//  OrdersClientNavigationController.h
//  estafetas
//
//  Created by Cristiano Alves on 08/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdersViewController.h"

@interface OrdersClientNavigationController : UINavigationController

@end
