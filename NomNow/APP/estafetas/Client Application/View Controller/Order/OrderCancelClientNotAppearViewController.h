//
//  OrderCancelClientNotAppearViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 09/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientBaseViewController.h"
#import "ClientServiceManager.h"
#import "Order.h"

@interface OrderCancelClientNotAppearViewController : ClientBaseViewController

@property (nonatomic, strong) Order *order;

@end
