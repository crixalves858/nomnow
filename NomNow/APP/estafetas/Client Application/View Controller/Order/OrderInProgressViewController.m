//
//  OrderInProgressViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderInProgressViewController.h"
#import <MPGNotification.h>

@interface OrderInProgressViewController ()

@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *courierImageView;
@property (weak, nonatomic) IBOutlet UILabel *courierName;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *courierClassification;
@property (weak, nonatomic) IBOutlet UILabel *hourDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryImageCourierImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeToRececeiveLabel;
@property (weak, nonatomic) IBOutlet UIView *noCourrierView;
@property (weak, nonatomic) IBOutlet UIView *deliveryImageCourierView;
@property (strong, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *courierRatingView;
@property (weak, nonatomic) IBOutlet FinalStarRatingBar *ratingRestaurantView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UIView *activeOrderDataView;
@property (weak, nonatomic) IBOutlet UIView *courierView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *courierViewHeightContraint;
@property (weak, nonatomic) IBOutlet UIButton *requestInvoiceButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageState;
@property (weak, nonatomic) IBOutlet UIView *mainView;


@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) double total;
@property (strong, nonatomic) NSDate *hourOfDelivery;
@property (nonatomic, strong) NSDate *hourFinishWaitTime;

@property (nonatomic, strong) UIBarButtonItem *messageButton;
@property (nonatomic, strong) UIBarButtonItem *helpButton;

@end

@implementation OrderInProgressViewController

@synthesize order = _order;

- (NSArray*) orderStateText {
    
    return @[@"À espera de estafeta",
             @"Encontramos um estafeta para si",
             @"O estafeta actualizou a encomenda. Aceite ou rejeite a alteração",
             @"A encomenda está confirmada",
             @"O estafeta está a dirigir-se para o restaurante.",
             @"O estafeta chegou ao restaurante",
             @"O estafeta alterou a encomenda. Ele encontra-se no restaurante à espera que aceite",
             @"A encomenda está confirmada, e a ser finalizada no restaurante.",
             @"A encomenda está confirmada, e a ser finalizada no restaurante.",
             @"O estafeta está a dirigir-se para o local da entrega.",
             @"O cliente chegou ao local da entrega, dirija-se até ao local.",
             @"O estafeta não o esta a conseguir encontrar, verifique a fotografia que o estafeta enviou e dirija-se ao local.",
             @"A encomenda foi entregue",
             @"A entrega falhou",
             @"A falha na entrega encontra-se a ser analisada",
             @"A entrega falhou por falha sua. O custo da encomenda será cubrado",
             @"A entrega falhou por falha do estafeta, não lhe será cubrado qualquer valor",
             @"Não encontramos estafetas para o seu pedido",
             @"Rejeitou a alteração, a encomenda foi cancelada",
             @"À espera de estafeta",
             @"Encomenda cancelada",
             @"O restaurante encontra-se fechado. A encomenda foi cancelada.",
             @"A encomenda foi cancelada por si"
             ];
    
}

- (void) viewDidLoad {
    
    self.helpButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"helpIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(helpAction)];

    self.messageButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"messageIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(openMessages)];
    
    [self.navigationItem setRightBarButtonItems:@[ self.messageButton, self.helpButton] animated:YES];
    
    self.navigationItem.title = self.order.restaurant.name;
    
    self.deliveryImageCourierView.layer.masksToBounds = NO;
    self.deliveryImageCourierView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.deliveryImageCourierView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    self.deliveryImageCourierView.layer.shadowRadius = 1.0f;
    self.deliveryImageCourierView.layer.shadowOpacity = 0.5f;
    self.deliveryImageCourierView.layer.cornerRadius = 10.0f;
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateUI];
    [self hourDeliveryUpdate];
    [self updateHourToReceive];
}

- (id) initWithOrderId:(NSString*) orderId {
    
    self = [super initWithNibName:@"OrderInProgressViewController" bundle:nil];
    
    self.order = [Order objectWithoutDataWithObjectId:orderId];
    
    [self loadOrder];
    
    return self;
}

- (void) setOrder:(Order *)order {
    
    _order = order;
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadOrder) name:[NSString stringWithFormat:@"FindCourier%@", order.objectId] object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadOrder) name:[NSString stringWithFormat:@"Order%@", order.objectId] object:nil];
}



- (UIImage*) imageWithOrderState:(OrderState) orderState{
    
    switch (orderState) {
        case OrderStateUpdateWaitingClientAccept:
        case OrderStateClientAccept:
        case OrderStateInit:
        case OrderStateHaveCourier:
            return [UIImage imageNamed:@"haveCourierIcon"];
            break;
        case OrderStateGoToClient:
        case OrderStateGoToRestaurant:
            return [UIImage imageNamed:@"goToRestaurantIcon"];
            break;
        case OrderStateUpdateWaitingClientAcceptInRestaurant:
        case OrderStateClientAcceptInRestaurant:
        case OrderStatePay:
        case OrderStateInRestaurant:
            return [UIImage imageNamed:@"inRestaurantIcon"];
            break;
        case OrderStateArrivedWaitClient:
        case OrderStateArrived:
            return [UIImage imageNamed:@"arriveIcon"];
            break;
        case OrderStateOrderDelivery:
            return [UIImage imageNamed:@"deliveryIcon"];
            break;
            
        case OrderStateDeliveryFail:
        case OrderStateDeliveryFailAnalize:
        case OrderStateCanceleRestaurantClose:
        case OrderStateCanceleByClientWithCourier:
        case OrderStateDeliveryFailClient:
        case OrderStateFailNotHaveCourier:
        case OrderStateDeliveryFailCourier:
        case OrderStateFailClientNotAccept:
        case OrderStateCanceleByClient:
            return [UIImage imageNamed:@"deliveryFailIcon"];
            break;
            
        default:
            return nil;//self.imageState.image;
            break;
    }
    
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - calcs

- (void) hourDeliveryUpdate {
    
    double courierVelocityKMH = [self.order.courier.courier.velocity doubleValue];
    double resturantTime;
    
    if(!self.order.timeWaitRestaurant || [self.order.timeWaitRestaurant doubleValue] == 0.0) {
        
        resturantTime = [self.order.restaurant.waitTime doubleValue] * self.order.products.count;
        
    } else {
        
        resturantTime = [self.order.timeWaitRestaurant doubleValue];
        
    }
    
    double distance1 = [self.order.distanceCourierRestaurant doubleValue];
    double distance2 = [self.order.distanceRestaurantClient doubleValue];
    
    if(self.order.state < OrderStateInRestaurant) {
        
        [[ClientServiceManager sharedInstance] getUpdateOrder:self.order withState:OrderStateGoToRestaurant block:^(OrderStateUpdate *orderStateUpdate, NSError *error) {
            
            NSDate *start;
            if(!orderStateUpdate) {
                
                start = [NSDate date];
                
            } else {
                
                start = orderStateUpdate.date;
                
            }
            
            NSDate *dateNow = [NSDate date];
            
            NSTimeInterval secondsBetween = [dateNow timeIntervalSinceDate:start];
            
            if(secondsBetween < 0) {
                secondsBetween = 0;
            }
            
            double time1s = (distance1/1000) / courierVelocityKMH;
            
            time1s = time1s * 60 * 60;
            
            time1s = time1s - secondsBetween;
            
            if(time1s<0) {
                time1s = 0;
            }
            
            double time2s = (distance2/1000) / courierVelocityKMH;
            
            time2s = time2s * 60 * 60;
            
            double timeWait;
            
            if(!self.order.timeWaitRestaurant && self.order.timeWaitRestaurant != 0) {
            
                timeWait = [self maxBetween:resturantTime and:time1s] + time2s;
                
            } else {
                
                timeWait = resturantTime*60 + time1s + time2s;
            }
            
            
            self.hourOfDelivery = [dateNow dateByAddingTimeInterval:timeWait];
            
            [self updateUI];
            
        }];
        
    } else if (self.order.state < OrderStateGoToClient) {
        
        [[ClientServiceManager sharedInstance] getUpdateOrder:self.order withState:OrderStateInRestaurant block:^(OrderStateUpdate *orderStateUpdate, NSError *error) {
            
            NSDate *start;
            if(!orderStateUpdate) {
                
                start = [NSDate date];
                
            } else {
                
                start = orderStateUpdate.date;
                
            }
            
            NSDate *dateNow = [NSDate date];
            
            NSTimeInterval secondsBetween = [dateNow timeIntervalSinceDate:start];
            
            if(secondsBetween < 0) {
                secondsBetween = 0;
            }
            
            double time2s = (distance2/1000) / courierVelocityKMH;
            
            time2s = time2s * 60 * 60;
            
            time2s = time2s - secondsBetween;
            
            if(time2s < 0) {
                time2s = 0;
            }
            
            double timeWait = resturantTime *60 + time2s;
            
            self.hourOfDelivery = [dateNow dateByAddingTimeInterval:timeWait];
            
            [self updateUI];
            
        }];
        
    } else {
        
        [[ClientServiceManager sharedInstance] getUpdateOrder:self.order withState:OrderStateGoToClient block:^(OrderStateUpdate *orderStateUpdate, NSError *error) {
            
            NSDate *start;
            if(!error || !orderStateUpdate) {
                
                start = [NSDate date];
                
            } else {
                
                start = orderStateUpdate.date;
                
            }
            
            NSDate *dateNow = [NSDate date];
            
            NSTimeInterval secondsBetween = [dateNow timeIntervalSinceDate:start];
            
            if(secondsBetween < 0) {
                secondsBetween = 0;
            }
            
            double time2s = (distance1/1000) / courierVelocityKMH;
            
            time2s = time2s * 60 * 60;
            
            double timeWait = time2s - secondsBetween;
            
            if(timeWait < 0) {
                
                timeWait = 0;
            }
            
            self.hourOfDelivery = [dateNow dateByAddingTimeInterval:timeWait];
            
            [self updateUI];
            
            
        }];
    }
}

- (void) calcTotal {
    
    double price = 0;
    for(int i = 0 ; i < self.order.products.count; i++) {
        
        OrderProduct *orderProduct = self.order.products[i];
        price += [orderProduct.quantity doubleValue] * [orderProduct.realPrice doubleValue];
    }
    
    if(self.order.commission) {
        
        price += [self.order.commission.value doubleValue];
    }
    
    self.total = price;
}

- (void) loadOrder {
    
    [SVProgressHUD showWithStatus:@"Actualizar" maskType:SVProgressHUDMaskTypeBlack];
    [[ClientServiceManager sharedInstance] getOrder:self.order block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        if(!error) {
            
            self.order = order;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateUI];
                [self hourDeliveryUpdate];
                [self updateHourToReceive];
                
            });
            
        } else {
            
            [self alertWithError:error];
            
        }
    }];
}

- (void) updateHourToReceive {
    
    if(self.order.state == OrderStateArrivedWaitClient) {
        
        [[ClientServiceManager sharedInstance] getUpdateOrder:self.order withState:OrderStateArrivedWaitClient block:^(OrderStateUpdate *orderStateUpdate, NSError *error) {
            
            if(!error) {
                
                NSDateComponents *dc = [[NSDateComponents alloc] init];
                [dc setSecond:TIME_WAIT_CLIENT];
                
                self.hourFinishWaitTime = [[NSCalendar currentCalendar] dateByAddingComponents:dc toDate:orderStateUpdate.date options:0];
                
                [self startTimer];
                
                [self updateDeliveryTime];
                
            }
        }];
    }
    
}


#pragma mark - updateUI


- (void)courierUpdateUI {
    
    
    
    if(self.order.state == OrderStateWatingCourier || self.order.state == OrderStateFailNotHaveCourier ) {
        
        self.courierClassification.hidden = YES;
        self.courierName.hidden = YES;
        self.courierClassification.hidden = YES;
       
        
    } else {
       
        self.courierClassification.hidden = NO;
        self.courierName.hidden = NO;
        self.courierClassification.hidden = NO;
        
        self.courierName.text = [NSString stringWithFormat:@"%@ %@",self.order.courier.courier.firstName, self.order.courier.courier.lastName];
        
        if(self.order.courier.courier.photo != nil) {
            
            [self.order.courier.courier.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.courierImageView.image = [UIImage imageWithData:data];
                } else {
                    
                    [self alertWithError:error];
                }
            }];
        }
        
       
        
        self.courierClassification.rating = [self.order.courier.courier.classification integerValue];
        self.courierClassification.enabled = NO;
    }
    
}

- (void) hourUpdateUI {
    
    if(self.order.state == OrderStateWatingCourier || self.order.state >= OrderStateArrived) {
        
        self.hourDeliveryLabel.hidden = YES;
        
    } else {
        
        self.hourDeliveryLabel.hidden = NO;
        
        self.hourDeliveryLabel.text = [self stringFromDate:self.hourOfDelivery];
        
        NSLog(@"Estado: %ld Hora actual: %@: Hora de entrega: %@", self.order.state, [self stringFromDate:[NSDate date]], [self stringFromDate:self.hourOfDelivery] );
        
    }
    
}

- (void) receiveImageUpdateUI {
    
    if(self.order.state == OrderStateArrivedWaitClient) {
        
        if(self.order.courier != nil) {
            
            [self.order.courierPhoto getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.deliveryImageCourierImageView.image = [UIImage imageWithData:data];
                    self.deliveryImageCourierView.hidden=NO;
                } else {
                    
                    [self alertWithError:error];
                }
            }];
        }
    } else {
        self.deliveryImageCourierView.hidden=YES;
    }
    
}

- (void) updateUI {
    
    switch (self.order.state) {
        case OrderStateInit:
            
            self.noCourrierView.hidden=NO;
            self.messageButton.enabled = NO;
            self.mainView.hidden = YES;
            self.courierView.hidden = YES;
            break;
        
        case OrderStateWatingCourier:
            
            self.noCourrierView.hidden=NO;
            self.messageButton.enabled = NO;
            self.mainView.hidden = YES;
            self.courierView.hidden = YES;

            break;
            
        case OrderStateFailNotHaveCourier:
            
            self.courierViewHeightContraint.constant = 0;
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = YES;
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = YES;

            break;
        
        case OrderStateOrderDelivery:
            
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = NO;
            self.requestInvoiceButton.hidden = NO;
            [self ratingsLoadData];
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;

            break;
            
        case OrderStateDeliveryFail:
            
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = NO;
            [self ratingsLoadData];
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            
            break;
        case OrderStateDeliveryFailAnalize:
            
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = NO;
            [self ratingsLoadData];
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
            
        case OrderStateDeliveryFailClient:
          
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = NO;
            [self ratingsLoadData];
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
            
        case OrderStateDeliveryFailCourier:
         
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = NO;
            [self ratingsLoadData];
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
        
        case OrderStateCanceleByClient:
           
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = YES;
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
            
        case OrderStateCanceleRestaurantClose:
            
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = YES;
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
            
        case OrderStateCanceleByClientWithCourier:
            
            self.activeOrderDataView.hidden = YES;
            self.ratingView.hidden = YES;
            self.messageButton.enabled = NO;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
            break;
            
        default:
            self.noCourrierView.hidden=YES;
            self.activeOrderDataView.hidden = NO;
            self.ratingView.hidden = YES;
            self.messageButton.enabled = YES;
            self.mainView.hidden = NO;
            self.courierView.hidden = NO;
           
            break;
    }
     self.imageState.image = [self imageWithOrderState:self.order.state];
    [self courierUpdateUI];
    
    [self hourUpdateUI];
    [self receiveImageUpdateUI];
    self.stateLabel.text = [self orderStateText][self.order.state];
    
    self.restaurantNameLabel.text = self.order.restaurant.name;
    
    self.pinLabel.text = [NSString stringWithFormat:@"%ld", (long)self.order.clientData.validationPIN];
    
    [self calcTotal];
    
    self.totalLabel.text = [NSString stringWithFormat:@"%.02f €", self.total];
    
    if(self.order.state == OrderStateUpdateWaitingClientAccept || self.order.state == OrderStateUpdateWaitingClientAcceptInRestaurant) {
        
        OrderUpdateViewController *orderUpdateViewController = [[OrderUpdateViewController alloc] initWithNibName:@"OrderUpdateViewController" bundle:nil];
        
        orderUpdateViewController.order= self.order;
        
        orderUpdateViewController.acceptState = self.order.state + 1;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:orderUpdateViewController];
        
        [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        nav.navigationBar.translucent = NO;
        
        [self presentViewController:nav animated:YES completion:nil];
        
    }
    
    if(self.order.state == OrderStateDeliveryFail) {
        
        OrderCancelClientNotAppearViewController *orderCancelClientNotAppearViewController = [[OrderCancelClientNotAppearViewController alloc] initWithNibName:@"OrderCancelClientNotAppearViewController" bundle:nil];
        
        orderCancelClientNotAppearViewController.order = self.order;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:orderCancelClientNotAppearViewController];
        
        [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        nav.navigationBar.translucent = NO;
        
        [self presentViewController:nav animated:YES completion:nil];
        
    }
}


- (void) ratingsLoadData {
    
    [self ratingCourierLoadData];
    [self ratingRestaurantLoadData];
}

- (void) ratingCourierLoadData{
    
    [[ClientServiceManager sharedInstance] getRatingWithReviewer:self.order.client rated:self.order.courier andOrder:(Order*)self.order block:^(Rating *rating, NSError *error) {
        
        if(!error) {
            
            self.courierRatingView.rating = [rating.value integerValue];
        }
    }];
    
    
    [self.courierRatingView setRatingChangedBlock:^(NSUInteger rating) {
        
        [[ClientServiceManager sharedInstance] setRatingWithReviewer:self.order.client rated:self.order.courier order:self.order andRating:rating block:^(Rating *rating, NSError *error) {
            
            if(error) {
                
                NSLog(@"erro: %@", error.description);
            }
        }];
    }];

}


- (void) ratingRestaurantLoadData{
    
    [[ClientServiceManager sharedInstance] getRatingWithReviewer:self.order.client restaurant:self.order.restaurant andOrder:(Order*)self.order block:^(RatingRestaurant *rating, NSError *error) {
        
        if(!error) {
            
            self.ratingRestaurantView.rating = [rating.value integerValue];
        }
    }];
    
    
    [self.ratingRestaurantView setRatingChangedBlock:^(NSUInteger rating) {
        
        [[ClientServiceManager sharedInstance] setRatingWithReviewer:self.order.client restaurant:self.order.restaurant order:self.order andRating:rating block:^(RatingRestaurant *rating, NSError *error) {
            
            if(error) {
                
                NSLog(@"erro: %@", error.description);
            }
        }];
    }];
    
}




#pragma mark - actions


- (void) openMessages {
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    
    chat.order = self.order;
       
    [self.navigationController pushViewController:chat animated:YES];
}


- (void) helpAction {
    
    UIActionSheet *actionSheet = nil;
    
    NSString *mostTime = nil;
    
    NSString *cancel = nil;
    
    if(self.order.state>=OrderStateHaveCourier && self.order.state<OrderStateArrived) {
        
        mostTime = @"O estafeta está a demorar demasiado tempo";
        
    }
    
    if(self.order.state<OrderStateOrderDelivery || self.order.state==OrderStateInit) {
        
        cancel = @"Já não pertendo receber a encomenda";
    }
    
    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Qual é o problema?"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:@"Fechar"
                                                    otherButtonTitles:@"Outro problema",cancel, mostTime, nil];
    
   
    [actionSheet showInView:self.view];
}

- (void)alertCourierNotAppear {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] createOrderProduct:self.order withDescription:@"O estafeta não está aparecer" withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            [self alertWithMessage:@"Iremos analisar a situação" andTitle:@"Pedimos desculpa"];
            
        } else {
            
            [self alertWithError:error];
        }
    }];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 3:
            [self alertCourierNotAppear];
            break;
        case 2:
            [self alertCancel];
            
            break;
        case 1:
            
            [self alertOther];
            break;
        default:
            break;
    }
}


- (void) alertCancel {
    
    if(self.order.state == OrderStateWatingCourier || self.order.state == OrderStateInit)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancelar"
                                                        message:@"Pretende cancelar? Como ainda não tem estafeta atribuido o cancelamento não terá qualquer custo"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Sim", @"Não", nil];
        [alert show];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancelar"
                                                        message:@"Ao cancelar a recepção o valor da encomenda será cobrado. Pretende cancelar?"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Sim", @"Não", nil];
        [alert show];
        
    }
    
    
    
}

- (void) alertOther{
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Outro problema" message:@"Explique a situação" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Enviar", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    
    
    
    
    
}

- (void)otherHandlerWithAlertView:(UIAlertView*)alertView {
    
    UITextField *textfield = [alertView textFieldAtIndex: 0];
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] createOrderProduct:self.order withDescription:textfield.text withBlock:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
           
            [SVProgressHUD showSuccessWithStatus:@"Obrigado pelo contacto" maskType:SVProgressHUDMaskTypeBlack];
           
            
        } else {
            
            [self alertWithError:error];
        }
    }];

}


- (void)cancelHandler {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] cancelWithOrder:self.order block:^(Order *order, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if(!error) {
            
            self.order = order;
            [self updateUI];
            
        } else {
            
            [self alertWithError:error];
        }
        
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if([alertView.title isEqualToString:@"Cancelar"]){
        
        switch (buttonIndex) {
                
            case 0:
                
                [self cancelHandler];

                break;
                
            default:
                break;
        }
    } else {
        
        switch (buttonIndex) {
                
            case 1:
                
                [self otherHandlerWithAlertView:alertView];
                
                break;
                
            default:
                break;
        }
        
    }
}

#pragma mark - Util

- (NSString *)stringFromDate:(NSDate*)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
    
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (double)maxBetween:(double)a and:(double)b {
    
    if(a>b){
        
        return a;
    
    } else {
    
        return b;
    }
    
}

#pragma mark - timer

- (void) updateDeliveryTime {
    
    NSTimeInterval dateTime = [self.hourFinishWaitTime timeIntervalSinceNow];
    
    if(dateTime <= 0) {
        [self stopTimer];
        dateTime = 0;
    }
    
    self.timeToRececeiveLabel.text = [NSString stringWithFormat:@"%@ s", [self stringFromTimeInterval:dateTime]];
}

- (void) startTimer {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1  target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
    
}

- (void) timerTick {
    
    [self updateDeliveryTime];
}

- (void) stopTimer {
    
    [self.timer invalidate];
    self.timer = nil;
}
- (IBAction)showOrderAction:(id)sender {
    
    OrderProductDetailViewController *orderProductDetailViewController = [[OrderProductDetailViewController alloc] init];
    
    orderProductDetailViewController.order = self.order;
    
    [self.navigationController pushViewController:orderProductDetailViewController animated:YES];
    
}


- (IBAction)requestInvoiceAction:(id)sender {
}


- (void) notificationAction:(NSNotification *) postNotification {
    
    
    Order *order = (Order*) postNotification.userInfo[@"order"];
    
    Message *message = (Message *) postNotification.userInfo[@"message"];
    
    NSArray *buttonArray;
    UIImage *icon;
    NSString *subtitle;
    NSString *title;
    buttonArray = [NSArray arrayWithObjects:@"Ver",@"Agora não", nil];
    
    if(!message) {
        
        icon = [UIImage imageNamed:@"ordersIcon" ];
        
        subtitle = @"A encomenda foi actualizada";
        
        title = order.restaurant.name;
        
        if([order.objectId isEqualToString:self.order.objectId]) {
            
            return;
        }
        
    } else {
        
        icon = [UIImage imageNamed:@"messageIcon" ];
        
        subtitle = @"Nova mensagem";
        
        title = message.order.restaurant.name;
        
        [[ServiceManager sharedInstance] seenMessage:message];
        
    }
    
    MPGNotification *notification = [[MPGNotification alloc] init];
    
    notification = [MPGNotification notificationWithTitle:title subtitle:subtitle backgroundColor:[UIColor whiteColor] iconImage:icon];
    [notification setButtonConfiguration:buttonArray.count withButtonTitles:buttonArray];
    notification.duration = 5.0;
    notification.swipeToDismissEnabled = NO;
    
    [notification setButtonHandler:^(MPGNotification *notification, NSInteger buttonIndex) {
        
        if(buttonIndex == notification.firstButton.tag) {
            
            Order *openOrder;
            
            if(!message) {
                
                openOrder = order;
                
            } else {
                
                openOrder = message.order;
            }
            
            OrderInProgressViewController *orderInProgressViewController = [[OrderInProgressViewController alloc] initWithNibName:@"OrderInProgressViewController" bundle:nil];
            
            orderInProgressViewController.order = openOrder;
            
            if(!order) {
                
                ChatViewController *chat = [ChatViewController messagesViewController];
                
                chat.order = openOrder;
                
                [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],orderInProgressViewController, chat] animated:YES];
                
            } else {
                
                [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],orderInProgressViewController] animated:YES];
            }
            
            
        }
    }];
    
    [notification setTitleColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    [notification setSubtitleColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    
    
    [notification setAnimationType:MPGNotificationAnimationTypeDrop];
    
    [notification show];
    
}






@end
