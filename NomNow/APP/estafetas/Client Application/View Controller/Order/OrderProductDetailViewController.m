//
//  OrderProductDetailViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 12/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderProductDetailViewController.h"

@interface OrderProductDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation OrderProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"Produtos";
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.order.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    OrderProduct *orderProduct = self.order.products[indexPath.row];
    
    Product *product = orderProduct.product;
    
    cell.textLabel.text = product.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Preço: %@ Quantidade: %@",orderProduct.realPrice, orderProduct.quantity];
    
    return cell;
    
}



@end
