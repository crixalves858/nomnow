//
//  OrderInProgressViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "ClientBaseViewController.h"
#import "OrderUpdateViewController.h"
#import "ClientServiceManager.h"
#import "OrderCancelClientNotAppearViewController.h"
#import <FinalStarRatingBar.h>
#import "RatingInClientViewController.h"
#import "OrderProductDetailViewController.h"
#import "ChatViewController.h"

@interface OrderInProgressViewController : ClientBaseViewController <UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) Order *order;

- (id) initWithOrderId:(NSString*) orderId;


@end
