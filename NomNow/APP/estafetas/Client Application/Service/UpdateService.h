//
//  UpdateService.h
//  estafetas
//
//  Created by Cristiano Alves on 10/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "OrderStateUpdate.h"
#import "ClientServiceManager.h"
#import "Message.h"

@interface UpdateService : NSObject

+ (instancetype) sharedInstance;

- (void) startService;

- (void) stopService;

@end
