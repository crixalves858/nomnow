//
//  UpdateService.m
//  estafetas
//
//  Created by Cristiano Alves on 10/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "UpdateService.h"

@interface UpdateService ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation UpdateService

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (void) startService {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: TIMER_STEP target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats: YES];
    }
}

- (void) stopService {
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void) timerTick {
    
    [self stopService];
    
    [[ClientServiceManager sharedInstance] getUpdateOrdersNotSeenWithBlock:^(NSArray *orderStateUpdates, NSError *error) {
        
        if(!error) {
            
            for(int i = 0; i < orderStateUpdates.count; i++) {
                
                OrderStateUpdate *orderStateUpdate = orderStateUpdates[i];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Order%@", orderStateUpdate.order.objectId]
                                                                    object:self
                                                                  userInfo:@{@"order":orderStateUpdate.order.objectId}];
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Notification"]
                                                                    object:self
                                                                  userInfo:@{@"order":orderStateUpdate.order}];
                orderStateUpdate.seen = YES;
            }
            
            [PFObject saveAllInBackground:orderStateUpdates block:^(BOOL succeeded, NSError *error) {
                
                
                [[ClientServiceManager sharedInstance] checkNotReadMessageWithBlock:^(NSArray *messages, NSError *error) {
                    
                    if(!error) {
                        
                        for(int i = 0; i<messages.count; i++) {
                            Message *message = messages[i];
                            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Message%@", message.order.objectId]
                                                                                object:self
                                                                              userInfo:@{@"order":message.order.objectId}];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"Notification"]
                                                                                object:self
                                                                              userInfo:@{@"message":message}];
                        }
                    }
                    
                    [self startService];

                }];
                
            }];
        }
        
    }];
}


@end
