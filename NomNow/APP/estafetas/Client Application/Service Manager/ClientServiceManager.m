//
//  ClientServiceManager.m
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientServiceManager.h"

@implementation ClientServiceManager


#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

#pragma mark - SignUp

- (void)signUpCreateClientDataWithFirstName:(NSString *)firstName lastName:(NSString *)lastName user:(User *)user phoneNumber:(NSString *)phoneNumber andImage:(UIImage*)image block:(void (^)(BOOL, NSError *))block {
    
    Client *client = [Client clientWithFirstName:firstName lastName:lastName];
    
    user.client = client;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    PFFile *imageFile = [PFFile fileWithData:imageData];
    
    client.photo = imageFile;
    
    client.courierData.phoneNumber = phoneNumber;
    
    [ACL clientACLBackgroundWithUser:user block:^(BOOL succeeded, NSError *error) {
        
        if(succeeded) {
            
            [user.client saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                [user saveInBackground];
                
                //notifications
                [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:@"user"];
                [[PFInstallation currentInstallation] saveEventually];
                
                block(succeeded,error);
                
            }];
            
        } else {
            
            block(succeeded,error);
        }
        
        
    }];
    
    
}

- (void)signUpWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email password:(NSString *)password phoneNumber:(NSString *)phoneNumber andImage:(UIImage*)image block:(void(^)(BOOL succeeded, NSError *error)) block{
    
    User *user = [[User alloc]init];
    user.email = email;
    user.password = password;
    user.username = email;
    
    user.ACL = [ACL privateACL];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *errorSignUp) {
        
        if (!errorSignUp) {
            
            [self signUpCreateClientDataWithFirstName:firstName lastName:lastName user:user phoneNumber:phoneNumber andImage:image block:block];
            
        }
        else {
            
            block(succeeded,errorSignUp);
        }
        
        
    }];
    
}

#pragma mark - Restaurants

- (void) getNearRestaurantsWithLatitude:(double)latitude andLongitude:(double)longitude block:(void (^)(NSArray *restaurants, NSArray *states, NSError *))block {
    
    PFGeoPoint *geoPoint = [[PFGeoPoint alloc] init];
    
    [geoPoint setLongitude:longitude];
    [geoPoint setLatitude:latitude];
    
    [PFCloud callFunctionInBackground : @"getNearRestaurants" withParameters : @{@"location":geoPoint} block:^(NSArray *object, NSError *error) {
        
        NSArray *restaurants = object[0];
        NSArray *states = object[1];
        
        block(restaurants, states, error);
        
    }];
    
}

- (void) getProductsByRestaurant:(Restaurant *)restaurant andCategory:(CategoryOfProduct *)category block:(void (^)(NSArray *, NSError *))block {
    
    PFQuery *query = [Product query];
    
    [query whereKey:@"restaurant" equalTo:restaurant];
    [query whereKey:@"category" equalTo:category];
    
    [query findObjectsInBackgroundWithBlock:block];
    
}

- (void) getProductsByRestaurant:(Restaurant *)restaurant block:(void (^)(NSMutableArray *, NSError *))block {
    
    PFQuery *query = [Product query];
    
    [query includeKey:@"restaurant"];
    
    [query whereKey:@"restaurant" equalTo:restaurant];
    
    [query includeKey:@"category"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSMutableArray *productBycategories = [[NSMutableArray alloc] init];
        
        for( int i = 0; i < restaurant.categories.count; i++)
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            productBycategories[i] = array;
            
            CategoryOfProduct *catRestaurant = restaurant.categories[i];
            for( int i = 0; i < objects.count; i++) {
                
                Product *product = objects[i];
                CategoryOfProduct *catObject = product.category;
                
                if( [ catObject.objectId isEqualToString:catRestaurant.objectId])
                    [array addObject:product];
                
            }
            
            
        }
        block(productBycategories, error);
        
    }];
    
}

#pragma mark - Order


- (void) checkoutCart:(Cart*)cart withDeliveryAddress:(NSString *)deliveryAddress location:(PFGeoPoint*)location doorNumber:(NSString*)doorNumber zipCode:(NSString*)zipCode floor:(NSString*)floor andObservations:(NSString*)observations block:(void(^)(Order *order, NSError *error)) block {
    
    Order *order = [[Order alloc]init];
    
    order.state = OrderStateInit;
    order.orderDate = [NSDate date];
    order.deliveryAddress = deliveryAddress;
    order.client = [[Session sharedInstance] currentUser];
    order.aceptedByClient = YES;
    order.deliveryLocation = location;
    order.deliveryDoorNumber = doorNumber;
    order.deliveryZipCode = zipCode;
    order.pay = NO;
    order.deliveryObservations = observations;
    order.deliveryFloor = floor;
    OrderClientData *orderClientData = [[OrderClientData alloc]init];
    PFACL *acl = [ACL privateACL];
    
    [acl setReadAccess:YES forUser:[[Session sharedInstance]currentUser]];
    [acl setWriteAccess:YES forUser:[[Session sharedInstance]currentUser]];
    
    order.ACL = acl;
    orderClientData.ACL = acl;
    
    orderClientData.validationPIN = [self generateValidationPIN];
    order.clientData = orderClientData;
    
    Restaurant *restaurant = nil;
    
    
    if (cart.products.count > 0) {
        
        OrderProduct *orderProduct = cart.products[0];
        restaurant = orderProduct.product.restaurant;
    }
    order.restaurant = restaurant;
    order.products = cart.products;
    
    
    
    
    [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (! error) {
            
            order.client = [ClientSession sharedInstance].currentUser;
            
            for (int i = 0; i < cart.products.count; i++) {
                
                OrderProduct *orderProduct = cart.products[i];
                orderProduct.ACL = acl;
                orderProduct.order = order;
                
            }
            
            [PFObject saveAllInBackground:cart.products block:^(BOOL succeeded, NSError *error) {
                
                if(!error) {
                    
                    [self findEligibleCouriersWithOrder:order];
                    

                    
                } else {
                    
                    [order deleteInBackground];
                }
                
                block(order,error);
            }];
            
            
            
            
        } else {
            
            block(order, error);
            
        }
    }];
}

- (int)generateValidationPIN {
    int lowerBound = 1000;
    int upperBound = 9999;
    int validationPIN = lowerBound + arc4random() % (upperBound - lowerBound);
    return validationPIN;
}

- (void) findEligibleCouriersWithOrder:(Order*)order {
    
    [PFCloud callFunctionInBackground : @"findEligibleCouriers" withParameters : @{@"order":order.objectId} block:^(id object, NSError *error) {
        
        if ( error) {
            
            NSLog(@"erro: %@", error.description);
            
        }
    }];
}

- (void) checkOrderState:(Order *)order block:(void (^)(FindCourierState ))block{
    
    [PFCloud callFunctionInBackground:@"checkOrderState" withParameters:@{@"order":order.objectId} block:^(id object, NSError *error) {
        
        
        
        if(!error)
        {
            NSNumber *number = object;
            FindCourierState state = number.integerValue;
            block(state);
            
        } else {
            NSString *str = error.userInfo[@"error"];
            FindCourierState state = [str integerValue];
            block(state);
            
        }
    }];
}

- (void) getMyOrdersWithBlock:(void(^)(NSArray *orders, NSError *error)) block {
    
    PFQuery *query = [Order query];
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query includeKey:@"courier"];
    [query includeKey:@"client"];
    [query includeKey:@"client.client"];
    [query includeKey:@"courier.courier"];
    [query includeKey:@"courier.courier.clientData"];
    [query includeKey:@"clientData"];
    [query includeKey:@"commission"];
    
    [query whereKey:@"client" equalTo:[[ClientSession sharedInstance] currentUser]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error) {
            
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for(int j = 0; j < objects.count ; j ++) {
                
                Order *order = objects[j];
                
                for(int i=0;i<order.products.count; i++) {
                    
                    OrderProduct *product = order.products[i];
                    
                    [array addObject:product.product];
                }
            }
            
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                
                
                block(objects, error);
                
            }];
            
            
        } else {
            block(objects, error);
        }
        
    }];
    
}

- (void) getMyOrdersWithState:(OrderState)state block:(void (^)(NSArray *, NSError *))block {
    
    PFQuery *query = [Order query];
    
    [query whereKey:@"client" equalTo:[[ClientSession sharedInstance] currentUser]];
    [query includeKey:@"commission"];
    [query whereKey:@"state" equalTo:[NSNumber numberWithInteger:state]];
    
    [query includeKey:@"products"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error) {
            
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            Order *order = [objects firstObject];
            
            for(int i=0;i<order.products.count; i++) {
                
                OrderProduct *product = order.products[i];
                
                [array addObject:product.product];
            }
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                
                
                block(objects, error);
                
            }];
            
            
        } else {
            block(objects, error);
        }
        
    }];
    
}



- (void) acceptedProductsWithOrder:(Order*)order block:(void(^)(Order *order, NSError *error)) block {
    
    [PFCloud callFunctionInBackground : @"clientAccepted" withParameters : @{@"order":order.objectId} block:^(id object, NSError *error) {
        
        block(object,error);
        
    }];
    
}

- (void) clientNotAppearToRecebeOrder:(Order*)order response:(NSString*)response block:(void(^)(Order *order, NSError *error)) block {
    
    [PFCloud callFunctionInBackground : @"clientNotAppearClientResponse" withParameters : @{@"order":order.objectId, @"clientResponse":response} block:^(id object, NSError *error) {
        
        block(object,error);
        
    }];
    
}

- (void) getMyActiveOrdersWithCourierWithBlock:(void(^)(NSArray *objects, NSError *error))block {
    
    
    PFQuery *query = [Order query];
    
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query includeKey:@"commission"];
    [query whereKey:@"state" greaterThanOrEqualTo:[NSNumber numberWithInteger:OrderStateHaveCourier]];
    
    [query whereKey:@"state" lessThan:[NSNumber numberWithInteger:OrderStateOrderDelivery]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error) {
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for(int j=0; j<objects.count; j++) {
                
                Order *order = (Order*)objects[j];
                
                for(int i=0;i<order.products.count; i++) {
                    
                    OrderProduct *product = order.products[i];
                    
                    [array addObject:product.product];
                }
            }
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                block(objects, error);
                
            }];
        } else {
            
            block(objects, error);
        }
    }];
    
}

- (void) getOrder:(Order*)order block:(void(^)(Order *order, NSError *error)) block {
    
    PFQuery *query = [Order query];
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query includeKey:@"courier"];
    [query includeKey:@"client"];
    [query includeKey:@"client.client"];
    [query includeKey:@"client.client.courierData"];
    [query includeKey:@"client.client.privateData"];
    [query includeKey:@"commission"];
    [query includeKey:@"courier.courier"];
    [query includeKey:@"courier.courier.clientData"];
    [query includeKey:@"clientData"];
    [query getObjectInBackgroundWithId:order.objectId block:^(PFObject *object, NSError *error) {
        
        if(!error) {
            
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            Order *order = (Order*)object;
            
            for(int i=0;i<order.products.count; i++) {
                
                OrderProduct *product = order.products[i];
                
                [array addObject:product.product];
            }
            
            [PFObject fetchAllInBackground:array block:^(NSArray *product, NSError *error) {
                
                block(order, error);
                
            }];
            
            
        } else {
            block(order, error);
        }

    }];
}

- (Order*) getOrder:(Order*)order {
    
    PFQuery *query = [Order query];
    [query includeKey:@"products"];
    [query includeKey:@"restaurant"];
    [query includeKey:@"courier"];
    [query includeKey:@"courier.courier"];
    [query includeKey:@"clientData"];
    [query includeKey:@"commission"];
    Order *newOrder = (Order*)[query getObjectWithId:order.objectId];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(int i=0;i<newOrder.products.count; i++) {
        
        OrderProduct *product = order.products[i];
        
        [array addObject:product.product];
    }
    
    [PFObject fetchAll:array];
    
    return newOrder;
}

- (void) calcEstimatedPriceAndWaitTimeWithRestaurant:(Restaurant*)restaurant andClientLocation:(PFGeoPoint *)clientLocation block:(void(^)(NSArray *objects, NSError *error))block {
    
    [PFCloud callFunctionInBackground : @"calcEstimatedPriceAndTime" withParameters : @{@"restaurant":restaurant.objectId, @"clientLocation" : clientLocation} block:block];
}


- (void) getUpdateOrder:(Order*)order withState:(OrderState)orderState block:(void(^)(OrderStateUpdate *orderStateUpdate, NSError *error))block{
    
    PFQuery *query = [OrderStateUpdate query];
    
    [query whereKey:@"order" equalTo:order];
    [query whereKey:@"state" equalTo:[NSNumber numberWithInteger:orderState]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        OrderStateUpdate *orderStateUpdate;
        if(!error && objects.count>0) {
            
            orderStateUpdate = [objects firstObject];
        }
        
        block(orderStateUpdate, error);
    }];
    
}

- (void) getUpdateOrdersNotSeenWithBlock:(void(^)(NSArray *orderStateUpdate, NSError *error))block {
    
    PFQuery *query = [OrderStateUpdate query];
    
    PFQuery *queryOrder = [Order query];
    
    [queryOrder includeKey:@"products"];
    [queryOrder includeKey:@"restaurant"];
    [queryOrder includeKey:@"courier"];
    [queryOrder includeKey:@"client"];
    [queryOrder includeKey:@"client.client"];
    [queryOrder includeKey:@"client.client.courierData"];
    [queryOrder includeKey:@"client.client.privateData"];
    [queryOrder includeKey:@"commission"];
    [queryOrder includeKey:@"courier.courier"];
    [queryOrder includeKey:@"courier.courier.clientData"];
    [queryOrder includeKey:@"clientData"];
    
    if(![[ClientSession sharedInstance] currentUser]) {
        
        block([[NSArray alloc] init], nil);
        
    } else {
        
        [queryOrder whereKey:@"client" equalTo:[[ClientSession sharedInstance] currentUser]];
        
        [queryOrder includeKey:@"restaurant"];
        
        [query whereKey:@"order" matchesQuery:queryOrder];
        
        [query includeKey:@"order"];
        
        [query includeKey:@"order.products"];
        [query includeKey:@"order.restaurant"];
        [query includeKey:@"order.courier"];
        [query includeKey:@"order.client"];
        [query includeKey:@"order.client.client"];
        [query includeKey:@"order.client.client.courierData"];
        [query includeKey:@"order.client.client.privateData"];
        [query includeKey:@"order.commission"];
        [query includeKey:@"order.courier.courier"];
        [query includeKey:@"order.courier.courier.clientData"];
        [query includeKey:@"order.clientData"];
        
        [query whereKey:@"seen" equalTo:@NO];
        
        [query findObjectsInBackgroundWithBlock:block];
        
    }
}

- (void) cancelWithOrder:(Order*)order block:(void(^)(Order *order, NSError *error))block {
    
    
    if(order.state == OrderStateWatingCourier || order.state == OrderStateInit) {
        
        order.state = OrderStateCanceleByClient;
        
    } else {
        
        order.state = OrderStateCanceleByClientWithCourier;
    }
    
    [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        block(order, error);
    }];
    
}

- (void) createCardWithName:(NSString*)name number:(NSNumber*)number month:(NSNumber*)month year:(NSNumber*)year cvv:(NSString*)cvv country:(NSString*)country withBlock:(void(^)(Card *card, NSError *error)) block {
    
    [SwitchPayments createCardTokenWithName:name number:number month:month year:year cvv:cvv country:country withBlock:^(SPCard *spCard, NSError *error) {
    
        if(!error) {
            
            [PFCloud callFunctionInBackground : @"createCard" withParameters : @{@"token":spCard.token} block:^(id object, NSError *error) {
                
                if(!error) {
                    
                    User *user = object;
                    [ClientSession sharedInstance].currentUser = user;
                    block(user.client.privateData.card,error);
                    
                } else {
                    
                    block(nil, error);
                }
                
               
                
            }];
            
        } else {
            
            block(nil, error);
        }
        
    }];
    
    
}


@end
