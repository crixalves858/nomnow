//
//  ClientServiceManager.h
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ServiceManager.h"
#import "ClientSession.h"
#import "OrderStateUpdate.h"
#import "Card.h"
#import "SwitchPayments.h"

@interface ClientServiceManager : ServiceManager

+ (instancetype) sharedInstance;

#pragma mark - SignUp

- (void) signUpWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email password:(NSString *)password phoneNumber:(NSString *)phoneNumber  andImage:(UIImage*)image block:(void(^)(BOOL succeeded, NSError *error)) block;

- (void)signUpCreateClientDataWithFirstName:(NSString *)firstName lastName:(NSString *)lastName user:(User *)user phoneNumber:(NSString *)phoneNumber andImage:(UIImage*)image block:(void (^)(BOOL, NSError *))block;

#pragma mark - Restaurants

- (void) getNearRestaurantsWithLatitude:(double)latitude andLongitude:(double)longitude block:(void(^)(NSArray *restaurants, NSArray *states, NSError *error)) block;

- (void) getProductsByRestaurant:(Restaurant *)restaurant andCategory:(CategoryOfProduct *)category block:(void(^)(NSArray *products, NSError *error)) block;

- (void) getProductsByRestaurant:(Restaurant *)restaurant block:(void (^)(NSMutableArray *productsByCategories, NSError *error))block;

#pragma mark - Order

- (void) checkoutCart:(Cart*)cart withDeliveryAddress:(NSString *)deliveryAddress location:(PFGeoPoint*)location doorNumber:(NSString*)doorNumber zipCode:(NSString*)zipCode floor:(NSString*)floor andObservations:(NSString*)observations block:(void(^)(Order *order, NSError *error)) block;

//- (void) checkOrderState:(Order*)order block:(void(^)(FindCourierState state)) block;

- (void) getMyOrdersWithBlock:(void(^)(NSArray *orders, NSError *error)) block;

- (void) getMyOrdersWithState:(OrderState)state block:(void(^)(NSArray *orders, NSError *error)) block;

- (void) acceptedProductsWithOrder:(Order*)order block:(void(^)(Order *order, NSError *error)) block;

- (void) clientNotAppearToRecebeOrder:(Order*)order response:(NSString*)response block:(void(^)(Order *order, NSError *error)) block;

- (void) getMyActiveOrdersWithCourierWithBlock:(void(^)(NSArray *objects, NSError *error))block;

- (void) getOrder:(Order*)order block:(void(^)(Order *order, NSError *error)) block;

- (void) calcEstimatedPriceAndWaitTimeWithRestaurant:(Restaurant*)restaurant andClientLocation:(PFGeoPoint *)clientLocation block:(void(^)(NSArray *objects, NSError *error))block; 

- (void) getUpdateOrder:(Order*)order withState:(OrderState)orderState block:(void(^)(OrderStateUpdate *orderStateUpdate, NSError *error))block;

- (Order*) getOrder:(Order*)order;

- (void) getUpdateOrdersNotSeenWithBlock:(void(^)(NSArray *orderStateUpdate, NSError *error))block;

- (void) cancelWithOrder:(Order*)order block:(void(^)(Order *order, NSError *error))block;

- (void) createCardWithName:(NSString*)name number:(NSNumber*)number month:(NSNumber*)month year:(NSNumber*)year cvv:(NSString*)cvv country:(NSString*)country withBlock:(void(^)(Card *card, NSError *error)) block;

@end
