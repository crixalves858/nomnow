//
//  ClientSession.h
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Session.h"
#import <CoreLocation/CoreLocation.h>

@interface ClientSession : Session <NSCoding>

@property (nonatomic, strong) Cart *cart;
@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) CLLocation *location;

+ (instancetype)sharedInstance;


@end
