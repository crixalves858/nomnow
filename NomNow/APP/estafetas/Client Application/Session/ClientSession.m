//
//  ClientSession.m
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientSession.h"


@interface ClientSession ()



@end

@implementation ClientSession 

@synthesize cart = _cart;

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
    
}



- (User*) currentUser {
    
    return [[Session sharedInstance] currentUser];
}

- (void) resetValues {
    
    self.cart = nil;
}


-(Cart*) cart {
    
    if (_cart == nil) {
        
        _cart = [[Cart alloc] init];
    }
    
    return _cart;
}

#pragma mark - NSCoding implementation for singleton

- (id)initWithCoder:(NSCoder *)aDecoder {
    // Unarchive the singleton instance.
    
    ClientSession *instance = [ClientSession sharedInstance];
    
    [instance setCart:[aDecoder decodeObjectForKey:@"cart"]];
    
    
    
    return instance;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
  
    ClientSession *instance = [ClientSession sharedInstance];
    
    [aCoder encodeObject:[instance cart] forKey:@"cart"];
      
}


@end
