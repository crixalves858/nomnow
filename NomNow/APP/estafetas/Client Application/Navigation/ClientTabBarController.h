//
//  ClientTabBar.h
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantsNavigationController.h"
#import "CartNavigationController.h"
#import "OrdersClientNavigationController.h"
#import "ProfileViewController.h"

#import "UpdateService.h"


@interface ClientTabBarController : UITabBarController

- (id) init;
- (void) changeActiveTabWithIndex:(int)index;


@end
