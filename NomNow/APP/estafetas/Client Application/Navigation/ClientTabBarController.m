//
//  ClientTabBar.m
//  estafetas
//
//  Created by Cristiano Alves on 23/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientTabBarController.h"

@implementation ClientTabBarController

- (id) init{
    
    self = [super init];
    
    CartNavigationController *cartNav = [[CartNavigationController alloc] init];
    RestaurantsNavigationController *restaurantsNav = [[RestaurantsNavigationController alloc] init];
    OrdersClientNavigationController *ordersNav = [[OrdersClientNavigationController alloc] init];
    
    ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
    UINavigationController *profileNav = [[UINavigationController alloc] initWithRootViewController:profileViewController];
    
    profileNav.navigationBar.translucent = NO;
  
    
    cartNav.navigationBar.translucent = NO;

    self.viewControllers = @[restaurantsNav, cartNav, ordersNav, profileNav];
    
    UITabBar *tabBar = self.tabBar;
    
    tabBar.translucent = NO;
    
    tabBar.tintColor = [UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1];
    
    UITabBarItem *restaurantsItem = tabBar.items[0];
    UITabBarItem *cartItem = tabBar.items[1];
    UITabBarItem *orderItem = tabBar.items[2];
    UITabBarItem *profileItem = tabBar.items[3];
    
    
    restaurantsItem.image=[UIImage imageNamed:@"restaurantsIcon"];
    cartItem.image=[UIImage imageNamed:@"cartIcon"];
    orderItem.image=[UIImage imageNamed:@"ordersIcon"];
    profileItem.image=[UIImage imageNamed:@"profileIcon"];

    
    restaurantsItem.title = @"Restaurantes";
    cartItem.title = @"Carrinho";
    orderItem.title = @"Encomendas";
    profileItem.title = @"Perfil";
    
    if([[ClientSession sharedInstance] currentUser] != nil) {
        
        [[UpdateService sharedInstance] startService];
     
    }
    
    return self;
}

- (void) changeActiveTabWithIndex:(int)index {
    
    int controllerIndex = index;
    
    UITabBarController *tabBarController = self;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = [[tabBarController.viewControllers objectAtIndex:controllerIndex] view];
    
    // Transition using a page curl.
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.5
                       options:(controllerIndex > tabBarController.selectedIndex ? UIViewAnimationOptionTransitionCurlUp : UIViewAnimationOptionTransitionCurlDown)
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = controllerIndex;
                        }
                    }];

}

@end
