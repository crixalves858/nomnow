//
//  AppDelegate.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "ServiceManager.h"
#import "ClientServiceManager.h"
#import "CourierServiceManager.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>

#import "Cancelation.h"
#import "ApplicationProblem.h"
#import "DeliveryProblem.h"
#import "MealProblem.h"
#import "Rating.h"
#import "Order.h"
#import "OrderClientData.h"
#import "Commission.h"
#import "OrderProduct.h"
#import "Message.h"
#import "Restaurant.h"
#import "CategoryOfProduct.h"
#import "Product.h"
#import "Schedule.h"
#import "Courier.h"
#import "CourierClientData.h"
#import "CourierPrivateData.h"
#import "Client.h"
#import "ClientCourierData.h"
#import "ClientPrivateData.h"
#import "User.h"
#import "Session.h"
#import "EligibleCourier.h"
#import "OrderStateUpdate.h"
#import "RatingViewController.h"
#import "OrderProblem.h"
#import "Card.h"
#import "RatingRestaurant.h"

#import "RestaurantsViewController.h"
#import "ChangeLocationViewController.h"
#import "LoginNavigationController.h"
#import "GoToRestaurantViewController.h"

#import "ChatViewController.h"

#import "SwitchPayments.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (void)parseSetup {
    
    [Parse setApplicationId:@"JKpsAZ9IoyUjy3z0fg78dOMOm4UmjL4r4a2PA7eM"
                  clientKey:@"5E5Rrwt6K7u4idsUAHb5MKppSMkTDQG1W9P64KGz"];
    
    [PFFacebookUtils initializeFacebook];
    
    [Cancelation registerSubclass];
    [ApplicationProblem registerSubclass];
    [DeliveryProblem registerSubclass];
    [MealProblem registerSubclass];
    [Rating registerSubclass];
    [Order registerSubclass];
    [OrderClientData registerSubclass];
    [OrderProduct registerSubclass];
    [Commission registerSubclass];
    [Message registerSubclass];
    [Restaurant registerSubclass];
    [CategoryOfProduct registerSubclass];
    [Product registerSubclass];
    [Schedule registerSubclass];
    [Courier registerSubclass];
    [CourierClientData registerSubclass];
    [CourierPrivateData registerSubclass];
    [Client registerSubclass];
    [ClientCourierData registerSubclass];
    [ClientPrivateData registerSubclass];
    [EligibleCourier registerSubclass];
    [User registerSubclass];
    [OrderStateUpdate registerSubclass];
    [OrderProblem registerSubclass];
    [Card registerSubclass];
    [RatingRestaurant registerSubclass];

}



-(void)viewSetup {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    LoginNavigationController *loginNavigationController = [[LoginNavigationController alloc] init];
    
    self.window.rootViewController = loginNavigationController;

    
    [self.window makeKeyAndVisible];

}

- (void) switchPaymentSetup {
    
    [SwitchPayments setupWithPublicKey:@"7adac0098011b4824bfef620b414ff1db3e6fc8c553a109e" andEnvironment:SwitchPayments.SANDBOX];

}

/*- (void) notificationRegister {
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
}*/

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self parseSetup];
    
    [self switchPaymentSetup];
    
    [self viewSetup];
    
    //   [self notificationRegister];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:[PFFacebookUtils session]];
}



- (void)changeRootViewController:(UIViewController*)viewController animated: (BOOL)flag{
    
    if(flag){
        
        UIView *myView1 = self.window.rootViewController.view;
        
        UIView *myView2 = viewController.view;
        
        myView2.frame = self.window.bounds;
        
        
        [self.window addSubview:myView2];
        
        CATransition* transition = [CATransition animation];
        transition.startProgress = 0;
        transition.endProgress = 1.0;
        transition.type = kCATransitionPush;
        transition.subtype = kCAGravityResize;
        transition.duration = 1.0;
        
        // Add the transition animation to both layers
        [myView1.layer addAnimation:transition forKey:@"transition"];
        [myView2.layer addAnimation:transition forKey:@"transition"];
        
        myView1.hidden = YES;
        myView2.hidden = NO;
    }
    
    self.window.rootViewController = viewController;
    
}

//melhorar!!!!!
/*#warning melhorar
UILocalNotification *localNotification;

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    localNotification = notification;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Encomenda actualizada"
                                                       message:notification.alertBody delegate:self
                                             cancelButtonTitle:@"Ignorar" otherButtonTitles:@"Ver agora", nil];
    
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]){
        //cancel clicked ...do your action
    }else{
        ClientTabBarController *clientTab = (ClientTabBarController*) self.window.rootViewController;
        UINavigationController *nav = clientTab.viewControllers[2];
        
        [nav popToRootViewControllerAnimated:ßYES];
        
        OrderInProgressViewController *orderInProgressViewController = [[OrderInProgressViewController alloc] initWithOrderId:localNotification.userInfo[@"order"]];
    
        [nav pushViewController:orderInProgressViewController animated:YES];
        
        [clientTab changeActiveTabIndex:2];

    }
}*/

- (void)applicationWillResignActive:(UIApplication *)application {
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
     [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
