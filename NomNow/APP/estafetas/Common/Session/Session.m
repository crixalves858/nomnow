//
//  Session.m
//  estafetas
//
//  Created by Cristiano Alves on 05/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Session.h"

@implementation Session

@synthesize currentUser = _currentUser;

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}


- (void) resetValues {
    
    self.currentUser = nil;
    self.userType = UserTypeUndefined;
    
}

- (User*) currentUser {
    
    if (!_currentUser )
    {
        _currentUser = (User*)[PFUser currentUser];
    }
    return _currentUser;
}

- (UserType) userType {
    
    User* user = self.currentUser;
    
    if(user.client != nil && user.courier != nil) {
        
         return UserTypeMulti;
        
    } else if (user.client != nil) {
        
       return UserTypeClient;
        
    } else if (user.courier != nil) {
        
        return UserTypeCourier;
    }
    
    return UserTypeUndefined;

}

@end
