//
//  Session.h
//  estafetas
//
//  Created by Cristiano Alves on 05/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Client.h"
#import "Constant.h"
#import "User.h"
#import "Courier.h"
#import "Cart.h"

@interface Session : NSObject

@property (nonatomic, strong) User *currentUser;
@property (nonatomic) UserType userType;


+ (instancetype)sharedInstance;

- (void)resetValues;



@end
