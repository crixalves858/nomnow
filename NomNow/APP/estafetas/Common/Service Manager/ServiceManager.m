//
//  ServiceManager.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ServiceManager.h"

@implementation ServiceManager

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

#pragma mark - Login and Logout

- (void)loginLoadDataWithUser:(PFUser *)pfUser block:(void (^)(BOOL, NSError *))block  {
    PFQuery *query = [User query];
    
    [query includeKey:@"client"];
    [query includeKey:@"client.courierData"];
    [query includeKey:@"client.privateData"];
    [query includeKey:@"courier"];
    [query includeKey:@"courier.privateData"];
    [query includeKey:@"courier.clientData"];
    
    [query getObjectInBackgroundWithId:pfUser.objectId block:^(PFObject *object, NSError *error) {
        
        if(! error) {
            
            User *user = (User*) object;
            
            //            if(user.client != nil && user.courier != nil) {
            //
            //                [[Session sharedInstance]setUserType:UserTypeMulti];
            //
            //            } else if (user.client != nil) {
            //
            //                [[Session sharedInstance] setUserType:UserTypeClient];
            //
            //            } else if (user.courier != nil) {
            //
            //                [[Session sharedInstance] setUserType:UserTypeCourier];
            //            }
            
            [[Session sharedInstance] setCurrentUser:user];
            block(YES,error);
        }
        else {
            
            block(NO,error);
        }
        
    }];
}

- (void)loginWithEmail:(NSString *)email password:(NSString *)password block:(void (^)(BOOL, NSError *))block {
    
    [PFUser logOut];
    
    [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *pfUser, NSError *error) {
        
        if ( !error) {
            
            [self loginLoadDataWithUser:pfUser block:block];
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

- (void) logout {
    
    [[Session sharedInstance] resetValues];
    [[ClientSession sharedInstance] resetValues];
    
    [PFUser logOut];
}


- (void)singUpCreateClientDataWithUser:(PFUser *)pUser block:(void (^)(BOOL, NSError *))block {
    User *user = (User*) pUser;
    
    PFACL *acl = [ACL privateACL];
    
    [acl setWriteAccess:YES forUser:user];
    [acl setReadAccess:YES forUser:user];
    
    [user setACL:acl];
    
    FBRequest *request = [FBRequest requestForMe];
    
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *userData = (NSDictionary *)result;
            NSString *firstName = userData[@"first_name"];
            NSString *lastName = userData[@"last_name"];
            NSString *facebookId = userData[@"id"];
            
            
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookId]];
            
            AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:URL]];
            
            [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                PFFile *photo = [PFFile fileWithData:responseObject];
                
                [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    
                    if(!error) {
                        
                        Client *client = [Client clientWithFirstName:firstName lastName:lastName];
                        client.photo = photo;
                        user.client = client;
                        
                        [ACL clientACLBackgroundWithUser:user block:^(BOOL succeeded, NSError *error) {
                            
                            if(succeeded) {
                                [user.client saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                    
                                    [user saveInBackground];
                                    
                                    //notifications
                                    [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:@"user"];
                                    [[PFInstallation currentInstallation] saveEventually];
                                    
                                    block(succeeded,error);
                                    
                                }];
                                
                            } else {
                                
                                block(succeeded,error);
                            }
                            
                            
                        }];
                    } else {
                        
                        block(succeeded, error);
                    }
                }];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Image error: %@", error);
            }];
            [requestOperation start];
            
        }
    }];
}

- (void) loginFacebookWithBlock:(void(^)(BOOL succeeded, NSError *error)) block {
    
    NSArray *permissionsArray = @[ @"user_about_me", @"user_birthday", @"user_location", @"email"];
    
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *pUser, NSError *error) {
        if (!pUser) {
            block(NO, error);
        } else if (pUser.isNew) {
            
            [self singUpCreateClientDataWithUser:pUser block:block];
            
        } else {
            
            [self loginLoadDataWithUser:(PFUser*)pUser block:block];
        }
    }];
}

#pragma mark - Save objects

#pragma mark save user

- (void) saveUser:(User*)user {
    
    [user saveEventually];
}

- (void) saveUser:(User*)user withBlock:(void(^)(BOOL succeeded, NSError *error)) block{
    
    [user saveEventually:block];
}

#pragma mark save client

- (void) saveClient:(Client*)client {
    
    [client saveEventually];
    
}

- (void) saveClient:(Client*)client withBlock:(void(^)(BOOL succeeded, NSError *error)) block {
    
    [client saveEventually:block];
    
}

#pragma mark - Message


- (void) sendMessageWithOrder:(Order*)order andMensage:(NSString*)message withBlock:(void(^)(Message *message, NSError *error)) block {
    
    Message *messageOrder = [[Message alloc]init];
    
    messageOrder.text = message;
    messageOrder.date = [NSDate date];
    messageOrder.order = order;
    messageOrder.sender = [[Session sharedInstance]currentUser];
    messageOrder.seen = NO;
    
    
    
    PFACL *acl = [ACL privateACL];
    
    if (![[Session sharedInstance]currentUser].client) {
        
        messageOrder.receiver = order.client;
        
    } else {
        
        messageOrder.receiver = order.courier;
    }
  
    [acl setWriteAccess:YES forUser:messageOrder.sender];
    [acl setReadAccess:YES forUser:messageOrder.sender];
    
    [acl setWriteAccess:YES forUser:messageOrder.receiver];
    [acl setReadAccess:YES forUser:messageOrder.receiver];
    
    messageOrder.ACL = acl;
    
    [messageOrder saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
       
        block(messageOrder, error);
    }];
    
}


- (void) getMessagesWithOrder:(Order*)order block:(void(^)(NSArray *messages, NSError *error)) block {
    
    PFQuery *query = [Message query];
    
    [query whereKey:@"order" equalTo:order];
    
    [query includeKey:@"sender"];
    [query includeKey:@"sender.client"];
    [query includeKey:@"sender.courier"];
    [query includeKey:@"receiver"];
    [query includeKey:@"receiver.client"];
    [query includeKey:@"receiver.courier"];
    
    [query findObjectsInBackgroundWithBlock:block];
    
}


- (void) updateOrder:(Order *)order state:(OrderState)state withBlock:(void(^)(BOOL succeeded, NSError *error))block {
    
    order.state = state;
    
    [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(!error) {
            
            
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

#pragma mark - rating

- (void) getRatingWithReviewer:(User *)reviewer rated:(User *)rated andOrder:(Order*)order block:(void (^)(Rating *rating, NSError *error))block {
    
    PFQuery *query = [Rating query];
    
    [query whereKey:@"reviewer" equalTo:reviewer];
    [query whereKey:@"rated" equalTo:rated];
    [query whereKey:@"order" equalTo:order];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        Rating *rating = nil;
        if(!error) {
            
            rating = [objects firstObject];
            
        }
        block(rating, error);
    }];
    
}

- (void) setRatingWithReviewer:(User *)reviewer rated:(User *)rated order:(Order*)order andRating:(NSUInteger)ratingValue block:(void (^)(Rating *rating, NSError *error))block {
    
    
    [self getRatingWithReviewer:reviewer rated:rated andOrder:(Order*)order block:^(Rating *rating, NSError *error) {
        if(!error && rating!=nil) {
            
            rating.value = [NSNumber numberWithInteger:ratingValue];
        } else {
            
            rating = [[Rating alloc]init];
            
            rating.order = order;
            rating.rated = rated;
            rating.reviewer = reviewer;
            rating.value = [NSNumber numberWithInteger:ratingValue];
            
        }
        
        [rating saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            block(rating, error);
            
        }];
    }];
}

- (void) getRatingWithReviewer:(User *)reviewer restaurant:(Restaurant *)restaurant andOrder:(Order*)order block:(void (^)(RatingRestaurant *rating, NSError *error))block {
    
    PFQuery *query = [RatingRestaurant query];
    
    [query whereKey:@"reviewer" equalTo:reviewer];
    [query whereKey:@"restaurant" equalTo:restaurant];
    [query whereKey:@"order" equalTo:order];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        RatingRestaurant *rating = nil;
        if(!error) {
            
            rating = [objects firstObject];
            
        }
        block(rating, error);
    }];
    
}

- (void) setRatingWithReviewer:(User *)reviewer restaurant:(Restaurant *)restaurant order:(Order*)order andRating:(NSUInteger)ratingValue block:(void (^)(RatingRestaurant *rating, NSError *error))block {
    
    
    [self getRatingWithReviewer:reviewer restaurant:restaurant andOrder:(Order*)order block:^(RatingRestaurant *rating, NSError *error) {
        if(!error && rating!=nil) {
            
            rating.value = [NSNumber numberWithInteger:ratingValue];
        } else {
            
            rating = [[RatingRestaurant alloc]init];
            
            rating.order = order;
            rating.restaurant = restaurant;
            rating.reviewer = reviewer;
            rating.value = [NSNumber numberWithInteger:ratingValue];
            
        }
        
        [rating saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            block(rating, error);
            
        }];
    }];
}


- (void) updateOrder:(Order *)order state:(OrderState)state andLocation:(CLLocation*)location withBlock:(void(^)(BOOL succeeded, NSError *error))block {
    
    order.state = state;
    
    [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            OrderStateUpdate *update = [[OrderStateUpdate alloc]init];
            update.order = order;
            update.state = state;
            update.location = [PFGeoPoint geoPointWithLocation:location];
            update.date = [NSDate date];
            update.seen = NO;
            
            [update saveInBackgroundWithBlock:block];
            
        } else {
            block(succeeded, error);
        }
        
    }];
    
}

- (void) checkNotReadMessageWithBlock:(void(^)(NSArray *messages, NSError *error)) block {
    
    PFQuery *query = [Message query];

    [query includeKey:@"order"];
    [query includeKey:@"order.products"];
    [query includeKey:@"order.restaurant"];
    [query includeKey:@"order.courier"];
    [query includeKey:@"order.client"];
    [query includeKey:@"order.client.client"];
    [query includeKey:@"order.client.client.courierData"];
    [query includeKey:@"order.client.client.privateData"];
    [query includeKey:@"order.commission"];
    [query includeKey:@"order.courier.courier"];
    [query includeKey:@"order.courier.courier.clientData"];
    [query includeKey:@"order.clientData"];
    
    [query whereKey:@"receiver" equalTo:[[Session sharedInstance] currentUser]];
    [query whereKey:@"seen" equalTo:@NO];
    
    [query findObjectsInBackgroundWithBlock:block];
}


#pragma mark - problem

- (void) createOrderProduct:(Order*)order withDescription:(NSString*)description withBlock:(void(^)(BOOL succeeded, NSError *error))block {
    
    OrderProblem *orderProblem = [[OrderProblem alloc] init];
    
    orderProblem.order = order;
    
    orderProblem.user= [Session sharedInstance].currentUser;
    
    orderProblem.descriptionOfProblem = description;
    
    [orderProblem saveEventually:block];
    
}


- (void) seenMessage:(Message*)message {
    
    message.seen = YES;
    
    [message saveInBackground];
}


@end
