//
//  ServiceManager.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PFFacebookUtils.h>
#import <AFNetworking.h>

#import "Constant.h"
#import "ACL.h"
#import "Role.h"
#import "Cart.h"
#import "Session.h"
#import <Parse/Parse.h>

#import "Cancelation.h"
#import "ApplicationProblem.h"
#import "DeliveryProblem.h"
#import "MealProblem.h"
#import "Rating.h"
#import "Order.h"
#import "OrderClientData.h"
#import "Commission.h"
#import "OrderProduct.h"
#import "Message.h"
#import "Restaurant.h"
#import "CategoryOfProduct.h"
#import "Product.h"
#import "Schedule.h"
#import "Courier.h"
#import "CourierClientData.h"
#import "CourierPrivateData.h"
#import "Client.h"
#import "ClientCourierData.h"
#import "ClientPrivateData.h"
#import "ClientSession.h"
#import "OrderStateUpdate.h"
#import "OrderProblem.h"
#import "RatingRestaurant.h"


@interface ServiceManager : NSObject


+ (instancetype) sharedInstance;

#pragma mark - Login and Logout

- (void)loginLoadDataWithUser:(PFUser *)pfUser block:(void (^)(BOOL, NSError *))block;
- (void) loginWithEmail:(NSString *)email password:(NSString *)password block:(void(^)(BOOL succeeded, NSError *error)) block;
- (void) loginFacebookWithBlock:(void(^)(BOOL succeeded, NSError *error)) block;
- (void) logout;

#pragma mark - Save objects

#pragma mark save user

- (void) saveUser:(User*)user;

- (void) saveUser:(User*)user withBlock:(void(^)(BOOL succeeded, NSError *error)) block;

#pragma mark save client

- (void) saveClient:(Client*)client;

- (void) saveClient:(Client*)client withBlock:(void(^)(BOOL succeeded, NSError *error)) block;

- (void) sendMessageWithOrder:(Order*)order andMensage:(NSString*)message withBlock:(void(^)(Message *message, NSError *error)) block;

- (void) getMessagesWithOrder:(Order*)order block:(void(^)(NSArray *messages, NSError *error)) block;

- (void) checkNotReadMessageWithBlock:(void(^)(NSArray *messages, NSError *error)) block;

- (void) seenMessage:(Message*)message;

- (void) updateOrder:(Order *)order state:(OrderState)state andLocation:(CLLocation*)location withBlock:(void(^)(BOOL succeeded, NSError *error))block;

#pragma mark - rating

- (void) getRatingWithReviewer:(User *)reviewer rated:(User *)rated andOrder:(Order*)order block:(void (^)(Rating *rating, NSError *error))block;

- (void) setRatingWithReviewer:(User *)reviewer rated:(User *)rated order:(Order*)order andRating:(NSUInteger)ratingValue block:(void (^)(Rating *rating, NSError *error))block;

- (void) setRatingWithReviewer:(User *)reviewer restaurant:(Restaurant *)restaurant order:(Order*)order andRating:(NSUInteger)ratingValue block:(void (^)(RatingRestaurant *rating, NSError *error))block;

- (void) getRatingWithReviewer:(User *)reviewer restaurant:(Restaurant *)restaurant andOrder:(Order*)order block:(void (^)(RatingRestaurant *rating, NSError *error))block;

#pragma mark - problem 

- (void) createOrderProduct:(Order*)order withDescription:(NSString*)description withBlock:(void(^)(BOOL succeeded, NSError *error))block;


@end
