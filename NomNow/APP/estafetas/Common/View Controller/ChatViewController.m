//
//  ChatViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 21/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (nonatomic, strong) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (nonatomic, strong) JSQMessagesAvatarImage *outgoingAvatarImageData;
@property (nonatomic, strong) JSQMessagesAvatarImage *incomingAvatarImageData;
@property (nonatomic, strong) NSString *phoneNumber;

@end

@implementation ChatViewController

- (void) viewDidLayoutSubviews {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect frame = self.view.frame;
    
    if([self.senderId isEqualToString:self.order.courier.objectId]) {
        
        if(frame.size.height+self.navigationController.tabBarController.tabBar.frame.size.height+self.tabBarController.tabBar.frame.size.height <= screenHeight) {
            
            frame.size.height +=self.tabBarController.tabBar.frame.size.height;
            self.view.frame = frame;
            
        }
    }

}

- (void)chatSetup {
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    
    
    self.outgoingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"User"]
                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    self.incomingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"User"]
                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    if([self.senderId isEqualToString:self.order.client.objectId]) {
        
        self.senderDisplayName = [NSString stringWithFormat:@"%@ %@", [[Session sharedInstance] currentUser].client.firstName, [[Session sharedInstance] currentUser].client.lastName];
        
        if(self.order.client.client.photo != nil) {
            
            [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.outgoingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:data]
                                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                }
            }];
        }
        
        if(self.order.courier.courier.photo != nil) {
            
            [self.order.courier.courier.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.incomingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:data]
                                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    
                }
            }];
        }
        
    } else {
        
        self.senderDisplayName = [NSString stringWithFormat:@"%@ %@", [[Session sharedInstance] currentUser].courier.firstName, [[Session sharedInstance] currentUser].courier.lastName];
        
        if(self.order.client.client.photo != nil) {
            
            [self.order.client.client.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.incomingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:data]
                                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    
                }
            }];
        }
        
        if(self.order.courier.courier.photo != nil) {
            
            [self.order.courier.courier.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                
                if(!error) {
                    
                    self.outgoingAvatarImageData = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:data]
                                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    
                }
            }];
        }
        
        
        
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.messages = [[NSMutableArray alloc]init];
    
    self.navigationItem.title = @"Mensagens";
    
    self.senderId = [[Session sharedInstance] currentUser].objectId;
    
    
    [self chatSetup];
    
    [self loadMessages];
    
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    
    UIButton *button = self.inputToolbar.contentView.rightBarButtonItem;

    [button setTitleColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    UIBarButtonItem *phoneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"phoneIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(makeCall)];
    
    
    if([self.senderId isEqualToString:self.order.client.objectId]) {
        
        if(!self.order.courier.courier.clientData.phoneNumber) {
            
            phoneButton.enabled = NO;
        } else {
            
            self.phoneNumber = self.order.courier.courier.clientData.phoneNumber;
        }
    } else {
        
        phoneButton.enabled = NO;
        
        [[CourierServiceManager sharedInstance] fetchIfNeedCourierData:self.order block:^(Order *order, NSError *error) {
           
            
            if(!error) {
                
                self.order = order;
                
                if(!self.order.client.client.courierData.phoneNumber) {
                    
                    phoneButton.enabled = NO;
                } else {
                    
                    phoneButton.enabled = YES;
                    self.phoneNumber = [NSString stringWithFormat:@"%@",self.order.client.client.courierData.phoneNumber];
                }
                
            }
            
        }];
    }
    
    
    self.navigationItem.rightBarButtonItem = phoneButton;

    
}


- (void) setOrder:(Order *)order {
    
    _order = order;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMessages) name:[NSString stringWithFormat:@"Message%@", order.objectId] object:nil];
    
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
      [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    
    [[ServiceManager sharedInstance] sendMessageWithOrder:self.order andMensage:text withBlock:^(Message *message, NSError *error) {
        
        if(!error) {
            
            JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                                     senderDisplayName:senderDisplayName
                                                                  date:date
                                                                  text:text];
            
            [self.messages addObject:message];
            [self finishSendingMessageAnimated:YES];
            
        }
        
    }];
}

- (void) loadMessages {
    
    [[ServiceManager sharedInstance] getMessagesWithOrder:self.order block:^(NSArray *messages, NSError *error) {
        
        
        if(!error) {
            
            self.messages = [[NSMutableArray alloc] init];
            
            for(int i=0;i<messages.count; i++)
            {
                
                
                Message *m = messages[i];
                
                
                JSQMessage *message = nil;
                
                if([m.sender.objectId isEqualToString:self.order.client.objectId]) {
                    
                    message = [[JSQMessage alloc] initWithSenderId:self.order.client.objectId
                                                 senderDisplayName:[NSString stringWithFormat:@"%@ %@", self.order.client.client.firstName, self.order.client.client.lastName]
                                                              date:m.date
                                                              text:m.text];
                } else {
                    
                    message = [[JSQMessage alloc] initWithSenderId:self.order.courier.objectId
                                                 senderDisplayName:[NSString stringWithFormat:@"%@ %@", self.order.courier.courier.firstName, self.order.courier.courier.lastName]
                                                              date:m.date
                                                              text:m.text];
                    
                }
                
                [self.messages addObject:message];

            }
            
            
            [self.collectionView reloadData];
            
            [self scrollToBottomAnimated:YES];
        }
        
    }];
}


#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
       
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageData;
    }
    
    return self.incomingBubbleImageData;

}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingAvatarImageData;
    }
    else {
        return self.incomingAvatarImageData;
    }
    
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
       if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
  
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
    
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.messages.count;//[self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}



#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:@"Custom Action"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *currentMessage = [self.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}



- (void) makeCall {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.phoneNumber]]];
    
}

@end
