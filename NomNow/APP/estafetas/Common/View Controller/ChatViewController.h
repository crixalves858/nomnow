//
//  ChatViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 21/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "JSQMessagesViewController.h"
#import <JSQMessages.h>
#import "Session.h"
#import "ServiceManager.h"
#import "CourierServiceManager.h"
#import "Message.h"

@interface ChatViewController : JSQMessagesViewController

@property (nonatomic, strong) Order *order;

@end
