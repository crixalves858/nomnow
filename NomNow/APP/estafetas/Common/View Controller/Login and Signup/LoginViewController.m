//
//  LoginViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "LoginViewController.h"
#import "ClientTabBarController.h"
#import "ConfirmOrderViewController.h"
#import "PayViewController.h"



#import "WaitClientConfirmOrderChangesViewController.h"

#import "CourierTabBarController.h"
#import "ServiceManager.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopConstraint;

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (strong, nonatomic) UIView *editView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self textFieldDelegateSetup];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self addKeyBoardObserver];
    
    [self autoLogin];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    // remove all observers from notification center to be sure we got no memory leaks
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void) textFieldDelegateSetup {
    
    self.emailField.delegate = self;
    self.passwordField.delegate = self;
}


#pragma mark - Actions


- (void)loginAction {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    
    [[[ServiceManager alloc] init] loginWithEmail:email password:password block:^(BOOL succeeded, NSError *error) {
        
        [SVProgressHUD dismiss];
        if(!error) {
            
            [self loginWithoutError];
        } else {
            
            [self alertWithError:error];
            //[self loginWithError];
        }
       
        
        
    }];
}

- (IBAction)loginButtonAction:(id)sender {
    
    [self loginAction];
}

- (IBAction)loginFacebookButtonAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ServiceManager sharedInstance] loginFacebookWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error) {
            
            [self loginWithoutError];
            
        } else {
            
            [self alertWithError:error];
        }
        
        [SVProgressHUD dismiss];
        
    }];
    
}

- (IBAction)noLoginButtonAction:(id)sender {
    
    [self openClientApplication];
    
}


- (IBAction)signupButtonAction:(id)sender {
    
    SignupViewController *signupViewController = [[SignupViewController alloc] initWithNibName:@"SignupViewController" bundle:nil];
    
    [self.navigationController pushViewController:signupViewController animated:YES];
}


#pragma mark - login functions

- (void)autoLogin {
    
    if ([Session sharedInstance].currentUser) {
        
        [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
        
        [[ServiceManager sharedInstance]loginLoadDataWithUser:[Session sharedInstance].currentUser block:^(BOOL s, NSError *error) {
            
            if(!error ) {
                
                [self loginWithoutError];
            } else {
                
                [self alertWithError:error];
            }
        }];
    }
}

- (void) loginWithoutError {
    
    if (!self.modal) {
        
        if([Session sharedInstance].userType == UserTypeClient) {
            
            [self openClientApplication];
            
        } else if([Session sharedInstance].userType == UserTypeCourier) {
            
            [self openCourierApplication];
        }
        
    } else {
        
        if([Session sharedInstance].userType == UserTypeCourier) {
            
            [self loginWithError];
            
        } else {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}

- (void) loginWithError {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login incorreto"
                                                    message:@"Verifique as credenciais"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Open Aplications

- (void) openClientApplication {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ClientTabBarController *clientTabBarController = [[ClientTabBarController alloc] init];
    
    [SVProgressHUD dismiss];
    
    [appDelegate changeRootViewController:clientTabBarController animated:NO];
}

- (void) openCourierApplication {
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[CourierServiceManager sharedInstance] myActiveOrdersWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error) {
            
            if(objects.count > 0)
            {
                Order *order = [objects firstObject];
                
                                
                [SVProgressHUD dismiss];
                
                CourierTabBarController *courierTabBarController = [[CourierTabBarController alloc] initWithOrder:order];
                
                appDelegate.window.rootViewController = courierTabBarController;
                
            } else {
                
                [SVProgressHUD dismiss];
                
                CourierTabBarController *courierTabBarController = [[CourierTabBarController alloc] init];
    
                appDelegate.window.rootViewController = courierTabBarController;
                
            }
            
            
        } else {
            
            [self alertWithError:error];
            
            
        }
    }];
    
    
}

#pragma mark - textFiel delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.editView = textField;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.editView = nil;
    
    [textField endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if(textField==self.emailField) {
        
        [self.passwordField becomeFirstResponder];
    } else {
        
        [self loginAction];
    }
    
    return YES;
}

#pragma mark - Keyboard

- (IBAction)tapView:(id)sender {
    
    [self.editView endEditing:YES];
}


- (void) addKeyBoardObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note {
    
    CGRect editViewFrame = self.editView.frame;
    CGRect viewLogin = self.editView.superview.frame;
    CGRect viewFrame = self.view.frame;
    
    NSDictionary* keyboardInfo = [note userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    

    CGFloat finishText = editViewFrame.size.height + editViewFrame.origin.y + viewFrame.origin.y + viewLogin.origin.y;
    
    NSTimeInterval duration = [[note userInfo][UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    if((viewFrame.size.height - keyboardFrameBeginRect.size.height) < finishText) {
        
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            self.contentViewButtonConstraint.constant = finishText - (viewFrame.size.height - keyboardFrameBeginRect.size.height);
            self.contentViewTopConstraint.constant = - (finishText - (viewFrame.size.height - keyboardFrameBeginRect.size.height));
            
            
        } completion:nil];


    }
    [self.view layoutIfNeeded];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSTimeInterval duration = [[notification userInfo][UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.contentViewButtonConstraint.constant =0;
        self.contentViewTopConstraint.constant = 0;

        
    } completion:^(BOOL finished) {
        
        
    }];
    
}




@end
