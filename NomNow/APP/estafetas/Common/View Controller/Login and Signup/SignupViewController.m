//
//  SignupViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 14/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *password1Field;
@property (weak, nonatomic) IBOutlet UITextField *password2Field;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberField;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Registo";
    
    UIBarButtonItem *registerTabButton=[[UIBarButtonItem alloc] initWithTitle:@"Registar" style:UIBarButtonItemStyleDone target:self action:@selector(registerAction)];
   
    self.navigationItem.rightBarButtonItem=registerTabButton;
    
    
}

- (void) registerAction{
    
    if([self.firstNameField.text isEqualToString:@""] || [self.lastNameField.text isEqualToString:@""] || [self.emailField.text isEqualToString:@""] || [self.password1Field.text isEqualToString:@""] || [self.password2Field.text isEqualToString:@""]) {
        
        [self alertWithMessage:@"Preencha todos os campos"];
        
        return;
    }
    
    if(![self.password1Field.text isEqualToString:self.password2Field.text]){
        
        [self alertWithMessage:@"As password são diferentes"];
        
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    [[ClientServiceManager sharedInstance] signUpWithFirstName:self.firstNameField.text
                                                      lastName:self.lastNameField.text
                                                         email:self.emailField.text
                                                      password:self.password1Field.text
                                                   phoneNumber:self.phoneNumberField.text
                                                        andImage:self.imageView.image
                                                         block:^(BOOL succeeded, NSError *error) {
                                                             
                                                             if(!error) {
                                                                                                                                
                                                                 AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                 
                                                                 ClientTabBarController *clientTabBarController = [[ClientTabBarController alloc] init];
                                                                 
                                                                 
                                                                 [SVProgressHUD dismiss];
                                                                 
                                                                 [appDelegate changeRootViewController:clientTabBarController animated:NO];
                                                                 
                                                             } else {
                                                                 
                                                                 [self alertWithError:error];
                                                             }
                                                             
                                                             
                                                             
                                                         }];
}


- (void)imagePicker {
    [SVProgressHUD showWithStatus:@"Aguarde" maskType:SVProgressHUDMaskTypeBlack];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        
        [SVProgressHUD dismiss];
        
    }];
}

- (IBAction)findPhotoAction:(id)sender {
    
    [self imagePicker];
}

#pragma mark - imagepicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
   self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    if(textField==self.firstNameField) {
        
        [self.lastNameField becomeFirstResponder];
    }
    
    if(textField==self.lastNameField) {
        
        [self.emailField becomeFirstResponder];
    }
    
    if(textField==self.emailField) {
        
        [self.phoneNumberField becomeFirstResponder];
    }
    
    if(textField==self.phoneNumberField) {
        
        [self.password1Field becomeFirstResponder];
    }
    
    if(textField==self.password1Field) {
        
        [self.password2Field becomeFirstResponder];
    }
    
    if(textField==self.password2Field) {
    
        [self.password2Field endEditing:YES];
        [self registerAction];
    }
    
    return YES;
}
- (IBAction)tapView:(id)sender {
    
    [self.view endEditing:YES];
    
}

@end
