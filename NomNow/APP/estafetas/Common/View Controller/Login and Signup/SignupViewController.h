//
//  SignupViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 14/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "BaseViewController.h"
#import "ClientServiceManager.h"
#import "ClientTabBarController.h"
#import "AppDelegate.h"

@interface SignupViewController : BaseViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
