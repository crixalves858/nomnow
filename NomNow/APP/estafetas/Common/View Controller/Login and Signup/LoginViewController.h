//
//  LoginViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <SVProgressHUD.h>

#import "BaseViewController.h"
#import "SignupViewController.h"


@interface LoginViewController : BaseViewController <UITextFieldDelegate>

@property (nonatomic) BOOL modal;

@end
