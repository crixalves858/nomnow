//
//  BaseViewController.m
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "BaseViewController.h"
#import <Parse/PFConstants.h>

#import "UpdateService.h"
#import "LoginNavigationController.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void) alertWithError:(NSError*)error {
    
    [SVProgressHUD dismiss];
    
    NSString *errorMessage = @"Ocurreu um erro contacte o administrador";
    
    NSLog(@"%@", error.description);
    if ([error code] == kPFErrorConnectionFailed) {
      
        errorMessage =@"A ligação à Internet parece não estar a funcionar.";
    } else if([error code] == kPFErrorObjectNotFound && [error.userInfo[@"error"] isEqual:@"invalid login parameters"]) {
        
        errorMessage = @"Dados do login incorrectos";
    } else if([[error domain] isEqualToString:kCLErrorDomain]) {
        
        errorMessage = @"Não encontramos resultados para a sua pesquisa";
    }
  
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro	"
                                                    message:errorMessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) alertWithMessage:(NSString*)message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro	"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) alertWithMessage:(NSString*)message andTitle:(NSString *)title {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
       
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.9686 green:0.6745 blue:0.35686 alpha:1]];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}



- (void)navigationBarSetupWithAddAction:(SEL)doneAction andTitle:(NSString*)title {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissAction)];
    UIBarButtonItem *doneTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:doneAction];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=doneTabButton;
    self.navigationItem.title=title;
}


- (void)navigationBarSetupWithEditAction:(SEL)doneAction andTitle:(NSString*)title {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissAction)];
    UIBarButtonItem *doneTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:doneAction];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=doneTabButton;
    self.navigationItem.title=title;
}

- (void)navigationBarSetupWithDoneAction:(SEL)doneAction andTitle:(NSString*)title {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissAction)];
    UIBarButtonItem *doneTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:doneAction];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=doneTabButton;
    self.navigationItem.title=title;
}

- (void)navigationBarSetupWithDoneAction:(SEL)doneAction cancelAction:(SEL)canceleAction andTitle:(NSString*)title {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:canceleAction];
    UIBarButtonItem *doneTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:doneAction];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=doneTabButton;
    self.navigationItem.title=title;
}

- (void)navigationBarSetupWithoutCanceleAndWithDoneAction:(SEL)doneAction andTitle:(NSString *)title {
    
    UIBarButtonItem *confirmCheckoutTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:doneAction];
    
    self.navigationItem.rightBarButtonItem=confirmCheckoutTabButton;
    self.navigationItem.title=title;
}

- (void)navigationBarSetupWithCanceleButtonAndTitle:(NSString*)title {
    
    UIBarButtonItem *dismissTabButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissAction)];
    self.navigationItem.leftBarButtonItem=dismissTabButton;
    self.navigationItem.rightBarButtonItem=nil;
    self.navigationItem.title=title;
}

- (void) dismissAction {
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
