//
//  BaseViewController.h
//  estafetas
//
//  Created by Cristiano Alves on 25/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD.h>


@interface BaseViewController : UIViewController

- (void) alertWithError:(NSError*)error;
- (void) alertWithMessage:(NSString*)message;
- (void) alertWithMessage:(NSString*)message andTitle:(NSString *)title;

- (void)navigationBarSetupWithEditAction:(SEL)doneAction andTitle:(NSString*)title;
- (void)navigationBarSetupWithAddAction:(SEL)doneAction andTitle:(NSString*)title;
- (void)navigationBarSetupWithDoneAction:(SEL)doneAction andTitle:(NSString*)title;
- (void)navigationBarSetupWithDoneAction:(SEL)doneAction cancelAction:(SEL)canceleAction andTitle:(NSString*)title;
- (void)navigationBarSetupWithoutCanceleAndWithDoneAction:(SEL)doneAction andTitle:(NSString *)title;
- (void)navigationBarSetupWithCanceleButtonAndTitle:(NSString*)title;

@end
