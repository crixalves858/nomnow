//
//  LoginNavigationController.m
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "LoginNavigationController.h"

@interface LoginNavigationController ()

@end

@implementation LoginNavigationController


- (id) init{
    
    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];

    loginViewController.modal = NO;
    
    self = [super initWithRootViewController:loginViewController];
    
    self.navigationBar.translucent = NO;
    
    return self;
}

- (id) initToCheckout {
    
    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginToCheckoutViewController" bundle:nil];
    
    loginViewController.modal = YES;
    
    self = [super initWithRootViewController:loginViewController];
    
    self.navigationBar.translucent = NO;
    
    return self;
    
}

@end
