//
//  LoginNavigationController.h
//  estafetas
//
//  Created by Cristiano Alves on 24/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface LoginNavigationController : UINavigationController

- (id) initToCheckout;

@end
