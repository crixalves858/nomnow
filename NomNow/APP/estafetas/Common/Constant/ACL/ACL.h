//
//  ACL.h
//  estafetas
//
//  Created by Cristiano Alves on 03/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Role.h"
#import "User.h"

@interface ACL : PFACL

+(PFACL*) privateACL;
+(PFACL*) publicACL;
+(PFACL*) publicReadACL;

+(void)clientACLBackgroundWithUser:(User *)user block:(void(^)(BOOL succeeded, NSError *error)) block;
@end
