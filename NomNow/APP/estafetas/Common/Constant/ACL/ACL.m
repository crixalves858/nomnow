//
//  ACL.m
//  estafetas
//
//  Created by Cristiano Alves on 03/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ACL.h"

@implementation ACL

+(PFACL*) privateACL {
    
    PFACL *acl = [PFACL ACL];
    
    [acl setPublicReadAccess:NO];
    [acl setPublicWriteAccess:NO];
    
    return acl;
}

+(PFACL*) publicACL {
    
    PFACL *acl = [PFACL ACL];
    
    [acl setPublicReadAccess:YES];
    [acl setPublicWriteAccess:YES];
    
    return acl;
}

+(PFACL*) publicReadACL {
    
    PFACL *acl = [PFACL ACL];
    
    [acl setPublicReadAccess:YES];
    [acl setPublicWriteAccess:NO];
    
    return acl;
}

+(void)clientACLBackgroundWithUser:(User *)user block:(void(^)(BOOL succeeded, NSError *error)) block {
    
    PFRole *roleCourier = [Role clientProfileCourierRoleWithUser:user];
    //PFRole *adminRole = [Role adminRole];
    
    [PFObject saveAllInBackground:@[roleCourier] block:^(BOOL succeeded, NSError *error) {
        
        PFACL *aclClient = [ACL privateACL];
        
        [aclClient setWriteAccess:NO forRole:roleCourier];
        [aclClient setReadAccess:YES forRole:roleCourier];
        
        [aclClient setReadAccess:YES forUser:user];
        [aclClient setWriteAccess:YES forUser:user];
        
        //[aclClient setReadAccess:YES forRole:adminRole];
        //[aclClient setWriteAccess:YES forRole:adminRole];
        
        PFACL *aclPrivate = [ACL privateACL];
        
        //[aclPrivate setReadAccess:YES forRole:adminRole];
        //[aclPrivate setWriteAccess:YES forRole:adminRole];
        
        [aclPrivate setReadAccess:YES forUser:user];
        [aclPrivate setWriteAccess:YES forUser:user];
        
        user.ACL = aclClient;
        user.client.ACL = aclClient;
        user.client.privateData.ACL = aclPrivate;
        user.client.courierData.ACL = aclPrivate;
        block(succeeded, error);
        
    }];
    
}


@end
