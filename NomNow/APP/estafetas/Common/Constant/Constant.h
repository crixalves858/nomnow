//
//  Constant.h
//  estafetas
//
//  Created by Cristiano Alves on 03/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#ifndef estafetas_Constant_h
#define estafetas_Constant_h


static NSString *const ROLE_CLIENT_PROFILE_COURIER = @"courier";
static NSString *const ROLE_CLIENT_PROFILE_COURIER_ORDER = @"courierOrder";
static NSString *const ROLE_ADMIN = @"admin";
static int const TIMER_STEP = 4;
static int const TIME_TO_RESPONSE = 20;
static double const PRICE_BY_METER = 0.0001;
static double const TIME_WAIT_CLIENT = 5*60;
static int MAX_DISTANCE_RESTAURANT = 5000;

typedef NS_ENUM(NSInteger, OrderState) {
    
    OrderStateWatingCourier = 0,
    OrderStateHaveCourier =1,
    OrderStateUpdateWaitingClientAccept = 2,
    OrderStateClientAccept = 3,
    OrderStateGoToRestaurant = 4,
    OrderStateInRestaurant = 5,
    OrderStateUpdateWaitingClientAcceptInRestaurant = 6,
    OrderStateClientAcceptInRestaurant = 7,
    OrderStatePay = 8,
    OrderStateGoToClient = 9,
    OrderStateArrived= 10,
    OrderStateArrivedWaitClient = 11,
    OrderStateOrderDelivery = 12,
    OrderStateDeliveryFail = 13,
    OrderStateDeliveryFailAnalize = 14,
    OrderStateDeliveryFailClient = 15,
    OrderStateDeliveryFailCourier = 16,
    OrderStateFailNotHaveCourier = 17,
    OrderStateFailClientNotAccept = 18,
    OrderStateInit = 19,
    OrderStateCanceleByClient = 20,
    OrderStateCanceleRestaurantClose = 21,
    OrderStateCanceleByClientWithCourier = 22
};

typedef NS_ENUM(NSInteger, CourierState) {
    CourierStateOff = 1,
    CourierStateWaiting = 2,
    CourierStateinWork = 3
};

typedef NS_ENUM(NSInteger, UserType) {
    
    UserTypeUndefined = 0,
    UserTypeClient = 1,
    UserTypeCourier = 2,
    UserTypeMulti = 3
    
};

typedef NS_ENUM(NSInteger, EligibleCourierState) {
    
    EligibleCourierStateAccept = 1,
    EligibleCourierStateRefuse = 2,
    EligibleCourierStateWaitResponse = 3,
    EligibleCourierStateNotContact = 4,
    EligibleCourierStateLastTry = 5,
    EligibleCourierStateNotResponse = 6
    
};

typedef NS_ENUM(NSInteger, FindCourierState) {
    
    FindCourierStateHaveCourier = 1,
    FindCourierStateWaiting = 2,
    FindCourierStateNotHaveCourie = 3
    
};

typedef NS_ENUM(NSInteger, NearRestaurantState) {
    
    NearRestaurantStateAvaliable = 1,
    NearRestaurantStateWithoutCouriers = 2,
    NearRestaurantStateClose = 3,
    
};

#endif
