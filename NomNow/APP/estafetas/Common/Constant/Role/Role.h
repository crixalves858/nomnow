//
//  Role.h
//  estafetas
//
//  Created by Cristiano Alves on 03/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "User.h"
#import "Constant.h"
#import "ACL.h"

@interface Role : PFRole

#pragma mark - client
+ (PFRole*) clientProfileCourierRoleWithUser:(User *)user;

#pragma mark - admin
+ (PFRole*) adminRole;

#pragma mark - tools
+ (void) saveRoles:(NSArray *)roles withBlock:(void(^)(BOOL succeeded)) block;


@end
