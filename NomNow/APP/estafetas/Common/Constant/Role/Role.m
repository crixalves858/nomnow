//
//  Role.m
//  estafetas
//
//  Created by Cristiano Alves on 03/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Role.h"

@implementation Role


#pragma mark - Client

+ (PFRole*) clientProfileCourierRoleWithUser:(User *)user {
    
    NSString *roleName = [NSString stringWithFormat:@"%@%@",ROLE_CLIENT_PROFILE_COURIER, user.objectId];
    
    PFRole *role = [PFRole roleWithName:roleName];
    
    PFACL *acl = [ACL publicACL];
    
    role.ACL = acl;
    
    return role;
}

#pragma mark - admin

+ (PFRole*) adminRole {
    
    PFRole *role = [PFRole roleWithName:ROLE_ADMIN];
    
    PFACL *acl = [ACL publicReadACL];
    
    role.ACL = acl;
    
    return role;
}

#pragma tools

+ (void) saveRoles:(NSArray *)roles withBlock:(void(^)(BOOL succeeded)) block {
    
    [PFObject saveAllInBackground:roles block:^(BOOL succeeded, NSError *error) {
        
        block(succeeded);
        
    }];
    
}


@end
