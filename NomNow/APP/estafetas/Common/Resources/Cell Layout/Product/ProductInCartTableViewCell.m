//
//  ProductInCartTableViewCell.m
//  estafetas
//
//  Created by Cristiano Alves on 13/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ProductInCartTableViewCell.h"

@implementation ProductInCartTableViewCell

@synthesize productNameLabel = _productNameLabel;
@synthesize productPriceLabel = _productPriceLabel;
@synthesize productQuantityLabel = _productQuantityLabel;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
