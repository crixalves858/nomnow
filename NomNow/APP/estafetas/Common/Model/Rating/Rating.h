//
//  Rating.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "User.h"
#import "Order.h"

@interface Rating : PFObject<PFSubclassing>

@property (nonatomic, strong) User *reviewer;
@property (nonatomic, strong) User *rated;
@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) Order *order;


@end
