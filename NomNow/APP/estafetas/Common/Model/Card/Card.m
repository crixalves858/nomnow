//
//  Card.m
//  estafetas
//
//  Created by Cristiano Alves on 28/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Card.h"

@implementation Card

@dynamic brand;
@dynamic expirationMonth;
@dynamic last4Digits;
@dynamic name;
@dynamic expirationYear;
@dynamic country;


+ (NSString *) parseClassName {
    
    return @"Card";
}


@end
