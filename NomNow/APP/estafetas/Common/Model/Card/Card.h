//
//  Card.h
//  estafetas
//
//  Created by Cristiano Alves on 28/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>

@interface Card : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSNumber *expirationMonth;
@property (nonatomic, strong) NSString *last4Digits;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *expirationYear;
@property (nonatomic, strong) NSString *country;

@end
