//
//  Product.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Product.h"

@implementation Product

@dynamic name;
@dynamic price;
@dynamic restaurant;
@dynamic category;

+(NSString *) parseClassName {
    
    return @"Product";
}



@end
