//
//  Product.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Restaurant.h"
#import "CategoryOfProduct.h"

@interface Product : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) CategoryOfProduct *category;


@end
