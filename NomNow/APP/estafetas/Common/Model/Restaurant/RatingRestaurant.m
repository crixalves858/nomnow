//
//  RatingRestaurant.m
//  estafetas
//
//  Created by Cristiano Alves on 12/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "RatingRestaurant.h"

@implementation RatingRestaurant

@dynamic reviewer;
@dynamic value;
@dynamic order;
@dynamic restaurant;


+(NSString*) parseClassName {
    
    return @"RatingRestaurant";
}


@end
