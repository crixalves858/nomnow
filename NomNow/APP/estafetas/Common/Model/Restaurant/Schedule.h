//
//  Schedule.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>

@interface Schedule : PFObject<PFSubclassing>

@property (nonatomic, strong) NSDate *startHour;
@property (nonatomic, strong) NSDate *finishHour;

@end
