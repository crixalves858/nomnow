//
//  Category.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>

@interface CategoryOfProduct : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *name;

@end
