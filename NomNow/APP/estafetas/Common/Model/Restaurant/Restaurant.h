//
//  Restaurant.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Constant.h"

@interface Restaurant : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) PFFile *photo;
@property (nonatomic, strong) PFGeoPoint *location;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSMutableArray *schedule; //of shedules
@property (nonatomic, strong) NSMutableArray *categories; //of category
@property (nonatomic, strong) NSNumber *waitTime;
@property (nonatomic) NearRestaurantState state;


@end
