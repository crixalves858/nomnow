//
//  Category.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CategoryOfProduct.h"

@implementation CategoryOfProduct

@dynamic name;

+(NSString *) parseClassName{
    
    return @"Category";
}

@end
