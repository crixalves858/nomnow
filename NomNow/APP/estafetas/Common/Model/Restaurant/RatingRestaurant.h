//
//  RatingRestaurant.h
//  estafetas
//
//  Created by Cristiano Alves on 12/05/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "User.h"
#import "Order.h"

@interface RatingRestaurant : PFObject<PFSubclassing>

@property (nonatomic, strong) User *reviewer;
@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) Restaurant *restaurant;

@end
