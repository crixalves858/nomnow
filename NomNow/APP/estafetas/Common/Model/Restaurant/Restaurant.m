//
//  Restaurant.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Restaurant.h"

@implementation Restaurant

@synthesize state = _state;
@dynamic name;
@dynamic photo;
@dynamic location;
@dynamic address;
@dynamic rating;
@dynamic phoneNumber;
@dynamic schedule;
@dynamic categories;
@dynamic waitTime;

#pragma mark - Parse

+ (NSString *) parseClassName {
    
    return @"Restaurant";
}

- (BOOL) isEqual:(id)object{
    
    Restaurant *restaurant = object;
    
    return [self.objectId isEqualToString:restaurant.objectId];
    
}

@end
