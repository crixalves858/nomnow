//
//  Schedule.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Schedule.h"

@implementation Schedule

@dynamic startHour;
@dynamic finishHour;

+(NSString*) parseClassName {
    
    return @"Schedule";
}

@end
