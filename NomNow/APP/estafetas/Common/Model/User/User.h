//
//  User.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Client.h"
#import "Courier.h"

@interface User : PFUser<PFSubclassing>

@property (nonatomic, strong) Client *client;
@property (nonatomic, strong) Courier *courier;

@end
