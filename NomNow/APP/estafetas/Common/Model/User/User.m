//
//  User.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic client;
@dynamic courier;

+ (void)load {
    [self registerSubclass];
}

+ (NSString*) parseClassName {
    
    return @"_User";
}





@end
