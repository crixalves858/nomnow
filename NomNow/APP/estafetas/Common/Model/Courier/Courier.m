//
//  Courier.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Courier.h"

@implementation Courier

@dynamic firstName;
@dynamic lastName;
@dynamic photo;
@dynamic clientData;
@dynamic privateData;
@dynamic classification;
@dynamic velocity;


+ (NSString*)parseClassName {
    
    return @"Courier";
}

@end
