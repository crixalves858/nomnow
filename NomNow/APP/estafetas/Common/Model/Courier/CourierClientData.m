//
//  CourierClientData.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CourierClientData.h"

@implementation CourierClientData

@dynamic location;
@dynamic phoneNumber;

+(NSString*) parseClassName {
    
    return @"CourierClientData";
}

@end
