//
//  CourierClientData.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>

@interface CourierClientData : PFObject<PFSubclassing>

@property (nonatomic, strong) PFGeoPoint *location;
@property (nonatomic, strong) NSString *phoneNumber;


@end
