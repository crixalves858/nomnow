//
//  CourierPrivateData.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Constant.h"

@interface CourierPrivateData : PFObject<PFSubclassing>


@property (nonatomic) CourierState state;

@end
