//
//  Courier.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "CourierClientData.h"
#import "CourierPrivateData.h"

@interface Courier : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) PFFile *photo;
@property (nonatomic, strong) NSNumber *velocity;

@property (nonatomic, strong) CourierClientData *clientData;
@property (nonatomic, strong) CourierPrivateData *privateData;

@property (nonatomic, strong) NSNumber *classification;

@end
