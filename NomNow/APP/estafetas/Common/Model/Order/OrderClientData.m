//
//  OrderClientData.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderClientData.h"

@implementation OrderClientData

@dynamic validationPIN;

+(NSString *) parseClassName {
    
    return @"OrderClientData";
}

@end
