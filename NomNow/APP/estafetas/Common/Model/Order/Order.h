//
//  Order.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Commission.h"
#import "OrderClientData.h"
#import "Restaurant.h"
#import "Constant.h"
#import "User.h"

@interface Order : PFObject<PFSubclassing>

@property (nonatomic, strong) NSDate *orderDate;
@property (nonatomic, strong) NSDate *deliveryDate;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic)         OrderState state;
@property (nonatomic, strong) NSString *deliveryAddress;
@property (nonatomic, strong) PFFile *invoice;
@property (nonatomic, strong) OrderClientData *clientData;
@property (nonatomic, strong) Commission *commission;
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) NSMutableArray *products;
@property (nonatomic) BOOL aceptedByClient;
@property (nonatomic, strong) PFGeoPoint *deliveryLocation;
@property (nonatomic, strong) NSString *deliveryDoorNumber;
@property (nonatomic, strong) NSString *deliveryZipCode;
@property (nonatomic, strong) NSNumber *distanceRestaurantClient;
@property (nonatomic, strong) NSNumber *distanceCourierRestaurant;
@property (nonatomic, strong) User *client;
@property (nonatomic, strong) User *courier;
@property (nonatomic, strong) NSNumber *timeWaitRestaurant;
@property (nonatomic, strong) PFFile *courierPhoto;
@property (nonatomic) BOOL pay;
@property (nonatomic, strong) NSString *deliveryFloor;
@property (nonatomic, strong) NSString *deliveryObservations;

@end
