//
//  OrderStateUpdate.h
//  estafetas
//
//  Created by Cristiano Alves on 16/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Order.h"

@interface OrderStateUpdate : PFObject<PFSubclassing>

@property (nonatomic) NSInteger state;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) PFGeoPoint *location;
@property (nonatomic, strong) Order *order;
@property (nonatomic) BOOL seen;

@end
