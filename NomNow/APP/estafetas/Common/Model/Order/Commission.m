//
//  Commission.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Commission.h"

@implementation Commission

@dynamic state;
@dynamic value;

+(NSString *) parseClassName {
    
    return @"Commission";
}

@end
