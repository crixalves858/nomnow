//
//  EligibleCourier.m
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "EligibleCourier.h"

@implementation EligibleCourier

@dynamic state;
@dynamic value;
@dynamic lastContact;
@dynamic order;
@dynamic courier;
@dynamic history;
@dynamic distanceToRestaurant;


+(NSString *) parseClassName {
    
    return @"EligibleCourier";
}


@end
