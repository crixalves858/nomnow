//
//  EligibleCourier.h
//  estafetas
//
//  Created by Cristiano Alves on 11/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Order.h"
#import "User.h"
#import "Constant.h"

@interface EligibleCourier : PFObject<PFSubclassing>

@property (nonatomic)         EligibleCourierState state;
@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) NSDate *lastContact;
@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) User *courier;
@property (nonatomic)         BOOL history;
@property (nonatomic, strong) NSNumber *distanceToRestaurant;

@end
