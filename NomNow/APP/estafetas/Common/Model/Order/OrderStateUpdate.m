//
//  OrderStateUpdate.m
//  estafetas
//
//  Created by Cristiano Alves on 16/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderStateUpdate.h"

@implementation OrderStateUpdate

@dynamic state;
@dynamic date;
@dynamic location;
@dynamic order;
@dynamic seen;

+(NSString *) parseClassName {
    
    return @"OrderStateUpdate";
}


@end
