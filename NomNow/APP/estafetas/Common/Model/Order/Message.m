//
//  Message.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Message.h"

@implementation Message

@dynamic sender;
@dynamic receiver;
@dynamic date;
@dynamic text;
@dynamic order;
@dynamic seen;

+(NSString *) parseClassName {
    
    return @"Message";
}

@end
