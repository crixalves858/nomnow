//
//  OrderProduct.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderProduct.h"

@implementation OrderProduct

@dynamic product;
@dynamic realIVA;
@dynamic realPrice;
@dynamic observations;
@dynamic quantity;
@dynamic order;


- (id)initWithProduct:(Product *)product{
    
    OrderProduct *orderProduct = [[OrderProduct alloc] init];
    orderProduct.product = product;
    
    orderProduct.realPrice = product.price;
    return orderProduct;
}


+(NSString*) parseClassName {
    
    return @"OrderProduct";
}




@end
