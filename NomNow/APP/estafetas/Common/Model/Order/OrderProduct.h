//
//  OrderProduct.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Product.h"
#import "Order.h"

@interface OrderProduct : PFObject<PFSubclassing>

@property (nonatomic, strong) Product *product;
@property (nonatomic, strong) NSNumber *realPrice;
@property (nonatomic, strong) NSNumber *realIVA;
@property (nonatomic, strong) NSString *observations;
@property (nonatomic, strong) NSNumber *quantity;
@property (nonatomic, strong) Order *order;

- (id)initWithProduct:(Product *)product;
@end
