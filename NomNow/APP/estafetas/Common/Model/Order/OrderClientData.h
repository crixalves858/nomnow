//
//  OrderClientData.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface OrderClientData : PFObject<PFSubclassing>

@property (nonatomic) NSInteger validationPIN;

@end
