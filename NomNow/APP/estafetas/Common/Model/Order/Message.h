//
//  Message.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Order.h"
#import "User.h"

@interface Message : PFObject<PFSubclassing>

@property (nonatomic, strong) User *sender;
@property (nonatomic, strong) User *receiver;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) Order *order;
@property (nonatomic) BOOL seen;

@end
