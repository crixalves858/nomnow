//
//  Order.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Order.h"

@implementation Order

@dynamic orderDate;
@dynamic deliveryDate;
@dynamic distance;
@dynamic state;
@dynamic deliveryAddress;
@dynamic invoice;
@dynamic clientData;
@dynamic commission;
@dynamic client;
@dynamic courier;
@dynamic restaurant;
@dynamic products;
@dynamic aceptedByClient;
@dynamic deliveryLocation;
@dynamic deliveryZipCode;
@dynamic deliveryDoorNumber;
@dynamic distanceCourierRestaurant;
@dynamic distanceRestaurantClient;
@dynamic timeWaitRestaurant;
@dynamic courierPhoto;
@dynamic pay;
@dynamic deliveryFloor;
@dynamic deliveryObservations;

+(NSString *) parseClassName {
    
    return @"Order";
}

- (BOOL) isEqual:(id)object{
    
    Order *order = object;
    
    return [self.objectId isEqualToString:order.objectId];
    
}

@end
