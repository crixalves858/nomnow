//
//  Cancelation.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Order.h"
#import "User.h"

@interface Cancelation : PFObject<PFSubclassing>

@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) User *user;

@end
