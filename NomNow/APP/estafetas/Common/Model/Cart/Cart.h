//
//  Cart.h
//  estafetas
//
//  Created by Cristiano Alves on 04/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"
#import "OrderProduct.h"
#import "Restaurant.h"

@interface Cart : NSObject

@property (nonatomic, strong, readonly) NSMutableArray* products;
@property (nonatomic, strong) Restaurant *restaurant;

- (BOOL) addProduct:(Product *)product;
- (BOOL) addProduct:(Product *)product withQuantity:(NSNumber *)quantity;
- (BOOL) addProduct:(Product *)product withQuantity:(NSNumber *)quantity andObservation:(NSString *)observation;
- (BOOL) addProduct:(Product *)product withObservation:(NSString *)observation;

@property (nonatomic, strong) NSNumber *total;

- (void) removeProducts:(Product *)product;
- (void)removeProductByIndex:(NSInteger )index;


@end
