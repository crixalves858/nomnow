//
//  Cart.m
//  estafetas
//
//  Created by Cristiano Alves on 04/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Cart.h"

@implementation Cart

@synthesize products = _products;
@synthesize total = _total;

- (NSMutableArray*)products{
    if(_products == nil) {
        _products = [[NSMutableArray alloc]init];
    }
    
    return _products;
}


#pragma mark - Add

- (BOOL)addProduct:(Product *)product {
    
    return [self addProduct:product withQuantity:nil andObservation:nil];
}

- (BOOL)addProduct:(Product *)product withQuantity:(NSNumber *)quantity {
    
    return [self addProduct:product withQuantity:quantity andObservation:nil];
    
}

- (BOOL)addProduct:(Product *)product withObservation:(NSString *)observation {
    
    return [self addProduct:product withQuantity:nil andObservation:observation];
    
}


- (BOOL)addProduct:(Product *)product withQuantity:(NSNumber *)quantity andObservation:(NSString *)observation {

    if ( quantity == nil) {
        
        quantity = [NSNumber numberWithInteger:1];
        
    }
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"product.objectId == [c] %@", product.objectId];
    
    NSArray *orderProducts = [self.products filteredArrayUsingPredicate:predicate];
    
    if(orderProducts.count == 0) {
        
        OrderProduct *orderProduct = [[OrderProduct alloc]initWithProduct:product];
        orderProduct.quantity = quantity;
        orderProduct.observations = observation;
        
        return [self addOrderProduct:orderProduct];
        
    } else {
        
        OrderProduct *orderProduct = orderProducts[0];
        
        orderProduct.quantity = [NSNumber numberWithInteger:(orderProduct.quantity.integerValue + quantity.integerValue)];
        
        if ( ![observation isEqualToString:@""])
            orderProduct.observations = observation;
        
        return YES;
    }

    
}


-(BOOL)addOrderProduct:(OrderProduct *)orderProduct {
    
    if(!self.restaurant) {
        
        self.restaurant = orderProduct.product.restaurant;
    }
    
    if(self.restaurant == orderProduct.product.restaurant) {
        
        
        [_products addObject:orderProduct];
        
        return YES;
    }
    
    return NO;
}


#pragma mark - total

-(NSNumber*)total {
    
    double totalTMP = 0.0;
    
    for(int i = 0 ; i < self.products.count ; i++) {
        
        OrderProduct *orderProduct = self.products[i];
        Product *product = orderProduct.product;
        
        totalTMP += ([orderProduct.quantity doubleValue] * [product.price doubleValue]);
        
    }
    
    _total = [NSNumber numberWithDouble:totalTMP];
    
    return _total;
}


#pragma mark - Remove

-(void)removeProducts:(Product *)product {
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"product.objectId == [c] %@", product.objectId];
    
    NSArray *orderProducts = [self.products filteredArrayUsingPredicate:predicate];
    
    if ( orderProducts.count != 0 ) {
        
        OrderProduct *orderProduct = orderProducts[0];
        
        [_products removeObject:orderProduct];
    }
    
    
}

- (void)removeProductByIndex:(NSInteger)index {
    
    [_products removeObjectAtIndex:index];
}





@end
