//
//  ClientPrivate.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Card.h"

@interface ClientPrivateData : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) PFGeoPoint *location;
@property (nonatomic, strong) Card *card;

@end
