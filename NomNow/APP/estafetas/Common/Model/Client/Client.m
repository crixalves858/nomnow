//
//  Client.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Client.h"

@implementation Client

@dynamic firstName;
@dynamic lastName;
@dynamic photo;
@dynamic privateData;
@dynamic courierData;
@dynamic classification;

+ (Client *)clientWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    
    Client *client = [[Client alloc]init];
    ClientCourierData *clientCourierData = [[ClientCourierData alloc]init];
    ClientPrivateData *clientPrivateData = [[ClientPrivateData alloc] init];
    
    client.firstName = firstName;
    client.lastName = lastName;
    client.courierData = clientCourierData;
    client.privateData = clientPrivateData;
    
    return client;  
}


#pragma mark - Parse

+ (NSString *)parseClassName {
    
    return @"Client";
}


@end
