//
//  ClientPrivate.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ClientPrivateData.h"

@implementation ClientPrivateData

@dynamic address;
@dynamic location;
@dynamic card;

+ (NSString *)parseClassName {
    
    return @"ClientPrivateData";
}

@end
