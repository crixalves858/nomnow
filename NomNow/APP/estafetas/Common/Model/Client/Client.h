//
//  Client.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "ClientPrivateData.h"
#import "ClientCourierData.h"

@interface Client : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) PFFile *photo;

@property (nonatomic, strong) ClientPrivateData *privateData;
@property (nonatomic, strong) ClientCourierData *courierData;

@property (nonatomic, strong) NSNumber *classification;

+ (Client *)clientWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;

@end
