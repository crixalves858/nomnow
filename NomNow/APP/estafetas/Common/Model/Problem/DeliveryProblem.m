//
//  DeliveryProblem.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "DeliveryProblem.h"

@implementation DeliveryProblem

@dynamic descriptionOfProblem;
@dynamic user;
@dynamic order;
@dynamic photo;

+(NSString *) parseClassName {
    
    return @"DeliveryProblem";
}

@end
