//
//  OrderProblem.m
//  estafetas
//
//  Created by Cristiano Alves on 21/04/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "OrderProblem.h"

@implementation OrderProblem

@dynamic descriptionOfProblem;
@dynamic order;
@dynamic user;

+(NSString *) parseClassName {
    
    return @"OrderProblem";
}

@end
