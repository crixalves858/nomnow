//
//  MealProblem.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "MealProblem.h"

@implementation MealProblem

@dynamic descriptionOfProblem;
@dynamic order;
@dynamic user;

+(NSString *) parseClassName {
    
    return @"MealProblem";
}

@end
