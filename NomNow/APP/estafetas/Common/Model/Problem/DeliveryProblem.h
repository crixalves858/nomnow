//
//  DeliveryProblem.h
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Parse/Parse.h>
#import "Order.h"
#import "User.h"

@interface DeliveryProblem : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *descriptionOfProblem;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) PFFile *photo;

@end
