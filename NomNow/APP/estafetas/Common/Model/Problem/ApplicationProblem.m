 //
//  ApplicationProblem.m
//  estafetas
//
//  Created by Cristiano Alves on 02/03/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "ApplicationProblem.h"

@implementation ApplicationProblem

@dynamic descriptionOfProblem;
@dynamic user;

+(NSString *)parseClassName {
    
    return @"ApplicationProblem";
}

@end
