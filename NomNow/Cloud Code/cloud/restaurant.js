/**
 * Created by cristianoalves on 10/03/15.
 */

function restaurantIsOpen(restaurants, i) {
    var shedules = restaurants[i].get("schedule");
    var flag = false;

    if (shedules == undefined) {
        return false;
    }

    for (var j = 0; j < shedules.length; j++) {

        var startHour = new Date(shedules[j].get("startHour"));

        startHour.setFullYear(2015);
        startHour.setMonth(0);
        startHour.setDate(1);

        var actualHour = new Date();

        actualHour.setFullYear(2015);
        actualHour.setMonth(0);
        actualHour.setDate(1);

        var finishHour = new Date(shedules[j].get("finishHour"));

        finishHour.setFullYear(2015);
        finishHour.setMonth(0);
        finishHour.setDate(1);


        if (startHour <= actualHour && actualHour <= finishHour) {

            flag = true;
            break;
        }
    }
    console.log("Result" + flag);
    return flag;
}

Parse.Cloud.define("getNearRestaurants", function (request, response) {

    Parse.Config.get().then(function (config) {

        var distanceMaxRestaurant = config.get("NearRestaurants_distanceMaxRestaurant");
        var distanceMaxCourier = config.get("NearRestaurants_distanceMaxCourier");
        var distanceMaxBetweenRestaurantAndCourier = config.get("NearRestaurants_distanceBetweenRestaurantAndCourier");
        var RESTAURANT_AVAILABLE = config.get("NearRestaurants_RESTAURANT_AVAILABLE");
        var RESTAURANT_WITHOUT_COURIER = config.get("NearRestaurants_RESTAURANT_WITHOUT_COURIER");
        var RESTAURANT_CLOSE = config.get("NearRestaurants_RESTAURANT_CLOSE");
        var COURIER_STATE_WAITING = config.get("COURIER_STATE_WAITING");


        var result = [];
        var resultRestaurants = [];
        var resultState = [];

        var queryRestaurant = new Parse.Query("Restaurant");
        var position = request.params.location;

        queryRestaurant.withinKilometers("location", position, distanceMaxRestaurant);

        queryRestaurant.near("location", position);

        queryRestaurant.include("categories");
        queryRestaurant.include("schedule");

        queryRestaurant.find({

            success: function (restaurants) {

                var queryCourier = new Parse.Query("Courier");
                var queryCourierClientData = new Parse.Query("CourierClientData");
                var queryCourierPrivateData = new Parse.Query("CourierPrivateDate");


                queryCourierPrivateData.equalTo("state", COURIER_STATE_WAITING);

                queryCourier.include("privateData");
                queryCourier.include("clientData");

                queryCourier.matchesQuery("privateData", queryCourierPrivateData);
                queryCourier.matchesQuery("clientData", queryCourierClientData);

                queryCourierClientData.withinKilometers("location", position, distanceMaxCourier);


                queryCourier.find({
                    success: function (couriers) {

                        for (var i = 0; i < restaurants.length; i++) {

                            var flag = restaurantIsOpen(restaurants, i);
                            var otherFlag = true;
                            if (!flag) {
                                otherFlag = false;
                                var restaurant = restaurants[i];
                                resultRestaurants.push(restaurant);
                                resultState.push(RESTAURANT_CLOSE);

                            } else {



                                for (var j = 0; j < couriers.length; j++) {

                                    if (restaurants[i].get("location").kilometersTo(couriers[j].get("clientData").get("location")) < distanceMaxBetweenRestaurantAndCourier) {

                                        var restaurant = restaurants[i];
                                        resultRestaurants.push(restaurant);
                                        resultState.push(RESTAURANT_AVAILABLE);
                                        otherFlag = false;

                                        break;
                                    }
                                }
                            }

                            if (otherFlag) {

                                resultRestaurants.push(restaurants[i]);
                                resultState.push(RESTAURANT_WITHOUT_COURIER);

                            }
                        }
                        result.push(resultRestaurants);
                        result.push(resultState);
                        response.success(result);
                    },
                    error: function (error) {
                        response.error(error);
                    }
                });
            },
            error: function (error) {
                response.error(error);
            }
        });
    }, function (error) {
        response.error(error);
    });
});

