/**
 * SocketIO REST API Module
 * @name SocketIO
 * @namespace SocketIO
 *
 *
 * <ul><li>Module Version: 1.0.0</li>
 *
 *
 */

 (function() {
    var SOCKET_IO_SERVER_URL = 'http://nomnow-glazed.rhcloud.com/'

    module.exports = {
        publishUpdate: function(collectionName, acl) {
            if (!SOCKET_IO_SERVER_URL) {
                throw new Error('No \'SOCKET_IO_SERVER_URL\' config defined in Parse.')
            }
            // Call socketIO HTTP REST API
            Parse.Cloud.httpRequest({
                method: 'PUT',
                url: SOCKET_IO_SERVER_URL + 'update/' + collectionName,
                body: {
                    request: JSON.stringify(acl)
                },
                success: function(httpResponse) {
                    console.log(httpResponse.text)
                },
                error: function(httpResponse) {
                    console.error('Request failed ' + httpResponse.status)
                }
            })
        }
    }
}())
