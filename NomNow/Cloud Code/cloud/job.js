Parse.Cloud.job("job", function(request, status) {
  // Set up to modify user data
  Parse.Cloud.useMasterKey();
  
  function sleep(delay) { 
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay);
  }
  
  
  function loop() {
    
    Parse.Cloud.run("cron", {}, {
      success: function(results) {
          sleep(1000);
          loop();
      },
      error: function(error) {
        sleep(1000);
         loop();
      }
    });
    
  }
  
  
  
  loop();
    
});
