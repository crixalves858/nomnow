/**
 * SwitchPayment Module
 * @name SwitchPayment
 * @namespace SP
 * @author Cristiano Alves [cristiano.alves@glazedsolutions.com]
 *
 *
 *
 */

 /**
* This is the description for my class.
*
* @class MyClass
* @constructor
*/
(function() {

    const SANDBOX = 'api-test.switchpayments.com/v1/';
    const LIVE = 'api.switchpayments.com/v1/';
    var environment;
    var merchant_id;
    var private_key;

    module.exports = {

        /**
         * Initialize the Switch Payment module with the merchant_key, private_key and environment.
         * @param {String} merchant_key Your merchant key in Switch
         * @param {String} key Your private key in Switch
         * @param {String} environmentType Environment to be use. SANDBOX or LIVE
         */
        initialize: function(merchant_key,key, environmentType) {
            environment = environmentType;
            private_key = key;
            merchant_id = merchant_key;
            return this;
        },

         /**
         * Create a card.
         * @param {String} oneTimeCardToken A one time card token
         * @return {Parse.Promise}
         */
        createCard: function(oneTimeCardToken) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id +":"+private_key+ "@" + environment+"/cards",
                body: '{"card":"' + oneTimeCardToken +'"}'
            }).then(function(httpResponse) {
                promise.resolve(httpResponse.data);

            }, function(httpResponse) {


                promise.reject(httpResponse.data);
            });

            return promise;
        },

        /**
         * Create a Payment capture on creation
         * @param {double} amount Amount of payment
         * @param {String} currency Currency type
         * @param {object} card Card create with createCard
         * @return {Parse.Promise}
         */
        createPayment: function(amount,currency, card) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id +":"+private_key+ "@" + environment+"/payments",
                body: '{"amount":'+ amount +', "capture_on_creation": true, "currency": "EUR" , "card": "' + card + '"}'
            }).then(function(httpResponse) {
                promise.resolve(httpResponse.data);

            }, function(httpResponse) {

                promise.reject(httpResponse.data);
            });

            return promise;

        }/*, capturePayment: function(amount, transationId) {
            var promise = new Parse.Promise();
            Parse.Cloud.httpRequest({
                method: "POST",
                url: "https://" + merchant_id + ":" + private_key + "@" + environment + "/payments/"+transationId+"/capture/",
                body: '{"amount":' + amount +'}'
            }).then(function (httpResponse) {
                promise.resolve(httpResponse.data);

            }, function (httpResponse) {
                //     promise.resolve();

                promise.reject(httpResponse.data);
            });

            return promise;
        }*/
    }
}());
