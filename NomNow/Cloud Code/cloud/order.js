/**
 * Created by cristianoalves on 10/03/15.
 */

const courierInEligileStr = "courier";
const lastContactInEligibleStr = "lastContact";
const historyInEligibleStr = "history";
const stateInEligibleStr = "state";
const orderInEligibleStr = "order";
const valueInEligibleStr = "value";
const distanceToRestaurantInEligibleStr = "distanceToRestaurant";


const privateDataInCourierStr = "privateData";
const clientDataInCourierStr = "clientData";
const locationInCourierStr = "location";
const classificationInCourierStr = "classification";
const stateInCourierStr = "state";

const stateInOrderStr = "state";
const courierInOrderStr = "courier";
const clientInOrderStr = "client";
const productsInOrderStr = "products";
const distanceRestaurantClientInOrderStr = "distanceRestaurantClient";
const distanceCourierRestaurantInOrderStr ="distanceCourierRestaurant";
const locationInOrderStr = "deliveryLocation";
const restaurantInOrdersStr = "restaurant";


const clientInUserStr = "client";

const orderInRestaurantStr = "restaurant";
const locationRestaurantStr = "location";

const courierDataInUserStr = "courier";

const clientCourierDataInClientStr = "courierData";

Parse.Cloud.define("findEligibleCouriers", function (request, response) {

    Parse.Config.get().then(function (config) {

        var distanceMaxBetweenRestaurantAndCourier = config.get("NearRestaurants_distanceBetweenRestaurantAndCourier");
        var COURIER_STATE_WAITING = config.get("COURIER_STATE_WAITING");

        var EligibleCourier = Parse.Object.extend("EligibleCourier");
        var Order = Parse.Object.extend("Order");

        Parse.Cloud.useMasterKey();

        var order = new Order();
        order.id = request.params.order;

        var queryOrder = new Parse.Query(Order);
        queryOrder.include("restaurant");
        queryOrder.include("restaurant.courier");
        queryOrder.get(order.id, {
            success: function (orderResult) {
                order = orderResult;

                var queryCourier = new Parse.Query("Courier");
                var queryUser = new Parse.Query(Parse.User);
                var queryCourierPrivateData = new Parse.Query("CourierPrivateDate");
                var queryCourierClientData = new Parse.Query("CourierClientData");
                var ELIIGIBLE_COURIERS_NOT_CONTACT = config.get("ELIIGIBLE_COURIERS_NOT_CONTACT");
                var ELIIGIBLE_COURIERS_ACCEPT = config.get("ELIIGIBLE_COURIERS_ACCEPT");




                if(order.get("restaurant").get("courier") != undefined) {

                    var eligibleCouriers = [];
                    var e = new EligibleCourier();
                    var acl = new Parse.ACL();
                    acl.setPublicReadAccess(false);
                    acl.setPublicWriteAccess(false);
                    acl.setReadAccess(order.get("restaurant").get("courier"),true);
                    acl.setWriteAccess(order.get("restaurant").get("courier"),true);
                    e.setACL(acl);
                    e.set(courierInEligileStr, order.get("restaurant").get("courier"));
                    e.set(stateInEligibleStr, ELIIGIBLE_COURIERS_ACCEPT);
                    e.set(historyInEligibleStr, false);
                    e.set(orderInEligibleStr,order);
                    e.set(distanceToRestaurantInEligibleStr, 0);
                    eligibleCouriers.push(e);
                    orderResult.set("state", 0);
                    orderResult.save();
                    Parse.Object.saveAll(eligibleCouriers, {
                        success: function (objs) {

                            response.success("ok");

                        }
                    });
                } else {


                    queryCourier.include(privateDataInCourierStr);
                    queryCourier.include(clientDataInCourierStr);


                    queryCourierClientData.withinKilometers(locationInCourierStr, order.get(orderInRestaurantStr).get(locationRestaurantStr), distanceMaxBetweenRestaurantAndCourier);

                    queryCourierPrivateData.equalTo(stateInCourierStr, COURIER_STATE_WAITING);

                    queryCourier.matchesQuery(privateDataInCourierStr, queryCourierPrivateData);

                    queryCourier.matchesQuery(clientDataInCourierStr, queryCourierClientData);

                    queryUser.matchesQuery(courierDataInUserStr, queryCourier);
                    queryUser.include(courierDataInUserStr);
                    queryUser.include(courierDataInUserStr + "." + privateDataInCourierStr);
                    queryUser.include(courierDataInUserStr + "." + clientDataInCourierStr);

                    queryUser.find({

                        success: function (users) {

                            var eligibleCouriers = [];

                            if (users.length == 0) {

                                response.error("no couriers");
                            } else {

                                for (var i = 0; i < users.length; i++) {

                                    var e = new EligibleCourier();
                                    var acl = new Parse.ACL();
                                    acl.setPublicReadAccess(false);
                                    acl.setPublicWriteAccess(false);
                                    acl.setReadAccess(users[i], true);
                                    acl.setWriteAccess(users[i], true);
                                    e.setACL(acl);
                                    e.set(courierInEligileStr, users[i]);
                                    e.set(stateInEligibleStr, ELIIGIBLE_COURIERS_NOT_CONTACT);
                                    e.set(historyInEligibleStr, false);
                                    e.set(orderInEligibleStr, order);
                                    eligibleCouriers.push(e);
                                    console.log( orderResult.get(locationInOrderStr));
                                    var url = URLDistanceGenerate(users, order.get(orderInRestaurantStr).get(locationRestaurantStr), orderResult.get(locationInOrderStr));
                                    Parse.Cloud.httpRequest({
                                        url: url,

                                        success: function (httpResponse) {
                                            var json = JSON.parse(httpResponse.text);
                                            var status = json["status"];
                                            var array = json["rows"];
                                            for (var i = 0; i < array.length-1; i++) {

                                                var value = json["rows"][i]["elements"][0]["distance"].value;
                                                eligibleCouriers[i].set(distanceToRestaurantInEligibleStr, value);
                                                eligibleCouriers[i].set(valueInEligibleStr, calculateRealValeu(eligibleCouriers[i].get(courierInEligileStr).get(courierDataInUserStr), value));

                                            }

                                            var distanceToClient = json["rows"][array.length-1]["elements"][0]["distance"].value;
                                            console.log(distanceToClient);

                                            orderResult.set(distanceRestaurantClientInOrderStr, distanceToClient);
                                            orderResult.set("state", 0);
                                            orderResult.save();
                                            Parse.Object.saveAll(eligibleCouriers, {
                                                success: function (objs) {

                                                    response.success("ok");

                                                }
                                            });

                                        },
                                        error: function (httpResponse) {

                                            for (var i = 0; i < couriers.length; i++) {

                                                var value = users[i].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr).kilometersTo(order.get(orderInRestaurantStr).get(locationRestaurantStr));
                                                eligibleCouriers[i].set(distanceToRestaurantInEligibleStr, value);
                                                eligibleCouriers[i].set(valueInEligibleStr, calculateRealValeu(eligibleCouriers[i].get(courierInEligileStr).get(courierDataInUserStr), value));

                                            }

                                            var distanceToClient = orderResult.get(locationInCourierStr).kilometersTo(order.get(restaurantInOrdersStr).get(locationRestaurantStr));
                                            orderResult.set(distanceRestaurantClientInOrderStr, distanceToClient);
                                            orderResult.set("state", 0);
                                            orderResult.save();

                                            Parse.Object.saveAll(eligibleCouriers, {
                                                success: function (objs) {

                                                    response.success("ok");

                                                }
                                            });
                                        }
                                    });
                                }

                            }
                        }
                    });
                }
            }
        });

    }, function (error) {

        response.error(error);
    });
});

//calcula o valor do estafeta
function calculateRealValeu(courier, value) {

    var velocity = courier.get("velocity");

    if(velocity == undefined) {

        velocity = 20;
    }

    value = ((value*1.0)/1000)/velocity;
    var classification = courier.get(classificationInCourierStr);

    console.log(value);
    if (classification >= 4) {
        return value;
    }

    return (5 - classification) * value;
}

function URLDistanceGenerate(array, finish) {
    var startString = "";

    for (var i = 0; i < array.length; i++) {
        var start = array[i].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr);
        startString = startString + start.latitude + "," + start.longitude + "|";
    }

    return "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + startString + "&destinations=" + finish.latitude + "," + finish.longitude;
}


function URLDistanceGenerate(array, finish, clientLocation) {
    var startString = "";

    for (var i = 0; i < array.length; i++) {
        var start = array[i].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr);
        startString = startString + start.latitude + "," + start.longitude + "|";
    }
    startString = startString + clientLocation.latitude + "," + clientLocation.longitude + "|";
    return "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + startString + "&destinations=" + finish.latitude + "," + finish.longitude;
}





Parse.Cloud.define("calcEstimatedPriceAndTime", function (request, response) {

    Parse.Config.get().then(function (config) {

        var distanceMaxBetweenRestaurantAndCourier = config.get("NearRestaurants_distanceBetweenRestaurantAndCourier");
        var COURIER_STATE_WAITING = config.get("COURIER_STATE_WAITING");

        var Restaurant = Parse.Object.extend("Restaurant");


        Parse.Cloud.useMasterKey();

        var restaurant = new Restaurant();
        var clientLocation = request.params["clientLocation"];
        restaurant.id = request.params.restaurant;

        var queryRestaurant = new Parse.Query(Restaurant);
        queryRestaurant.include("courier");
        queryRestaurant.get(restaurant.id, {
            success: function (restaurantResult) {
                restaurant = restaurantResult;


                if(restaurant.get("courier") != undefined) {

                    response.success([0, 0, 0, 0]);
                } else {

                    var queryCourier = new Parse.Query("Courier");
                    var queryUser = new Parse.Query(Parse.User);
                    var queryCourierPrivateData = new Parse.Query("CourierPrivateDate");
                    var queryCourierClientData = new Parse.Query("CourierClientData");
                    var ELIIGIBLE_COURIERS_NOT_CONTACT = config.get("ELIIGIBLE_COURIERS_NOT_CONTACT");
                    var PRICE_BY_METER = config.get("PRICE_BY_METER");
                    queryCourier.include(privateDataInCourierStr);
                    queryCourier.include(clientDataInCourierStr);



                    queryCourierClientData.withinKilometers(locationInCourierStr, restaurant.get(locationRestaurantStr), distanceMaxBetweenRestaurantAndCourier);

                    queryCourierPrivateData.equalTo(stateInCourierStr, COURIER_STATE_WAITING);

                    queryCourier.matchesQuery(privateDataInCourierStr, queryCourierPrivateData);

                    queryCourier.matchesQuery(clientDataInCourierStr, queryCourierClientData);

                    queryUser.matchesQuery(courierDataInUserStr, queryCourier);
                    queryUser.include(courierDataInUserStr);
                    queryUser.include(courierDataInUserStr+"."+privateDataInCourierStr);
                    queryUser.include(courierDataInUserStr+"."+clientDataInCourierStr);

                    queryUser.find({

                        success: function (users) {

                            var eligibleCouriers = [];

                            if (users.length == 0) {

                                response.error("no couriers");
                            } else {


                                var url = URLDistanceGenerate(users, restaurant.get(locationRestaurantStr), clientLocation);
                                Parse.Cloud.httpRequest({
                                    url: url,

                                    success: function (httpResponse) {
                                        var json = JSON.parse(httpResponse.text);
                                        var status = json["status"];
                                        var array = json["rows"];
                                        var max;
                                        var min;
                                        var maxTime;
                                        var minTime;
                                        var distanceToClient = json["rows"][array.length-1]["elements"][0]["distance"].value;
                                        if (array.length-1 > 0) {

                                            max = json["rows"][0]["elements"][0]["distance"].value;
                                            min = json["rows"][0]["elements"][0]["distance"].value;
                                            maxTime =  (json["rows"][0]["elements"][0]["distance"].value+distanceToClient) / (users[0].get("courier").get("velocity") * 1000);
                                            minTime =  (json["rows"][0]["elements"][0]["distance"].value+distanceToClient) / (users[0].get("courier").get("velocity") * 1000);
                                        }
                                        for (var i = 0; i < array.length-1; i++) {

                                            var value = json["rows"][i]["elements"][0]["distance"].value;
                                            var valueTime = (json["rows"][i]["elements"][0]["distance"].value+distanceToClient) / (users[i].get("courier").get("velocity") * 1000);
                                            if (value > max) {

                                                max = value;
                                            }

                                            if (value < min) {

                                                min = value;
                                            }

                                            if(valueTime < minTime) {

                                                minTime = valueTime;
                                            }

                                            if(valueTime > maxTime) {

                                                maxTime = valueTime;
                                            }

                                        }


                                        console.log("minDis: "+min + " client: " + distanceToClient);
                                        response.success([PRICE_BY_METER*(min+distanceToClient), PRICE_BY_METER*(max+distanceToClient), minTime*60, maxTime*60]);


                                    },
                                    error: function (httpResponse) {

                                        var max;
                                        var min;
                                        var maxTime;
                                        var minTime;
                                        var distanceToClient = clientLocation.kilometersTo(restaurant.get(locationRestaurantStr));
                                        if (users.length > 0) {

                                            max = users[0].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr).kilometersTo(restaurant.get(locationRestaurantStr));
                                            min = users[0].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr).kilometersTo(restaurant.get(locationRestaurantStr));
                                            maxTime =  (max+distanceToClient) / (users[0].get("courier").get("velocity") * 1000);
                                            minTime =  (min+distanceToClient) / (users[0].get("courier").get("velocity") * 1000);
                                        }
                                        for (var i = 0; i < users.length; i++) {

                                            var value = users[i].get(courierDataInUserStr).get(clientDataInCourierStr).get(locationInCourierStr).kilometersTo(restaurant.get(locationRestaurantStr));
                                            var valueTime = (value + distanceToClient)/ (users[i].get("courier").get("velocity") * 1000);
                                            if (value > max) {

                                                max = value;
                                            }

                                            if (value < min) {

                                                min = value;
                                            }

                                            if(valueTime < minTime) {

                                                minTime = valueTime;
                                            }

                                            if(valueTime > maxTime) {

                                                maxTime = valueTime;
                                            }

                                        }



                                        response.success([PRICE_BY_METER*(min+distanceToClient), PRICE_BY_METER*(max+distanceToClient), minTime*60, maxTime*60]);
                                    }
                                });

                            }
                        }

                    });
                }
            }
        });

    }, function (error) {

        response.error(error);
    });
});



Parse.Cloud.define("checkOrderState", function (request, response) {

    var Order = Parse.Object.extend("Order");
    var EligibleCourier = Parse.Object.extend("EligibleCourier");

    function sort(eligibleCouriers) {
        eligibleCouriers.sort(
            function (a, b) {

                if (a.get(stateInEligibleStr) < b.get(stateInEligibleStr)) {

                    return -1;

                } else if (a.get(stateInEligibleStr) > b.get(stateInEligibleStr)) {

                    return 1;

                } else {

                    if (a.get(valueInEligibleStr) < b.get(valueInEligibleStr)) {

                        return -1;
                    } else if (a.get(valueInEligibleStr) > b.get(valueInEligibleStr)) {

                        return 11;
                    } else {
                        return 0;
                    }
                }

            });
    }



    Parse.Config.get().then(function(config) {

        var ELIIGIBLE_COURIERS_NOT_CONTACT = config.get("ELIIGIBLE_COURIERS_NOT_CONTACT");
        var ELIIGIBLE_COURIERS_NOT_RESPONSE = config.get("ELIIGIBLE_COURIERS_NOT_RESPONSE");
        var ELIIGIBLE_COURIERS_ACCEPT = config.get("ELIIGIBLE_COURIERS_ACCEPT");
        var ELIIGIBLE_COURIERS_REFUSE = config.get("ELIIGIBLE_COURIERS_REFUSE");
        var ELIIGIBLE_COURIERS_LAST_TRY = config.get("ELIIGIBLE_COURIERS_LAST_TRY");
        var ELIIGIBLE_COURIERS_WAIT_RESPONSE = config.get("ELIIGIBLE_COURIERS_WAIT_RESPONSE");

        var COURIER_STATE_IN_WORK = config.get("COURIER_STATE_IN_WORK");

        var RETURN_STATE_ORDER_HAVE_COURIER = config.get("RETURN_STATE_ORDER_HAVE_COURIER");
        var RETURN_STATE_ORDER_WAITING = config.get("RETURN_STATE_ORDER_WAITING");
        var RETURN_STATE_ORDER_NOT_HAVE_COURIER = config.get("RETURN_STATE_ORDER_NOT_HAVE_COURIER");

        var ORDER_STATE_GO_TO_RESTAURANT = config.get("ORDER_STATE_GO_TO_RESTAURANT");
        var ORDER_STATE_FAIL_NOT_HAVE_COURIER = config.get("ORDER_STATE_FAIL_NOT_HAVE_COURIER");

        var PRICE_BY_METER = config.get("PRICE_BY_METER");
        var OrderStateUpdate = Parse.Object.extend("OrderStateUpdate");

        var ORDER_STATE_HAVE_COURIER = config.get("ORDER_STATE_HAVE_COURIER");
        var TIME_TO_RESPONSE = config.get("TIME_TO_RESPONSE");


        function courierAccepted(eligibleCouriers) {


            var Commission = Parse.Object.extend("Commission");
            var commission = new Commission();

            eligibleCouriers[0].get(courierInEligileStr).get(courierDataInUserStr).get(privateDataInCourierStr).set(stateInCourierStr, COURIER_STATE_IN_WORK);

            for (var i = 0; i < eligibleCouriers.length; i++) {

                eligibleCouriers[i].set(historyInEligibleStr, true);

            }


            var update = new OrderStateUpdate();

            update.set("state", 0);
            update.set("location", null);
            update.set("date", new Date());
            update.set("order", order);
            update.set("seen", false);

            update.save();



            order.set(courierInOrderStr, eligibleCouriers[0].get(courierInEligileStr));
            order.set(stateInOrderStr, ORDER_STATE_HAVE_COURIER);
            order.set(distanceCourierRestaurantInOrderStr,eligibleCouriers[0].get(distanceToRestaurantInEligibleStr));
            commission.set("value", 0);
            commission.set("value", (order.get("distanceCourierRestaurant")+order.get("distanceRestaurantClient"))*PRICE_BY_METER);
            commission.set("state", 0);
            commission.save().then(
                function() {


                    order.set("commission", commission);
                    var aclOrder = order.getACL();
                    aclOrder.setReadAccess(eligibleCouriers[0].get(courierInEligileStr),true);
                    aclOrder.setWriteAccess(eligibleCouriers[0].get(courierInEligileStr),true);

                    order.setACL(aclOrder);

                    Parse.Object.saveAll(eligibleCouriers, null, null);

                    order.save(null, null, null);
            });





            var products = eligibleCouriers[0].get(orderInEligibleStr).get(productsInOrderStr);

            if(products.length > 0) {

                var aclProducts = new Parse.ACL();
                //products[0].getACL();

                aclProducts.setReadAccess(eligibleCouriers[0].get(courierInEligileStr),true);
                aclProducts.setWriteAccess(eligibleCouriers[0].get(courierInEligileStr),true);

                aclProducts.setReadAccess(eligibleCouriers[0].get(orderInEligibleStr).get(clientInOrderStr),true);
                aclProducts.setWriteAccess(eligibleCouriers[0].get(orderInEligibleStr).get(clientInOrderStr),true);

                for( var i = 0; i < products.length; i++) {

                    products[i].setACL(aclProducts);
                }

            }

            Parse.Object.saveAll(products);




            var aclCourierDataInClient = order.get(clientInOrderStr).get(clientInUserStr).get(clientCourierDataInClientStr).getACL();

            aclCourierDataInClient.setReadAccess(eligibleCouriers[0].get(courierInEligileStr),true);
            aclCourierDataInClient.setWriteAccess(eligibleCouriers[0].get(courierInEligileStr),false);


            order.get(clientInOrderStr).get(clientInUserStr).get(clientCourierDataInClientStr).setACL(aclCourierDataInClient);
            order.get(clientInOrderStr).get(clientInUserStr).get(clientCourierDataInClientStr).save();

            var queryRole = new Parse.Query(Parse.Role);

            var roleStr = "courier"+eligibleCouriers[0].get(orderInEligibleStr).get(clientInOrderStr).id;

            queryRole.equalTo("name", roleStr);

            queryRole.find({
                success: function (role) {
                    console.log(role);
                    if(role.length > 0) {

                        role[0].getUsers().add(eligibleCouriers[0].get(orderInEligibleStr).get(courierInOrderStr));

                        role[0].save();
                        response.success(RETURN_STATE_ORDER_HAVE_COURIER);
                    }


                }
            });

        }

        function courierRefuse(eligibleCouriers, userClient, client, products) {

            var state = eligibleCouriers[0].get(stateInEligibleStr);
            var courier = eligibleCouriers[0].get(courierInEligileStr);

            var aclOrder = order.getACL();
            aclOrder.setReadAccess(courier,false);
            aclOrder.setWriteAccess(courier,false);

            order.setACL(aclOrder);

            var aclUserClient = userClient.getACL();
            aclUserClient.setReadAccess(courier,false);
            aclUserClient.setWriteAccess(courier,false);
            userClient.setACL(aclUserClient);


            var aclClient = client.getACL();
            aclClient.setReadAccess(courier,false);
            aclClient.setWriteAccess(courier,false);
            client.setACL(aclClient);

            if(products.length > 0) {

                var aclProducts = products[0].getACL();

                aclProducts.setReadAccess(courier, false);
                aclProducts.setWriteAccess(courier, false);

                for (var i = 0; i < products.length; i++) {

                    products[i].setACL(aclProducts);
                }

            }

            client.save();
            userClient.save();
            order.save();

            Parse.Object.saveAll(products);


            eligibleCouriers[0].set(historyInEligibleStr,true);

            eligibleCouriers[0].save(null, null, null);

            eligibleCouriers.splice(0, 1);



        }

        function contactCourier(eligibleCouriers, userClient, client, products, state) {

            var courier = eligibleCouriers[0].get(courierInEligileStr);
            var queryInstallation = new Parse.Query(Parse.Installation);

            queryInstallation.equalTo("user", userClient);

            eligibleCouriers[0].set(stateInEligibleStr, state);

            var actualDate = new Date();

            eligibleCouriers[0].set(lastContactInEligibleStr, actualDate);
            eligibleCouriers[0].save();

            var aclOrder = order.getACL();
            aclOrder.setReadAccess(courier,true);
            aclOrder.setWriteAccess(courier,false);

            order.setACL(aclOrder);

            var aclUserClient = userClient.getACL();
            aclUserClient.setReadAccess(courier,true);
            aclUserClient.setWriteAccess(courier,false);
            userClient.setACL(aclUserClient);

            var aclClient = client.getACL();
            aclClient.setReadAccess(courier,true);
            aclClient.setWriteAccess(courier,false);

            client.setACL(aclClient);

            if(products.length > 0) {

                var aclProducts = products[0].getACL();

                aclProducts.setReadAccess(courier,true);
                aclProducts.setWriteAccess(courier,false);

                for( var i = 0; i < products.length; i++) {

                    products[i].setACL(aclProducts);
                }

            }

            Parse.Object.saveAll(products);


            client.save();
            userClient.save();
            order.save();


            Parse.Push.send({
                where: queryInstallation,
                data: {
                    alert: "Have a order."
                }
            }, {
                success: function () {

                },
                error: function (error) {

                    console.log(error);
                }
            });

            response.success(RETURN_STATE_ORDER_WAITING);

        }

        Parse.Cloud.useMasterKey();

        var order = new Order();
        order.id = request.params.order;

        var queryEligible = new Parse.Query(EligibleCourier);
        queryEligible.include(orderInEligibleStr);
        queryEligible.include(courierInEligileStr);
        queryEligible.include(courierInEligileStr+"."+courierDataInUserStr);
        queryEligible.include(orderInEligibleStr+"."+clientInOrderStr);
        queryEligible.include(orderInEligibleStr+"."+clientInOrderStr+"."+clientInUserStr);
        queryEligible.include(orderInEligibleStr+"."+clientInOrderStr+"."+clientInUserStr+"."+clientCourierDataInClientStr);
        queryEligible.include(orderInEligibleStr+"."+productsInOrderStr);
        queryEligible.equalTo(orderInEligibleStr, order);
        queryEligible.equalTo(historyInEligibleStr, false);

        queryEligible.find({
            success: function (eligibleCouriers) {

                function waitingCourier(eligibleCouriers, userClient, client, products) {

                    var actualDate = new Date();
                    var lastContact = eligibleCouriers[0].get(lastContactInEligibleStr);
                    var diffMs = (actualDate - lastContact);
                    var state = eligibleCouriers[0].get(stateInEligibleStr);
                    var courier = eligibleCouriers[0].get(courierInEligileStr);


                    if (diffMs > TIME_TO_RESPONSE) {

                        var aclOrder = order.getACL();
                        aclOrder.setReadAccess(courier,false);
                        aclOrder.setWriteAccess(courier,false);

                        order.setACL(aclOrder);

                        var aclUserClient = userClient.getACL();
                        aclUserClient.setReadAccess(courier,false);
                        aclUserClient.setWriteAccess(courier,false);
                        userClient.setACL(aclUserClient);


                        var aclClient = client.getACL();
                        aclClient.setReadAccess(courier,false);
                        aclClient.setWriteAccess(courier,false);
                        client.setACL(aclClient);

                        if(products.length > 0) {

                            var aclProducts = products[0].getACL();

                            aclProducts.setReadAccess(courier, false);
                            aclProducts.setWriteAccess(courier, false);

                            for (var i = 0; i < products.length; i++) {

                                products[i].setACL(aclProducts);
                            }

                        }

                        client.save();
                        userClient.save();
                        order.save();

                        Parse.Object.saveAll(products);



                        if (state == ELIIGIBLE_COURIERS_WAIT_RESPONSE) {

                            eligibleCouriers[0].set(stateInEligibleStr, ELIIGIBLE_COURIERS_NOT_RESPONSE);

                            eligibleCouriers[0].save(null, {
                                success: function(gameScore) {
                                    sort(eligibleCouriers);
                                    checkState();
                                },
                                error: function(gameScore, error) {

                                }
                            });

                        } else {

                            eligibleCouriers[0].set(historyInEligibleStr, true);

                            eligibleCouriers[0].save(null, {
                                success: function(eligible) {

                                    eligibleCouriers.splice(0, 1);
                                    sort(eligibleCouriers);
                                    checkState();
                                },
                                error: function(eligible, error) {


                                }
                            });
                        }

                    } else {

                        response.success(RETURN_STATE_ORDER_WAITING);

                    }

                }

                var userClient;
                var client;
                var products;
                if(eligibleCouriers.length > 0) {


                     userClient = eligibleCouriers[0].get(orderInEligibleStr).get(clientInOrderStr);

                     client = userClient.get(clientInUserStr);
                     order = eligibleCouriers[0].get(orderInEligibleStr);
                     products = order.get(productsInOrderStr)
                }

                function checkState() {

                    if (eligibleCouriers.length == 0) {

                        order.set(stateInOrderStr, ORDER_STATE_FAIL_NOT_HAVE_COURIER);

                        order.save();

                        var update = new OrderStateUpdate();

                        update.set("state", ORDER_STATE_FAIL_NOT_HAVE_COURIER);
                        update.set("location", null);
                        update.set("date", new Date());
                        update.set("order", order);
                        update.set("seen", false);

                        update.save();

                        response.error(RETURN_STATE_ORDER_NOT_HAVE_COURIER);
                        return;
                    }

                    var state = eligibleCouriers[0].get(stateInEligibleStr);


                    switch (state) {

                        case ELIIGIBLE_COURIERS_ACCEPT:

                            courierAccepted(eligibleCouriers);
                            break;

                        case ELIIGIBLE_COURIERS_REFUSE:

                            courierRefuse(eligibleCouriers, userClient, client, products);
                            sort(eligibleCouriers);
                            checkState();
                            break;

                        case ELIIGIBLE_COURIERS_WAIT_RESPONSE:

                            waitingCourier(eligibleCouriers, userClient, client, products);
                            break;

                        case ELIIGIBLE_COURIERS_LAST_TRY:
                            waitingCourier(eligibleCouriers, userClient, client, products);
                            break;

                        case ELIIGIBLE_COURIERS_NOT_CONTACT:
                            contactCourier(eligibleCouriers, userClient, client, products, ELIIGIBLE_COURIERS_WAIT_RESPONSE);
                            break;

                        case ELIIGIBLE_COURIERS_NOT_RESPONSE:



                            var eligibleCourier = eligibleCouriers[0];
                            var new_history = new EligibleCourier();

                            new_history.set(stateInEligibleStr, eligibleCourier.get(stateInEligibleStr));
                            new_history.set(valueInEligibleStr, eligibleCourier.get(valueInEligibleStr));
                            new_history.set(lastContactInEligibleStr, eligibleCourier.get(lastContactInEligibleStr));
                            new_history.set(orderInEligibleStr, eligibleCourier.get(orderInEligibleStr));
                            new_history.set(courierInEligileStr, eligibleCourier.get(courierInEligileStr));
                            new_history.set(historyInEligibleStr, true);

                            new_history.setACL(eligibleCourier.getACL());

                            new_history.save(null, {
                                success: function(history) {

                                    contactCourier(eligibleCouriers, userClient, client, products, ELIIGIBLE_COURIERS_LAST_TRY);

                                }
                            });
                            break;
                    }
                }

                sort(eligibleCouriers);
                checkState();
            }
        });

    }, function(error) {
        response.error(error);
    });

});