# SP

SwitchPayment Module



* * *

### SP.initialize(merchant_key, key, environmentType) 

Initialize the Switch Payment module with the merchant_key, private_key and environment.

**Parameters**

**merchant_key**: `String`, Your merchant key in Switch

**key**: `String`, Your private key in Switch

**environmentType**: `String`, Environment to be use. SANDBOX or LIVE



### SP.createCard(oneTimeCardToken) 

Create a card.

**Parameters**

**oneTimeCardToken**: `String`, A one time card token

**Returns**: `Parse.Promise`


### SP.createPayment(amount, currency, card) 

Create a Payment capture on creation

**Parameters**

**amount**: `double`, Amount of payment

**currency**: `String`, Currency type

**card**: `object`, Card create with createCard

**Returns**: `Parse.Promise`



* * *










