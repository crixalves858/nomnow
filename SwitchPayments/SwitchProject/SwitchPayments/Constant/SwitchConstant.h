//
//  Constant.h
//  SwitchPayments
//
//  Created by Cristiano Alves on 24/04/15.
//  Copyright (c) 2015 switchpayments. All rights reserved.
//

#ifndef SwitchPayments_Constant_h
#define SwitchPayments_Constant_h

typedef NS_ENUM(NSInteger, EnvironmentType) {
    SandBox = 0,
    Live = 1,
};

#endif
