//
//  SwitchPayments.m
//  SwitchPayments
//
//  Created by Cristiano Alves on 24/04/15.
//  Copyright (c) 2015 switchpayments. All rights reserved.
//

#import "SwitchPayments.h"
#import <AFNetworking.h>


static NSString *SPPublicKey;
static NSString *SPEnvironment;

@implementation SwitchPayments


+ (NSString*) SANDBOX {
    
    return @"https://api-test.switchpayments.com/v1/";
}

+ (NSString*) LIVE {
    
    return @"https://api.switchpayments.com/v1/";
}

+ (void) setupWithPublicKey:(NSString *)publicKey andEnvironment:(NSString*)environment {
    
    SPEnvironment = environment;
    SPPublicKey = publicKey;
}

+ (void) createCardTokenWithName:(NSString*)name number:(NSNumber*)number month:(NSNumber*)month year:(NSNumber*)year cvv:(NSString*)cvv country:(NSString*)country withBlock:(void(^)(SPCard *card, NSError *error)) block {
  
    NSDictionary *paramenters = @{
                                 @"name":name,
                                 @"number":number,
                                 @"expiration_month":month,
                                 @"expiration_year":year,
                                 @"cvv":cvv,
                                 @"country":country,
                                 @"type":@"card",
                                 @"cvc":cvv};
    

    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:SPEnvironment]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:SPPublicKey password:@""];
    
    [manager POST:@"tokens/" parameters:paramenters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        SPCard *card = [[SPCard alloc] init];

        card.name = name;
        card.month = month;
        card.year = year;
        NSString *stringNumber =  [NSString stringWithFormat:@"%@", number];
        card.last4Numbers = [stringNumber substringFromIndex:12];
        card.token = responseObject[@"value"];
        block(card, nil);
    
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
    }];
    
}


@end
