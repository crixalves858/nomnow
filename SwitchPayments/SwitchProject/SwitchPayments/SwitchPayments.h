//
//  SwitchPayments.h
//  SwitchPayments
//
//  Created by Cristiano Alves on 24/04/15.
//  Copyright (c) 2015 switchpayments. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SPCard.h"

/*!
 SDK for useß Switch Payment API.
 */
@interface SwitchPayments : NSObject
///--------------------------------------
/// @name Connecting to Switch Payment
///--------------------------------------


/*!
 * @discussion SandBox Environment
 * @return Return a NSString with URL of SandBox API
 */
+ (NSString*) SANDBOX;

/*!
 * @discussion Live Environment
 * @return Return a NSString with URL of Production API
 */
+ (NSString*) LIVE;

/*!
 * @discussion Configure the Switch Payment with key public and the environment to be used
 * @param publicKey is a public key available in Switch Payment Dashboard
 * @param environment is the environment to be used - SANDBOX or LIVE
 */
+ (void) setupWithPublicKey:(NSString*)publicKey andEnvironment:(NSString*)environment;

/*!
 * @discussion Create a Card token with credit card data in background with block
 * @param name is the name of credit card owner
 * @param number is the number of credit card
 * @param month is the month of validity date of credit card
 * @param year is a year of validity date of credit card
 * @param cvv are the 3 caracter present in back of credit card
 * @param country is the country of the credit card
 * @param block is the block to execute. It should have the following argument signature: `^(SPCard *card, NSError *error)`.
 */
+ (void) createCardTokenWithName:(NSString*)name number:(NSNumber*)number month:(NSNumber*)month year:(NSNumber*)year cvv:(NSString*)cvv country:(NSString*)country withBlock:(void(^)(SPCard *card, NSError *error)) block;


@end
