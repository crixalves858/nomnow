//
//  SPCard.h
//  SwitchPayments
//
//  Created by Cristiano Alves on 24/04/15.
//  Copyright (c) 2015 switchpayments. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPCard : NSObject


/*!
 * @brief Name of credit card owner.
 */
@property (nonatomic, strong) NSString *name;

/*!
 * @brief Month of validity date of credit card.
 */
@property (nonatomic, strong) NSNumber *month;

/*!
 * @brief Year of validity date of credit card.
 */
@property (nonatomic, strong) NSNumber *year;

/*!
 * @brief Token create by Switch Payment API.
 */
@property (nonatomic, strong) NSString *token;

/*!
 * @brief Last four numbers of credit card number.
 */
@property (nonatomic, strong) NSString *last4Numbers;

@end
