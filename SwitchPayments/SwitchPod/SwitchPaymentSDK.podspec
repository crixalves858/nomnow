#
# Be sure to run `pod lib lint SwitchPaymentSDK.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "SwitchPaymentSDK"
  s.version          = "0.1.0"
  s.summary          = "A short description of SwitchPaymentSDK."
  s.description      = <<-DESC
                       An optional longer description of SwitchPaymentSDK

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "http://switchpayments.com"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Switch Payment" => "support@switchpayments.com" }
  s.source           = { :git => 'https://github.com/crixalves/switch.git', :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/**/*'
  s.resource_bundles = {
    'SwitchPaymentSDK' => ['Pod/Assets/*.png']
  }
  s.vendored_libraries = 'Pod/**/*.a'
  s.public_header_files = 'Pod/**/*.h'
  # s.frameworks = 'UIKit',
  s.dependency 'AFNetworking'

end
