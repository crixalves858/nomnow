# NomNow #

Este repositório contém o código referente à APP NomNow incluindo o CloudCode do seu *backend* e um sdk do seu sistema de pagamento.

### APP ###

App encontra-se NomNow/APP/.
Sendo um projecto de xCode desenvolvido em objective-c.

### CloudCode ###

Encontra-se em NomNow/CloudCode/

É um conjunto de ficheiro desenvolvidos em Parse cloud code.

### Switch Payment ###

Encontra-se em SwitchPayment/

Contém um CocoaPod e o projecto para compilar a lib.